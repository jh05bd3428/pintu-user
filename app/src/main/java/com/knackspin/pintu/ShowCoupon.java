package com.knackspin.pintu;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.knackspin.pintu.Adapter.CouponAdapter;
import com.knackspin.pintu.Model.CouponModel;
import com.knackspin.pintu.Singleton.Singleton;
import com.knackspin.pintu.utils.UrlPoint;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ShowCoupon extends AppCompatActivity {

    EditText shop_coupon_code;
    TextView coupon_code_apply,c_code,c_amount;
    ImageView details_arrow_back;

    ArrayList<CouponModel> listOfCoupons;
    CouponAdapter couponAdapter;
    RecyclerView recyclerView;
    LinearLayoutManager linearLayoutManager;

    String provider_id,u_id;
    RelativeLayout rel_no_coupon;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_coupon);

        initialization();
        listOfCoupons = new ArrayList<>();
        recyclerView.setHasFixedSize(true);
        couponAdapter = new CouponAdapter(listOfCoupons, this);
        recyclerView.setAdapter(couponAdapter);
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);

        SharedPreferences sharedPreferences = getSharedPreferences("MyData", Context.MODE_PRIVATE);
        provider_id = sharedPreferences.getString("providerID", "NA");
        u_id = sharedPreferences.getString("userId", "NA");

        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, new IntentFilter("sending_coupon_code"));


        showCoupon();

        coupon_code_apply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

              checkBlankFeilds();

            }
        });

        details_arrow_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });

    }



    private void checkBlankFeilds() {

        if (shop_coupon_code.getText().toString().isEmpty())
        {
            Toast.makeText(this, "Coupon code is empty", Toast.LENGTH_SHORT).show();
        } else
            {
                checkCouponCode();
            }
    }



    //method to verify the applied coupon code

    private void checkCouponCode() {

        CustomLayout.setUIToWait(ShowCoupon.this,true);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, UrlPoint.checkCouponCode, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                CustomLayout.setUIToWait(ShowCoupon.this,false);

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    if (jsonObject.optString("flag").equalsIgnoreCase("true"))
                    {
                         String message = jsonObject.getString("msg");
                        Toast.makeText(ShowCoupon.this, message, Toast.LENGTH_SHORT).show();

                        sendCouponDetails();

                    } else
                        {
                            Toast.makeText(ShowCoupon.this, "Invalid Coupon", Toast.LENGTH_SHORT).show();
                        }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d("CouponException", e.getMessage());
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                CustomLayout.setUIToWait(ShowCoupon.this,false);

                Toast.makeText(ShowCoupon.this,"Error checking coupon code", Toast.LENGTH_SHORT).show();
                //Log.e("Coupon", error.getMessage());

            }
        })

        {
            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<String, String>();
                params.put("provider_id",provider_id);
                params.put("user_id",u_id);
                params.put("promo_code",shop_coupon_code.getText().toString());
                return params;
            }

        };

        Singleton.getInstance(ShowCoupon.this).addTorequestqu(stringRequest);

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        );

    }


    //method to get the available coupon code

    private void showCoupon() {

        CustomLayout.setUIToWait(ShowCoupon.this,true);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, UrlPoint.showCoupon, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                CustomLayout.setUIToWait(ShowCoupon.this,false);

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    if (jsonObject.optString("success").equalsIgnoreCase("true"))
                    {
                        JSONArray dataArray = jsonObject.getJSONArray("data");

                        for (int data = 0;data<dataArray.length();data++)
                        {

                            JSONObject ob = dataArray.getJSONObject(data);
                            CouponModel cm = new CouponModel(ob.getString("id"), ob.getString("promo_code"),
                                    ob.getString("promo_amount"),ob.getString("description"));

                             listOfCoupons.add(cm);

                        }

                        recyclerView.setAdapter(couponAdapter);

                    } else
                    {
                        Toast.makeText(ShowCoupon.this, "No Coupon found", Toast.LENGTH_SHORT).show();
                        rel_no_coupon.setVisibility(View.VISIBLE);
                        recyclerView.setVisibility(View.GONE);

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d("Coupon Exception", e.getMessage());
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                CustomLayout.setUIToWait(ShowCoupon.this,false);

                Toast.makeText(ShowCoupon.this, "Something went wrong", Toast.LENGTH_SHORT).show();

            }
        })

        {

            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<String, String>();
                params.put("provider_id",provider_id);
                params.put("user_id",u_id);
                return params;
            }

        };

        Singleton.getInstance(ShowCoupon.this).addTorequestqu(stringRequest);

    }


    private void sendCouponDetails() {

        Intent intent = new Intent();
        intent.putExtra("discount_amount", c_amount.getText().toString());
        intent.putExtra("coupon_code", c_code.getText().toString());
        setResult(RESULT_OK, intent);
        finish();

    }

    public BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            String coupon_code = intent.getStringExtra("COUPON_CODE");
            String coupon_amount = intent.getStringExtra("COUPON_AMOUNT");
            shop_coupon_code.setText(coupon_code);
            c_amount.setText(coupon_amount);
            c_code.setText(coupon_code);

        }
    };


    private void initialization() {

        shop_coupon_code = findViewById(R.id.shop_coupon_code);
        coupon_code_apply = findViewById(R.id.coupon_code_apply);

        recyclerView = findViewById(R.id.coupon_recyclerview);
        details_arrow_back = findViewById(R.id.detail_back_arrow);

        c_code = findViewById(R.id.c_code);
        c_amount = findViewById(R.id.c_amount);
        rel_no_coupon = findViewById(R.id.rel_no_coupon);

    }
}
