package com.knackspin.pintu;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.NetworkError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.knackspin.pintu.Adapter.CheckRewardAdapater;
import com.knackspin.pintu.Model.CheckRewardModel;
import com.knackspin.pintu.Singleton.Singleton;
import com.knackspin.pintu.utils.UrlPoint;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class CheckReward extends AppCompatActivity {

    RecyclerView recyclerView;
    StaggeredGridLayoutManager staggeredGridLayoutManager;
    CheckRewardAdapater checkrewardAdpater;
    ArrayList<CheckRewardModel> listOfRewards;
    LinearLayoutManager layoutManager;
    ImageView back_rewards;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_reward);

        recyclerView = findViewById(R.id.check_reward_view);
        listOfRewards = new ArrayList<>();
        back_rewards = findViewById(R.id.back_rewards);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        checkrewardAdpater = new CheckRewardAdapater(listOfRewards,this);
        staggeredGridLayoutManager = new StaggeredGridLayoutManager(1,LinearLayoutManager.VERTICAL);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(staggeredGridLayoutManager);



        back_rewards.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });


        getRewardProducts();

    }

    private void getRewardProducts() {

        CustomLayout.setUIToWait(CheckReward.this,true);


        StringRequest stringRequest = new StringRequest(Request.Method.GET, UrlPoint.checkRewardUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                CustomLayout.setUIToWait(CheckReward.this,false);


                JSONObject jsonObject = null;

                try {
                    jsonObject = new JSONObject(response);

                    if(jsonObject.getString("status").equalsIgnoreCase("found"))
                    {

                        JSONArray jsonArray = jsonObject.getJSONArray("product");
                        for(int p=0; p<jsonArray.length(); p++)
                        {
                            JSONObject jo1 = jsonArray.getJSONObject(p);
                            CheckRewardModel check = new CheckRewardModel(jo1.optString("image"),jo1.optString("product_name"),jo1.optString("product_price"),jo1.optString("reward_point"));
                            listOfRewards.add(check);
                            recyclerView.setAdapter(checkrewardAdpater);
                        }
                    }
                    else
                        {
                            Toast.makeText(CheckReward.this, "No Products to show", Toast.LENGTH_SHORT).show();
                        }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d("Exception", e.getMessage());
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                CustomLayout.setUIToWait(CheckReward.this,true);

                if(error instanceof ServerError)
                {

                    Toast.makeText(CheckReward.this, "Server Down! Please try again", Toast.LENGTH_SHORT).show();
                }

                else if (error instanceof NetworkError)
                {
                    Toast.makeText(CheckReward.this, "Check your Internet Connection", Toast.LENGTH_SHORT).show();
                }

                else
                {
                    Toast.makeText(CheckReward.this, "Something Broken", Toast.LENGTH_SHORT).show();
                }

            }
        });
        Singleton.getInstance(CheckReward.this).addTorequestqu(stringRequest);

    }

}
