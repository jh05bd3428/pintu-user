package com.knackspin.pintu.Address;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.knackspin.pintu.Adapter.AddressAdapter;
import com.knackspin.pintu.CustomLayout;
import com.knackspin.pintu.Database.DatabaseHelper;
import com.knackspin.pintu.Model.AddressModel;
import com.knackspin.pintu.R;
import com.knackspin.pintu.Singleton.Singleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class SelectAddress extends AppCompatActivity {


    TextView add_address,send_address;
    public ArrayList<AddressModel> listOfAddress;
    DatabaseHelper databaseHelper;
    private RecyclerView recyclerView;
    AddressAdapter addressAdapter;
    LinearLayoutManager linearLayoutManager;


    String user_Id;
    public static final String DEFAULT = "NA";

    String getAddressUrl = "http://ryes.in/mobileApi/user/get_user_address";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_address);

        initailization();
        listOfAddress = new ArrayList<>();
        recyclerView = findViewById(R.id.select_address_recyle);
        recyclerView.setHasFixedSize(true);
        //databaseHelper = new DatabaseHelper(this);
        addressAdapter = new AddressAdapter(listOfAddress,this);
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(addressAdapter);

        SharedPreferences sharedPreferences = getSharedPreferences("MyData", Context.MODE_PRIVATE);
        user_Id = sharedPreferences.getString("userId", DEFAULT);

        getAddress();

        add_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(SelectAddress.this, FetchAddress.class);
                startActivity(intent);

            }
        });

        send_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                sendData();
            }
        });

    }

    private void sendData() {

      /*  Intent intent = new Intent();
        intent.putExtra("first_name", );
        setResult(RESULT_OK, intent);
        finish();*/

    }

    private void getAddress() {

        CustomLayout.setUIToWait(SelectAddress.this,true);


        StringRequest stringRequest = new StringRequest(Request.Method.POST, getAddressUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                CustomLayout.setUIToWait(SelectAddress.this,false);
               // Toast.makeText(SelectAddress.this, "Inside string request", Toast.LENGTH_SHORT).show();
                JSONObject jsonObject = null;

                try {
                    jsonObject = new JSONObject(response);

                    if (jsonObject.optString("status").equalsIgnoreCase("success"))
                    {
                        JSONArray jsonArray = jsonObject.optJSONArray("data");
                         for (int i = 0; i<jsonArray.length(); i++)
                         {

                             JSONObject ob = jsonArray.getJSONObject(i);
                             AddressModel am = new AddressModel(ob.optString("name"),
                                     ob.optString("address"),
                                     ob.optString("street"),
                                     ob.optString("city"),
                                     ob.optString("state"),
                                     ob.optString("zip"),
                                     ob.optString("phone"));
                             listOfAddress.add(am);
                             Log.d("", "");
                         }

                        addressAdapter.notifyDataSetChanged();
                    }

                    else
                        {
                            Toast.makeText(SelectAddress.this, "No address found", Toast.LENGTH_SHORT).show();
                        }

                   // recyclerView.setAdapter(addressAdapter);
                   // addressAdapter.notifyDataSetChanged();

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                CustomLayout.setUIToWait(SelectAddress.this,false);

                Toast.makeText(SelectAddress.this, error.getMessage(), Toast.LENGTH_SHORT).show();

              /*  if(error instanceof ServerError)
                {

                    Toast.makeText(SelectAddress.this, "Server Down! Please try again", Toast.LENGTH_SHORT).show();
                }

                else if (error instanceof NetworkError)
                {
                    Toast.makeText(SelectAddress.this, "Check your Internet Connection", Toast.LENGTH_SHORT).show();
                }

                else
                {
                    Toast.makeText(SelectAddress.this, "Something Broken", Toast.LENGTH_SHORT).show();
                }*/

            }
        })

        {
            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id",user_Id);
                return params;
            }

        };

        Singleton.getInstance(SelectAddress.this).addTorequestqu(stringRequest);
    }

    private void initailization() {

        add_address = findViewById(R.id.add_address_button);
        send_address = findViewById(R.id.address_select_button);
    }
}
