package com.knackspin.pintu.Address;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.cardview.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.knackspin.pintu.CustomLayout;
import com.knackspin.pintu.Database.DatabaseHelper;
import com.knackspin.pintu.Login;
import com.knackspin.pintu.R;
import com.knackspin.pintu.Singleton.Singleton;
import com.knackspin.pintu.utils.UrlPoint;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class FetchAddress extends AppCompatActivity implements LocationListener {


    Button send_address_button;
    DatabaseHelper databaseHelper;
    EditText user_name, user_address,user_street,user_state,user_city,user_zip,user_phone;
    TextView userId_text;
    ImageView fetch_back_image;

    String u_address_id, address;
    public static final String DEFAULT = "";
    SharedPreferences sharedPreferences;

    List<Address> addresses;
    Geocoder geocoder;
    double n_latitude, n_longitude;
    LocationManager locationManager;
    CardView cv_add_address;

    private static final int MY_PERMISSIONS_REQUEST_CODE = 123;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fetch_address);
        initialization();


        sharedPreferences = getSharedPreferences("MyData", Context.MODE_PRIVATE);
        u_address_id = sharedPreferences.getString("userId", DEFAULT);
        userId_text.setText(u_address_id);


        addresses = new ArrayList<>();
        geocoder = new Geocoder(this, Locale.getDefault());



        send_address_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                validateData();
            }
        });


        fetch_back_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });

        cv_add_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getAddress();
            }
        });
    }

    private void validateData() {


                if (user_address.getText().toString().equals(""))
                {
                    Toast.makeText(this, "Please enter house number", Toast.LENGTH_SHORT).show();
                } else
                    {
                        if (user_street.getText().toString().equals(""))
                        {
                            Toast.makeText(this, "Please enter street details", Toast.LENGTH_SHORT).show();
                        } else
                            {
                               checkUserID();
                            }
                    }
    }

    private void checkUserID() {

        if (userId_text.getText().toString() != DEFAULT)
        {
            sendAddressData();
        }
        else
            {

                askUserId();
            }
    }

    private void askUserId() {

        Intent intent = new Intent(FetchAddress.this, Login.class);
        startActivity(intent);
        finish();
    }

    private void sendAddressData() {

        CustomLayout.setUIToWait(FetchAddress.this,true);

        final String username = user_name.getText().toString();
        final String uid = u_address_id;
        final String address = user_address.getText().toString();
        final String street =  user_street.getText().toString();
        final String city = user_city.getText().toString();
        final String state = user_state.getText().toString();
        final String zip = user_zip.getText().toString();


        StringRequest stringRequest = new StringRequest(Request.Method.POST, UrlPoint.fetchAddress, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                CustomLayout.setUIToWait(FetchAddress.this,false);



                     JSONObject jsonObject = null;

                try {
                    jsonObject = new JSONObject(response);

                    if (jsonObject.optString("status").equalsIgnoreCase("success"))
                    {

                        JSONArray jsonArray = jsonObject.optJSONArray("address");

                        for (int k=0; k<jsonArray.length();k++)
                        {
                            JSONObject ob = jsonArray.getJSONObject(k);

                            String u_name = ob.getString("name");
                            String u_address = ob.getString("address");
                            String u_city = ob.getString("city");
                            String u_state = ob.getString("state");
                            String u_street = ob.getString("street");
                            String u_zip = ob.getString("zip");

                            user_name.setText(u_name);
                            user_address.setText(u_address);
                            user_city.setText(u_city);
                            user_state.setText(u_state);
                            user_street.setText(u_street);
                            user_zip.setText(u_zip);
                        }


                        Toast.makeText(FetchAddress.this, "Address added successfully", Toast.LENGTH_SHORT).show();
                        sendAdd();

                    } else
                        {
                            Toast.makeText(FetchAddress.this, "Failed", Toast.LENGTH_SHORT).show();
                        }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                CustomLayout.setUIToWait(FetchAddress.this,false);

                if(error instanceof ServerError)
                {

                    Toast.makeText(FetchAddress.this, "Server Down! Please try again", Toast.LENGTH_SHORT).show();
                }

                else if (error instanceof NetworkError)
                {
                    Toast.makeText(FetchAddress.this, "Check your Internet Connection", Toast.LENGTH_SHORT).show();
                }

                else
                {
                    Toast.makeText(FetchAddress.this, "Something Broken", Toast.LENGTH_SHORT).show();
                }

            }
        })

        {
            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<String, String>();
                params.put("userId",uid);
                params.put("address",address);
                params.put("street", street);
                params.put("city", "");
                params.put("state","");
                params.put("zip","");
                params.put("fName", "");
                return params;
            }

        };

        Singleton.getInstance(FetchAddress.this).addTorequestqu(stringRequest);

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        );

    }

    private void sendAdd() {

        Intent intent = new Intent();
        intent.putExtra("cx_name", user_name.getText().toString());
        intent.putExtra("cx_address", user_address.getText().toString());
        intent.putExtra("cx_state", user_state.getText().toString());
        intent.putExtra("cx_street", user_street.getText().toString());
        intent.putExtra("cx_city", user_city.getText().toString());
        intent.putExtra("cx_zip", user_zip.getText().toString());
        intent.putExtra("cx_phone", user_phone.getText().toString());

        setResult(RESULT_OK, intent);
        finish();
    }


    private void initialization() {

        send_address_button = findViewById(R.id.send_address_button);
        user_name = findViewById(R.id.user_name_model);
        user_address = findViewById(R.id.user_address);
        user_street = findViewById(R.id.user_street);
        user_state = findViewById(R.id.user_state);
        user_city = findViewById(R.id.user_city);
        user_zip = findViewById(R.id.user_zip);
        user_phone = findViewById(R.id.user_phone);
        userId_text = findViewById(R.id.userID_text);
        databaseHelper = new DatabaseHelper(this);
        cv_add_address = findViewById(R.id.cv_add_address);

        fetch_back_image = findViewById(R.id.fetch_back_image);

    }

    @Override
    public void onLocationChanged(Location location) {


        n_longitude = location.getLongitude();
        n_latitude = location.getLatitude();


        Log.d("Location", "Longitude"+ n_longitude + " " +"Latitude" + n_latitude );

        displayAddress();


    }


    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }


    private void displayAddress() {

        try {
            addresses = geocoder.getFromLocation(n_latitude, n_longitude, 1);

            address = addresses.get(0).getAddressLine(0);

            user_street.setText(address);
            Log.d("Address", "Address is "+address);

        } catch (IOException e) {
            e.printStackTrace();
        }



    }


    private void getAddress()
    {

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    Activity#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for Activity#requestPermissions for more details.
                return;
            }
        }

        try {

            Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            onLocationChanged(location);

        }
        catch (NullPointerException ne)
        {
            ne.getMessage();
        }



    }


}


