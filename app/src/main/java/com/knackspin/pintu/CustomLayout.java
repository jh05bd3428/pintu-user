package com.knackspin.pintu;

import android.app.ProgressDialog;
import android.content.Context;

import java.util.Objects;

public class CustomLayout {

    private static ProgressDialog mProgressDialog;

    public static void setUIToWait(final Context context, boolean wait) {

        if (wait) {
            if (mProgressDialog == null) {
                mProgressDialog = ProgressDialog.show(context, null, null);
                mProgressDialog.setContentView(R.layout.custom_layout);
                Objects.requireNonNull(mProgressDialog.getWindow()).setBackgroundDrawableResource(android.R.color.transparent);
            } else {
                mProgressDialog.show();
            }
        } else {
            if (mProgressDialog != null && mProgressDialog.isShowing()) {
                mProgressDialog.dismiss();
                mProgressDialog = null;
            }
        }

    }

}
