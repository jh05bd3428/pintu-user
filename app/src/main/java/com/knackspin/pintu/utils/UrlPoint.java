package com.knackspin.pintu.utils;

import com.knackspin.pintu.RootUrl;

public class UrlPoint extends RootUrl {

 //Login url
 public static final String Login_Url = base_url+"mobileApi/user/login";

 //VerifyPincode url
 public static final String VerifyUrl = base_url+"mobileApi/user/get_provider_details";

 //serach_pincode
 public static final String search_pincode = base_url+"mobileApi/user/search_pincode";
 //shop_image_url
 public static final String shop_image = base_url+"uploads/provider/";


 //firstTimeRegister
 public static final String firstTimeRegister = base_url+"mobileApi/user/register";

 //verifyUserOtp
 public static final String verifyUserOtp = base_url+"mobileApi/user/verify_otp";

 //updateProfile
 public static final String updateProfile = base_url+"mobileApi/user/update_profile";

 //Registration Url
 public static final String Registration_Url = base_url+"mobileApi/user/register";

 //registration_otp
 public static final String register_otp = base_url+"mobileApi/user/getOtp";

 //placeOrderUrl
 public static final String placeOrderApi = base_url+"mobileApi/user/placeorder";

 //BannerUrl
 public static final String getBanner = base_url+"mobileApi/user/banner_image";
 //path for banner image folder
 public static final String banner_path = base_url+"uploads/banner_image/";

// ViewCategoryUrl
 public static final String getCategory = base_url+"mobileApi/user/mobileHomePage";

 //Random Category
 public static final String randomCategory = "http://ryes.in/mobileApi/user/get_random_sub_category";
 //RandomCategoryImagePath
 public static final String randomImagePath = base_url+"uploads/category/";

 //subcategoryUrl
 public static final String getSubcategory = base_url+"mobileApi/user/get_subcategory";

 //getProductUrl
 public static final String getProduct = base_url+"mobileApi/user/get_products?page=";
 //getProductImage
 public static final String getProductImage = base_url+"uploads/products/";


 //orderHistoryUrl
 public static final String getOrderHistory = base_url+"mobileApi/user/get_orders";

 //searchProductUrl
 public static final String getSearchProduct = base_url+"mobileApi/user/product_search";

 //FetchAddress
 public static final String fetchAddress = base_url+"mobileApi/user/update_address";


 //rewardImagePath
 public static final String rewardImagePath = base_url+"uploads/products/";


 //checkRewardUrl
 public static final String checkRewardUrl = base_url+"mobileApi/user/get_reward_product";
 //getRewardUrl
 public static final String getRewardUrl = base_url+"mobileApi/user/get_total_reward_point";

 //walletUrl
 public static final String getWalletPoints = base_url+"mobileApi/user/get_wallet";

 //phone_verify_forgot_password
 public static final String checkMobileNumber = base_url+"mobileApi/user/resend_otp";

 //passwordResetApi
 public static final String changPassword = base_url+"mobileApi/user/password_reset";

 //homepageapi
 public static final String homePageApi = base_url+"mobileApi/user/mobileHomePage";
 //category_image
 public static final String categoryImage = base_url + "uploads/category/";

 //getSavedAddress
 public static final String getUserAddress = base_url+"mobileApi/user/get_user_address";

 //viewOrderDetails
 public  static final String statusUrl = base_url+"mobileApi/user/get_single_order_detail";
 //image
 public static final String statusImage = base_url+"uploads/products/";

//feedbackUrl
 public static final String feedback = base_url+"mobileApi/user/user_feedback";

//cancelOrderApi
public static final String cancelOrder = base_url+"mobileApi/user/cancel_order";

//showCouponApi
 public static final String showCoupon = base_url+"mobileApi/user/promolist";

 //checkCouponCode
 public static final String checkCouponCode = base_url+"mobileApi/user/mobilecheck_promocode";


 //showOfferProductApi
 public static final String showOfferProduct = base_url+"mobileApi/user/offer_products";

 //notification_path

 public static final String notificationPath = base_url+"uploads/notification/";

 //getServisablePincode
 public static final String getServisablePincode = base_url+"/mobileApi/user/all_active_provider";

}

