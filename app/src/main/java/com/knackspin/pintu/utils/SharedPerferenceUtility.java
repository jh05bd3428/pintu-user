package com.knackspin.pintu.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPerferenceUtility {

    private Context context;
     SharedPreferences sharedPreferences;
    public SharedPerferenceUtility(Context context){
        this.context = context;
        sharedPreferences = context.getSharedPreferences("RYSE",Context.MODE_PRIVATE);
    }

    public void setUserID (String userid)
    {
        sharedPreferences.edit().putString("userid", userid).apply();
    }

    public String getUserID ()
    {
        return sharedPreferences.getString("userid","");
    }
}
