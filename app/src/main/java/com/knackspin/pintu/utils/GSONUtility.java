package com.knackspin.pintu.utils;

import com.google.gson.Gson;

public class GSONUtility {

   private final static Gson gson = new Gson();

   public static <T>  T desearlize(String response,Class<T> tClass){
       return gson.fromJson(response,tClass);
   }
}
