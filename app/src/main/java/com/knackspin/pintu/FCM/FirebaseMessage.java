package com.knackspin.pintu.FCM;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import androidx.core.app.NotificationCompat;
import android.text.Html;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.knackspin.pintu.HomeScreen;
import com.knackspin.pintu.R;
import com.knackspin.pintu.utils.UrlPoint;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class FirebaseMessage extends FirebaseMessagingService {


    private static final String TAG = "FirebaseIDService";
    public static final int NOTIFICATION_ID_BIG_IMAGE = 101;


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        if (remoteMessage.getData().size()> 0 )
        {
            Log.d("fcm", remoteMessage.getData().toString());

            if (remoteMessage.getData().get("image").isEmpty())
            {
                  sendNotification(remoteMessage.getData().get("title"), remoteMessage.getData().get("body"));
            } else
                {
                    Bitmap bitmap = getBitmapFromURL(UrlPoint.notificationPath + remoteMessage.getData().get("image"));
                    showImageNotification(bitmap, remoteMessage.getData().get("title"), remoteMessage.getData().get("body"));
                }

        }

    }

    private void sendNotification(String title, String body) {
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel mChannel = new NotificationChannel(
                    "IVN_1", "IV", NotificationManager.IMPORTANCE_HIGH);
            notificationManager.createNotificationChannel(mChannel);
        }


        Intent intent = new Intent(this, HomeScreen.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_ONE_SHOT);


        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, "IVN_1")
                .setContentTitle(title)
                .setWhen(System.currentTimeMillis())
                .setContentText(body)
                .setAutoCancel(true)
                .setPriority(Notification.PRIORITY_HIGH)
                .setDefaults(Notification.DEFAULT_ALL)
                .setLargeIcon(BitmapFactory.decodeResource(this.getResources(), R.drawable.small_pintu))
                .setSmallIcon(R.drawable.ic_notifsmall_name)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(body))
                .setContentIntent(pendingIntent);

        notificationManager.notify(0, notificationBuilder.build());

    }

    private void showImageNotification(Bitmap bitmap, String title, String body) {

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel mChannel = new NotificationChannel(
                    "IVN_2", "IV", NotificationManager.IMPORTANCE_HIGH);
            notificationManager.createNotificationChannel(mChannel);
        }

        NotificationCompat.BigPictureStyle bigPictureStyle = new NotificationCompat.BigPictureStyle();
        bigPictureStyle.setBigContentTitle(title);
        bigPictureStyle.setSummaryText(Html.fromHtml(body).toString());
        bigPictureStyle.bigPicture(bitmap);
        Intent intent = new Intent(this, HomeScreen.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_ONE_SHOT);
        Notification notification;
        notification = new NotificationCompat.Builder(
                this, "IVN_2").setTicker(title)
                .setAutoCancel(true)
                .setContentTitle(title)
                .setContentIntent(pendingIntent)
                .setPriority(Notification.PRIORITY_HIGH)
                .setDefaults(Notification.DEFAULT_ALL)
                .setStyle(bigPictureStyle)
                .setLargeIcon(BitmapFactory.decodeResource(this.getResources(), R.drawable.small_pintu))
                .setSmallIcon(R.drawable.ic_notifsmall_name)
                .setStyle(bigPictureStyle)
                .setContentText(body)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(body))
                .build();

        notificationManager.notify(NOTIFICATION_ID_BIG_IMAGE, notification);

    }


    private Bitmap getBitmapFromURL(String image) {

        try {
            URL url = new URL(image);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);

        String new_token = FirebaseInstanceId.getInstance().getToken();

        SharedPreferences sharedPreferences = getSharedPreferences("MyData",Context.MODE_PRIVATE); // creating xml file to store data
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString("newToken", new_token);
        editor.apply();

    }
}
