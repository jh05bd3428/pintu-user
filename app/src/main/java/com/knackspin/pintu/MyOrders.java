package com.knackspin.pintu;

import android.content.Context;
import android.content.SharedPreferences;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.knackspin.pintu.Adapter.MyOrderAdapter;
import com.knackspin.pintu.Model.MyOrderModel;
import com.knackspin.pintu.Singleton.Singleton;
import com.knackspin.pintu.utils.ConnectionCheck;
import com.knackspin.pintu.utils.UrlPoint;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MyOrders extends AppCompatActivity {

    String u_id;
    TextView order_user_id;

    public static final String DEFAULT = "NA";

    ArrayList<MyOrderModel> listOfOrders;
    RecyclerView recyclerView;
    MyOrderAdapter myOrderAdapter;
    LinearLayoutManager linearLayoutManager;
    ImageView orders_back_image;
    RelativeLayout noConnectionLayout;
    Button retry_button;

    LinearLayout noOrdersLayout;

    //FrameLayout frameLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_orders);

        initialization();

        connectionCheck();

        listOfOrders = new ArrayList<>();

        SharedPreferences sharedPreferences = getSharedPreferences("MyData", Context.MODE_PRIVATE);
        u_id = sharedPreferences.getString("userId", DEFAULT);
        order_user_id.setText(u_id);


        recyclerView.setHasFixedSize(true);
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        myOrderAdapter = new MyOrderAdapter(listOfOrders, this);

        orders_back_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });

        retry_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                checkConnectionBack();
            }
        });


    }

    private void checkConnectionBack() {

        connectionCheck();

    }

    private void initialization() {

        retry_button = findViewById(R.id.retry_button);
        noConnectionLayout = findViewById(R.id.noConnectionLayout);
        recyclerView = findViewById(R.id.my_orders_recylerview);
        order_user_id = findViewById(R.id.order_user_id);
        orders_back_image = findViewById(R.id.orders_back_image);

        noOrdersLayout = findViewById(R.id.noOrdersLayout);
    }

    private void connectionCheck() {

        if(new ConnectionCheck(this).isNetworkAvailable())
        {


            noConnectionLayout.setVisibility(View.GONE);

            checkUserId();


            // Toast.makeText(HomeScreen.this, "Having connection", Toast.LENGTH_SHORT).show();

        } else
        {

            recyclerView.setVisibility(View.GONE);
            noConnectionLayout.setVisibility(View.VISIBLE);
            // Toast.makeText(HomeScreen.this, "Please check your Internet connection", Toast.LENGTH_SHORT).show();
        }



    }

    private void checkUserId() {


        if (order_user_id.getText().toString() != DEFAULT)
        {
            showOrders();

        } else
            {
                Toast.makeText(this, "Please login to View Orders", Toast.LENGTH_LONG).show();
                noOrdersLayout.setVisibility(View.GONE);

            }

    }

    private void showOrders() {

        recyclerView.setVisibility(View.VISIBLE);
        getOrders();

    }

    private void getOrders() {


        CustomLayout.setUIToWait(MyOrders.this, true);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, UrlPoint.getOrderHistory, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                CustomLayout.setUIToWait(MyOrders.this, false);
                noOrdersLayout.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);


                JSONObject jsonObject = null;

                if (!listOfOrders.isEmpty())
                    listOfOrders.clear();

                try {
                    jsonObject = new JSONObject(response);

                    if(jsonObject.optString("flag"). equalsIgnoreCase("true"))
                    {

                        JSONArray jsonArray = jsonObject.getJSONArray("order_details");

                        for (int m=0; m < jsonArray.length();m++)
                        {
                            JSONObject ob5 = jsonArray.getJSONObject(m);
                            MyOrderModel myOrderModel = new MyOrderModel(ob5.optString("order_id"),ob5.optString("grand_total"),ob5.getString("created_at"),ob5.getString("status"));
                            listOfOrders.add(myOrderModel);
                        }

                        recyclerView.setAdapter(myOrderAdapter);
                    }else
                    {
                        noOrdersLayout.setVisibility(View.VISIBLE);
                        recyclerView.setVisibility(View.GONE);
                       // Toast.makeText(MyOrders.this, "No order found", Toast.LENGTH_SHORT).show();

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                CustomLayout.setUIToWait(MyOrders.this, false);

                Toast.makeText(MyOrders.this, error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        })

        {
            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id",u_id);
                return params;
            }

        };

        Singleton.getInstance(MyOrders.this).addTorequestqu(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        );

    }

    @Override
    protected void onResume() {
        super.onResume();

        Log.d("Check", "Resume");

        checkUserId();
    }
}
