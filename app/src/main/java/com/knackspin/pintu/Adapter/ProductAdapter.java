package com.knackspin.pintu.Adapter;

import android.content.Context;
import android.graphics.Paint;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;

import com.bumptech.glide.Glide;
import com.knackspin.pintu.Database.DatabaseHelper;
import com.knackspin.pintu.Model.DialogModel;
import com.knackspin.pintu.Model.Product_model;
import com.knackspin.pintu.R;
import com.knackspin.pintu.SubMenu;
import com.knackspin.pintu.fragments.ProductVariantDialogFragment;
import com.knackspin.pintu.fragments.Product_fragment;
import com.knackspin.pintu.interfaces.ProductModelSetData;
import com.knackspin.pintu.utils.UrlPoint;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ViewHolder> {

    public static final String TAG = ProductAdapter.class.getSimpleName();

    private List<Product_model> productModel;
    private List<DialogModel> spinerModel;
    private Product_fragment mContext;
    SubMenu subMenu;
    private Context context;
    DatabaseHelper myDb;
    ViewHolder holder;
    ProductAdapter productAdapter;


    int spinnerPosition = 0;
    int spinnerPos1 = 0;
    DecimalFormat form;

    public ProductAdapter(List<Product_model> productModel, Product_fragment mContext, Context context) {
        this.productModel = productModel;
        this.mContext = mContext;
        this.context = context;
        myDb = new DatabaseHelper(context);
        subMenu = (SubMenu) mContext.getActivity();

        form = new DecimalFormat("0.00");
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.fresh_demo, viewGroup, false);

        holder = new ViewHolder(view);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int position) {

        final Product_model product_model = productModel.get(position);

        viewHolder.displayQuantity.setText(myDb.getProdQty(product_model.getProduct_id()));

        viewHolder.product_id.setText(product_model.getProduct_id());
        viewHolder.brand_name.setText(product_model.getBrand_name());
        viewHolder.product_name.setText(product_model.getProduct_name());
        Glide.with(context).load(UrlPoint.getProductImage + product_model.getProduct_image()).into(viewHolder.product_image);

        spinerModel = product_model.getDialogModel();

        if (spinnerPosition == 0) {
            viewHolder.variant_id.setText(spinerModel.get(spinnerPosition).getVariant_id());
            viewHolder.variant_weight.setText(spinerModel.get(spinnerPosition).getVariant_name());
            viewHolder.actual_price.setText("Mrp" + "-" + "\u20B9" + spinerModel.get(spinnerPosition).getMrp_price());
           // viewHolder.offered_price.setText((spinerModel.get(spinnerPosition).getOffered_price()));

            viewHolder.offered_price.setText("\u20B9"+form.format(Double.parseDouble(spinerModel.get(spinnerPosition).getOffered_price())));

        }

        if (!myDb.getProdVariant(product_model.getProduct_id()).equalsIgnoreCase("false")) {

            for (int v=0; v<spinerModel.size(); v++){

                if (spinerModel.get(v).getVariant_id().equalsIgnoreCase(myDb.getProdVariant(product_model.getProduct_id()))){

                    viewHolder.variant_id.setText(spinerModel.get(v).getVariant_id());
                    viewHolder.variant_weight.setText(spinerModel.get(v).getVariant_name());
                    viewHolder.actual_price.setText("Mrp" + "-" + "\u20B9" + spinerModel.get(v).getMrp_price());
                    viewHolder.offered_price.setText("\u20B9" + spinerModel.get(v).getOffered_price());

                }
            }
        }

        viewHolder.select_arrow_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final int pos = viewHolder.getAdapterPosition();
                ArrayList<DialogModel> listSpinners = (ArrayList<DialogModel>) productModel.get(pos).getDialogModel();


                ProductModelSetData modelSetData = new ProductModelSetData() {
                    @Override
                    public void setProductModel(int spinnerPos, int productPos) {
                        List<DialogModel> spinnerModel = productModel.get(pos).getDialogModel();
                        viewHolder.variant_id.setText(spinnerModel.get(spinnerPos).getVariant_id());
                        viewHolder.variant_weight.setText(spinnerModel.get(spinnerPos).getVariant_name());
                        viewHolder.actual_price.setText("Mrp" + "-" + "\u20B9" + spinnerModel.get(spinnerPos).getMrp_price());
                        viewHolder.offered_price.setText("\u20B9" + spinnerModel.get(spinnerPos).getOffered_price());
                        spinnerPos1 = spinnerPos;
                    }
                };

                ProductVariantDialogFragment productVariantDialogFragment = new ProductVariantDialogFragment();
                Bundle bundle = new Bundle();
                bundle.putString("PROD_ID", product_model.getProduct_id());
                bundle.putSerializable("LIST", listSpinners);
                bundle.putSerializable("MODEL", modelSetData);
                productVariantDialogFragment.setArguments(bundle);

                productVariantDialogFragment.show(mContext.getChildFragmentManager(), TAG);

            }
        });


        viewHolder.increment.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View v) {


                String increment_value = viewHolder.displayQuantity.getText().toString();
                int int_value = Integer.parseInt(increment_value);
                int_value++;
                viewHolder.displayQuantity.setText(String.valueOf(int_value));

                int adapterPos = viewHolder.getAdapterPosition();
                String variant_id = productModel.get(position).getDialogModel().get(spinnerPos1).getVariant_id();


                try {

                    String pid = productModel.get(position).getProduct_id();
                    String p_b_name = productModel.get(position).getBrand_name();
                    String p_p_name = productModel.get(position).getProduct_name();

                    String p_v_weight = productModel.get(position).getDialogModel().get(spinnerPos1).getVariant_name();
                    String p_a_price = productModel.get(position).getDialogModel().get(spinnerPos1).getMrp_price();
                    String p_o_price = productModel.get(position).getDialogModel().get(spinnerPos1).getOffered_price();
                    String vid = productModel.get(position).getDialogModel().get(spinnerPos1).getVariant_id();


                    int p_quantity = int_value;
                    String productUrl = UrlPoint.getProductImage + productModel.get(position).getProduct_image();

                    check_details(productUrl, pid, vid, p_b_name, p_p_name, p_v_weight, p_a_price, p_o_price, String.valueOf(p_quantity));
                }

                catch (IndexOutOfBoundsException ioe)
                {
                    ioe.getMessage();
                }

                String quantity = String.valueOf(int_value);
                Log.d("VariantId", variant_id);
                Log.d("VariantId", String.valueOf(adapterPos));
                Log.d("VariantId", quantity);




            }
        });

        viewHolder.decrement.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View v) {

                String decrement_value = viewHolder.displayQuantity.getText().toString();
                int int_dec_value = Integer.parseInt(decrement_value);

                try
                {
                    String pid = productModel.get(position).getProduct_id();
                    String p_b_name = productModel.get(position).getBrand_name();
                    String p_p_name = productModel.get(position).getProduct_name();

                    String p_v_weight = productModel.get(position).getDialogModel().get(spinnerPos1).getVariant_name();
                    String p_a_price = productModel.get(position).getDialogModel().get(spinnerPos1).getMrp_price();
                    String p_o_price = productModel.get(position).getDialogModel().get(spinnerPos1).getOffered_price();
                    String vid = productModel.get(position).getDialogModel().get(spinnerPos1).getVariant_id();

                    //  int p_quantity = int_dec_value;
                    String productUrl = UrlPoint.getProductImage + productModel.get(position).getProduct_image();

                    if (int_dec_value > 1) {
                        int_dec_value--;
                        viewHolder.displayQuantity.setText(String.valueOf(int_dec_value));
                        check_details(productUrl, pid, vid, p_b_name, p_p_name, p_v_weight, p_a_price, p_o_price, String.valueOf(int_dec_value));
                    }else if (int_dec_value == 1){
                        myDb.delete_data(String.valueOf(pid));
                        viewHolder.displayQuantity.setText("0");
                        subMenu.updateCartCount();
                    }
                }

                catch (IndexOutOfBoundsException ioe)
                {
                    ioe.getMessage();
                }



            }
        });
    }


    private void check_details(String product_image, String pid, String vid, String p_b_name, String p_p_name, String p_v_weight, String p_a_price, String p_o_price, String p_quantity) {

        boolean isChecked = myDb.checkDetails(pid);

        if (isChecked) {
            update_data(pid, p_b_name, p_p_name, p_v_weight, p_a_price, p_o_price, p_quantity);
            // Toast.makeText(context, "updated", Toast.LENGTH_SHORT).show();

        } else {
            insert_data(product_image, vid, pid, p_b_name, p_p_name, p_v_weight, p_a_price, p_o_price, p_quantity);
            //   Toast.makeText(context, "Inserted", Toast.LENGTH_SHORT).show();
            subMenu.updateCartCount();

        }
    }

    private void update_data(String pid, String p_b_name, String p_p_name, String p_v_weight, String p_a_price, String p_o_price, String p_quantity) {

        boolean isUpdated = myDb.updateData(pid, p_b_name, p_p_name, p_v_weight, p_a_price, p_o_price, p_quantity);
        if (isUpdated) {
            //Toast.makeText(context, "", Toast.LENGTH_SHORT).show();
        } else {
            //  Toast.makeText(context, "", Toast.LENGTH_SHORT).show();
        }
    }

    private void insert_data(String product_image, String pid, String vid, String p_b_name, String p_p_name, String p_v_weight, String p_a_price, String p_o_price, String p_quantity) {

        boolean isInserted = myDb.insert_data(product_image, vid, pid, p_b_name, p_p_name, p_v_weight, p_a_price, p_o_price, p_quantity);
        if (isInserted) {
            //Log.d("","");
        } else {
        }

    }


    @Override
    public int getItemCount() {
        return productModel.size();
    }

    @Override
    public long getItemId(int position)
    {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return (position);
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView product_id;
        TextView brand_name;
        TextView product_name;
        ImageView product_image;
        TextView variant_name;
        TextView actual_price;
        TextView offered_price;
        ImageView increment;
        ImageView decrement;
        TextView displayQuantity;
        TextView variant_weight;
        TextView variant_id;
        ImageView arrow_down;
        LinearLayout select_arrow_layout;


        public ViewHolder(@NonNull final View itemView) {
            super(itemView);

            select_arrow_layout =itemView.findViewById(R.id.select_arrow_layout);
            brand_name = itemView.findViewById(R.id.brand_name);
            product_name = itemView.findViewById(R.id.product_name);
            product_image = itemView.findViewById(R.id.product_image);
            variant_name = itemView.findViewById(R.id.variant_name);
            increment = itemView.findViewById(R.id.increment);
            decrement = itemView.findViewById(R.id.decrement);
            displayQuantity = itemView.findViewById(R.id.displayQuantity);
            //dialog_product_type = itemView.findViewById(R.id.dialog_prod_type);
            actual_price = itemView.findViewById(R.id.actual_price);
            offered_price = itemView.findViewById(R.id.offered_price);
            variant_weight = itemView.findViewById(R.id.variant_weight);
            product_id = itemView.findViewById(R.id.product_id);
            variant_id = itemView.findViewById(R.id.variant_id);
            arrow_down = itemView.findViewById(R.id.select_arrow);

            actual_price.setPaintFlags(actual_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

        }
    }

}

//cat id -5
//product_id - 46