package com.knackspin.pintu.Adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.knackspin.pintu.Model.AddressModel;
import com.knackspin.pintu.R;

import java.util.List;

public class AddressAdapter extends RecyclerView.Adapter<AddressAdapter.ViewHolder> {

    private List<AddressModel> addressModels;
    private Context context;

    public AddressAdapter(List<AddressModel> addressModels, Context context) {
        this.addressModels = addressModels;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int position) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.address_model,viewGroup,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {

        final AddressModel address = addressModels.get(position);

        viewHolder.full_name.setText(address.getFull_name());
        viewHolder.first_address.setText(address.getAddress() + "," + address.getStreet() );
        viewHolder.second_address.setText(address.getCity()+ "," + address.getState()+ ","+address.getZip());
        viewHolder. phone_number.setText(address.getPhone_number());
    }

    @Override
    public int getItemCount() {
        return addressModels.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView full_name;
        TextView first_address;
        TextView second_address;
        TextView phone_number;



        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            full_name = itemView.findViewById(R.id.user_name_model);
            first_address = itemView.findViewById(R.id.first_address);
            second_address = itemView.findViewById(R.id.second_address);
            phone_number = itemView.findViewById(R.id.address_phone_number);
           /* address = itemView.findViewById(R.id.address_model);
            state = itemView.findViewById(R.id.address_state);
            city = itemView.findViewById(R.id.address_city);
            street = itemView.findViewById(R.id.address_street);
            zip = itemView.findViewById(R.id.address_zip);
            phone_number = itemView.findViewById(R.id.address_phone);*/
        }
    }
}

