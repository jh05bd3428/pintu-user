package com.knackspin.pintu.Adapter;

import android.content.Context;
import android.graphics.Paint;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.knackspin.pintu.Database.DatabaseHelper;
import com.knackspin.pintu.Model.OfferDialogModel;
import com.knackspin.pintu.Model.OfferNotifModel;
import com.knackspin.pintu.NotifOffers;
import com.knackspin.pintu.R;
import com.knackspin.pintu.fragments.OfferDialogFragmnet;
import com.knackspin.pintu.interfaces.OfferNotifModelSetData;
import com.knackspin.pintu.utils.UrlPoint;

import java.util.ArrayList;
import java.util.List;

public class OfferNotifAdapter extends RecyclerView.Adapter<OfferNotifAdapter.ViewHolder> {

    private List<OfferNotifModel> offerNotifModels;
    private Context context;

    private List<OfferDialogModel> offerDialogModels;
    private  NotifOffers mContext;

    private NotifOffers notifOffers;
    DatabaseHelper myDb;

    int dialogspinnerPosition= 0;
    int dialogspinnerPos1 = 0;

    public static final String TAG = OfferNotifAdapter.class.getSimpleName();

    String serach_img = "http://ryes.in/uploads/products/";





    public OfferNotifAdapter(List<OfferNotifModel> offerNotifModels, Context context, NotifOffers mContext ) {
        this.offerNotifModels = offerNotifModels;
        this.context = context;
        this.mContext = mContext;

        myDb = new DatabaseHelper(context);
        notifOffers = (NotifOffers) mContext;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int pos) {

        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.offer_notif_model, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int pos) {

        final OfferNotifModel offerNotifModel = offerNotifModels.get(pos);

        viewHolder.product_id.setText(offerNotifModel.getOffer_product_id());
        viewHolder.brand_name.setText(offerNotifModel.getOffer_brand_name());
        viewHolder.product_name.setText(offerNotifModel.getOffer_product_name());
        Glide.with(context).load(UrlPoint.getProductImage + offerNotifModel.getOffer_product_image()).into(viewHolder.product_image);

        offerDialogModels = offerNotifModel.getDialogModels();

        if (dialogspinnerPosition == 0)
        {
            viewHolder.variant_id.setText(offerDialogModels.get(dialogspinnerPosition).getOffer_dialog_variant_id());
            viewHolder.variant_weight.setText(offerDialogModels.get(dialogspinnerPosition).getOffer_dialog_variant_name());
            viewHolder.actual_price.setText("Mrp" + "-" + "\u20B9" + offerDialogModels.get(dialogspinnerPosition).getOffer_dialog_mrp_price());
            viewHolder.offered_price.setText("\u20B9" + offerDialogModels.get(dialogspinnerPosition).getOffer_dialog_offered_price());

        }


        if (!myDb.getProdVariant(offerNotifModel.getOffer_product_id()).equalsIgnoreCase("false")) {

            for (int v=0; v<offerDialogModels.size(); v++){

                if (offerDialogModels.get(v).getOffer_dialog_variant_id().equalsIgnoreCase(myDb.getProdVariant(offerNotifModel.getOffer_product_id()))){

                    viewHolder.variant_id.setText(offerDialogModels.get(v).getOffer_dialog_variant_id());
                    viewHolder.variant_weight.setText(offerDialogModels.get(v).getOffer_dialog_variant_name());
                    viewHolder.actual_price.setText("Mrp" + "-" + "\u20B9" + offerDialogModels.get(v).getOffer_dialog_mrp_price());
                    viewHolder.offered_price.setText("\u20B9" + offerDialogModels.get(v).getOffer_dialog_offered_price());

                    //viewHolder.displayQuantity.setText("0");
                }
            }
        }

        viewHolder.select_arrow_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final int pos = viewHolder.getAdapterPosition();
                ArrayList<OfferDialogModel> listOfSpinners = (ArrayList<OfferDialogModel>) offerNotifModels.get(pos).getDialogModels();

                OfferNotifModelSetData offerNotifModelSetData = new OfferNotifModelSetData() {
                    @Override
                    public void setOfferModel(int spinnerPos, int productPos) {

                        List<OfferDialogModel> spinnerModel = offerNotifModels.get(pos).getDialogModels();
                        viewHolder.variant_id.setText(spinnerModel.get(spinnerPos).getOffer_dialog_variant_id());
                        viewHolder.variant_weight.setText(spinnerModel.get(spinnerPos).getOffer_dialog_variant_name());
                        viewHolder.actual_price.setText("Mrp"+ "-"+"\u20B9"+spinnerModel.get(spinnerPos).getOffer_dialog_mrp_price());
                        viewHolder.offered_price.setText("\u20B9"+spinnerModel.get(spinnerPos).getOffer_dialog_offered_price());

                        dialogspinnerPos1 = spinnerPos;

                    }
                };


                OfferDialogFragmnet offerDialogFragmnet = new OfferDialogFragmnet();
                Bundle bundle = new Bundle();
                bundle.putString("prod_id", offerNotifModel.getOffer_product_id());
                bundle.putSerializable("list", listOfSpinners);
                bundle.putSerializable("model", offerNotifModelSetData);
                offerDialogFragmnet.setArguments(bundle);

                offerDialogFragmnet.show((mContext).getSupportFragmentManager(),TAG);


            }
        });


        viewHolder.increment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String increment_value = viewHolder.displayQuantity.getText().toString();
                int int_value = Integer.parseInt(increment_value);
                int_value++;
                viewHolder.displayQuantity.setText(String.valueOf(int_value));



                String pid = offerNotifModels.get(pos).getOffer_product_id();
                String p_b_name = offerNotifModels.get(pos).getOffer_brand_name();
                String p_p_name = offerNotifModels.get(pos).getOffer_product_name();

                String p_v_weight = offerNotifModels.get(pos).getDialogModels().get(dialogspinnerPos1).getOffer_dialog_variant_name();
                String p_a_price =  offerNotifModels.get(pos).getDialogModels().get(dialogspinnerPos1).getOffer_dialog_mrp_price();
                String vid =  offerNotifModels.get(pos).getDialogModels().get(dialogspinnerPos1).getOffer_dialog_variant_id();
                String p_o_price =  offerNotifModels.get(pos).getDialogModels().get(dialogspinnerPos1).getOffer_dialog_offered_price();

                int p_quantity = int_value;
                String productUrl = serach_img + offerNotifModels.get(pos).getOffer_product_image();

                check_details (productUrl,pid,vid,p_b_name,p_p_name,p_v_weight,p_a_price,p_o_price, String.valueOf(p_quantity));


            }
        });

        viewHolder.decrement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String decrement_value = viewHolder.displayQuantity.getText().toString();
                int int_dec_value = Integer.parseInt(decrement_value);

                String pid = offerNotifModels.get(pos).getOffer_product_id();
                String p_b_name = offerNotifModels.get(pos).getOffer_brand_name();
                String p_p_name = offerNotifModels.get(pos).getOffer_product_name();

                String p_v_weight = offerNotifModels.get(pos).getDialogModels().get(dialogspinnerPos1).getOffer_dialog_variant_name();
                String p_a_price =  offerNotifModels.get(pos).getDialogModels().get(dialogspinnerPos1).getOffer_dialog_mrp_price();
                String vid =  offerNotifModels.get(pos).getDialogModels().get(dialogspinnerPos1).getOffer_dialog_variant_id();
                String p_o_price =  offerNotifModels.get(pos).getDialogModels().get(dialogspinnerPos1).getOffer_dialog_offered_price();

               // int p_quantity = int_dec_value;
                String productUrl = serach_img + offerNotifModels.get(pos).getOffer_product_image();

                if (int_dec_value > 1) {
                    int_dec_value--;
                    viewHolder.displayQuantity.setText(String.valueOf(int_dec_value));
                   check_details (productUrl,pid,vid,p_b_name,p_p_name,p_v_weight,p_a_price,p_o_price, String.valueOf(int_dec_value));
                }else if (int_dec_value == 1){
                    myDb.delete_data(String.valueOf(pid));
                    viewHolder.displayQuantity.setText("0");
                    notifOffers.updateCartCount();
                }
            }
        });
    }


    private void check_details(String product_image,String pid,String vid, String p_b_name, String p_p_name, String p_v_weight, String p_a_price, String p_o_price, String p_quantity)
    {

        myDb = new DatabaseHelper(context);
        boolean isChecked = myDb.checkDetails(pid);

        if (isChecked)
        {
            update_data(pid,p_b_name,p_p_name,p_v_weight,p_a_price,p_o_price,p_quantity);
            //Toast.makeText(context, "updated", Toast.LENGTH_SHORT).show();

        } else
        {
            insert_data(product_image, vid,pid,p_b_name,p_p_name,p_v_weight,p_a_price,p_o_price,p_quantity);
            notifOffers.updateCartCount();
            //Toast.makeText(context, "Inserted", Toast.LENGTH_SHORT).show();
        }

    }

    private void update_data(String pid, String p_b_name, String p_p_name, String p_v_weight, String p_a_price, String p_o_price, String p_quantity)
    {

        myDb = new DatabaseHelper(context);
        boolean isUpdated = myDb.updateData(pid,p_b_name,p_p_name,p_v_weight,p_a_price,p_o_price,p_quantity);
        if (isUpdated)
        {
            //Toast.makeText(context, "", Toast.LENGTH_SHORT).show();
        }else
        {
            //  Toast.makeText(context, "", Toast.LENGTH_SHORT).show();
        }
    }

    private void insert_data (String product_image,String pid,String vid, String p_b_name, String p_p_name, String p_v_weight, String p_a_price, String p_o_price, String p_quantity)
    {

        myDb = new DatabaseHelper(context);
        boolean isInserted = myDb.insert_data(product_image,vid,pid,p_b_name,p_p_name,p_v_weight,p_a_price,p_o_price,p_quantity);
        if (isInserted)
        {
            //Log.d("","");
        }

        else
        {
        }

    }



    @Override
    public int getItemCount() {
        return offerNotifModels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {

        TextView product_id;
        TextView brand_name;
        TextView product_name;
        ImageView product_image;
        TextView variant_name;
        TextView actual_price;
        TextView offered_price;
        ImageView increment;
        ImageView decrement;
        TextView displayQuantity;
        TextView variant_weight;
        TextView variant_id;
        ImageView arrow_down;
        LinearLayout select_arrow_layout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);


            select_arrow_layout =itemView.findViewById(R.id.offer_select_arrow_layout);
            brand_name = itemView.findViewById(R.id.offer_brand_name);
            product_name = itemView.findViewById(R.id.offer_product_name);
            product_image = itemView.findViewById(R.id.offer_product_image);
            variant_name = itemView.findViewById(R.id.offer_dialog_variant_name);
            increment = itemView.findViewById(R.id.offer_increment);
            decrement = itemView.findViewById(R.id.offer_decrement);
            displayQuantity = itemView.findViewById(R.id.offer_displayQuantity);
            actual_price = itemView.findViewById(R.id.offer_actual_price);
            offered_price = itemView.findViewById(R.id.offer_offered_price);
            variant_weight = itemView.findViewById(R.id.offer_variant_weight);
            product_id = itemView.findViewById(R.id.offer_product_id);
            variant_id = itemView.findViewById(R.id.offer_variant_id);
            arrow_down = itemView.findViewById(R.id.offer_select_arrow);

            actual_price.setPaintFlags(actual_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }
    }
}
