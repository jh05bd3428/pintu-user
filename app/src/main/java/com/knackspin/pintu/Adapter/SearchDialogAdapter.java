package com.knackspin.pintu.Adapter;

import android.content.Context;
import android.graphics.Paint;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.knackspin.pintu.Database.DatabaseHelper;
import com.knackspin.pintu.Model.SearchDialogModel;
import com.knackspin.pintu.R;
import com.knackspin.pintu.interfaces.SearchFragmnetSetData;

import java.util.List;

public class SearchDialogAdapter extends RecyclerView.Adapter<SearchDialogAdapter.ViewHolder> {

    private String dialogQwerty;
    private List<SearchDialogModel> dialogSpinerModel;
    private Context context;
    private SearchFragmnetSetData searchFragmnetSetData;

    DatabaseHelper myDb;
    ViewHolder holder;

    public SearchDialogAdapter(String dialogQwerty, List<SearchDialogModel> dialogSpinerModel, Context context, SearchFragmnetSetData searchFragmnetSetData) {
        this.dialogQwerty = dialogQwerty;
        this.dialogSpinerModel = dialogSpinerModel;
        this.context = context;
        this.searchFragmnetSetData = searchFragmnetSetData;
    }




    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.search_fragmnet_dialog,viewGroup,false);
        return new ViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, int i) {

        final SearchDialogModel searchDialogModel = dialogSpinerModel.get(i);

        viewHolder.search_dialog_variant_id.setText(searchDialogModel.getDialog_variant_id());
        viewHolder.search_dialog_variant_name.setText(searchDialogModel.getDialog_variant_name());
        viewHolder.search_dialog_actual_mrp.setText("Mrp"+" "+"\u20B9" +  searchDialogModel.getDialog_mrp_price());
        viewHolder.search_dialog_offer_price.setText("\u20B9" + searchDialogModel.getDialog_offered_price());

        viewHolder.search_full_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int pos = viewHolder.getAdapterPosition();
                searchFragmnetSetData.setData(pos, 0);
                Log.d("SearchDialogAdapter", dialogSpinerModel.get(pos).toString());

            }
        });

    }

    @Override
    public int getItemCount() {
        return dialogSpinerModel.size();
    }

    @Override
    public int getItemViewType(int position) {
        return (position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {


        TextView search_dialog_variant_id;
        TextView search_dialog_variant_name;
        TextView search_dialog_actual_mrp;
        TextView search_dialog_offer_price;
        TextView search_dialog_variant_p_id;
        RelativeLayout search_full_layout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            search_dialog_variant_id = itemView.findViewById(R.id.search_dialog_variant_id);
            search_dialog_variant_name = itemView.findViewById(R.id.search_dialog_variant_name);
            search_dialog_actual_mrp = itemView.findViewById(R.id.search_dialog_actual_mrp);
            search_dialog_offer_price = itemView.findViewById(R.id.search_dialog_offer_price);
            search_dialog_variant_p_id = itemView.findViewById(R.id.search_dialog_variant_p_id);
            search_full_layout = itemView.findViewById(R.id.search_full_layout);

            search_dialog_actual_mrp.setPaintFlags(search_dialog_actual_mrp.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

            //search_dialog_actual_mrp.setPaintFlags(search_dialog_actual_mrp.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);


        }
    }

}
