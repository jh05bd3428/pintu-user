package com.knackspin.pintu.Adapter;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.knackspin.pintu.Model.RandomCategory;
import com.knackspin.pintu.R;
import com.knackspin.pintu.SubMenu;
import com.knackspin.pintu.utils.UrlPoint;

import java.util.ArrayList;

public class RandomAdpater extends RecyclerView.Adapter<RandomAdpater.ViewHolder> {

    private ArrayList<RandomCategory> randomCategories;
    private Context context;

    public RandomAdpater(ArrayList<RandomCategory> randomCategories, Context context) {
        this.randomCategories = randomCategories;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.random_model,viewGroup,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int position) {

        final RandomCategory randomCategory = randomCategories.get(position);
        viewHolder.name.setText(randomCategory.getRandom_cat_name());
        Glide.with(context).load(UrlPoint.randomImagePath + randomCategory.getRandom_cat_image()).into(viewHolder.image);

       viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {

               int pos = viewHolder.getAdapterPosition();

               Intent intent = new Intent(context, SubMenu.class);
               intent.putExtra("ran_sub_cat", randomCategories.get(pos).getRandom_sc_id());
               intent.putExtra("id", randomCategories.get(pos).getRandom_c_id());
               intent.putExtra("flag", true);
               context.startActivity(intent);

           }
       });
    }


    @Override
    public int getItemCount() {
        return randomCategories.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {

        ImageView image;
        TextView name;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            image = itemView.findViewById(R.id.random_image);
            name = itemView.findViewById(R.id.random_text);
        }
    }
}
