package com.knackspin.pintu.Adapter;

import android.content.Context;
import android.graphics.Paint;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.knackspin.pintu.Database.DatabaseHelper;
import com.knackspin.pintu.Model.Cart_model;
import com.knackspin.pintu.R;

import java.text.DecimalFormat;
import java.util.List;

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.ViewHolder> {

    private  List<Cart_model> cartModels;
    private Context mContext;
    private DatabaseHelper databaseHelper;

    int int_value;
    DecimalFormat form;


    public CartAdapter(List<Cart_model> cartModels,Context mContext) {
        this.cartModels = cartModels;
        this.mContext = mContext;
        databaseHelper = new DatabaseHelper(mContext);

        form = new DecimalFormat("0.00");

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.cart_model,viewGroup,false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {

        final Cart_model cart_model = cartModels.get(position);

        viewHolder.brand_name.setText(cart_model.getBrand_name().toUpperCase());
        viewHolder.product_name.setText(cart_model.getProduct_name().toUpperCase());
        viewHolder.variant_weight.setText(cart_model.getVariant_weight());
        viewHolder.actual_price.setText("Mrp" + "-" +"\u20B9" +cart_model.getActual_price());
        //viewHolder.offered_price.setText("\u20B9"+ cart_model.getOffered_price());
        viewHolder.offered_price.setText("\u20B9"+form.format(Double.parseDouble(cart_model.getOffered_price())));

        viewHolder.quantity.setText(cart_model.getQuantity());
        viewHolder.cart_product_id.setText(cart_model.getProduct_id());
        viewHolder.cart_variant_id.setText(cart_model.getCart_variant_id());
        Glide.with(mContext).load(cart_model.getProduct_Url()).into(viewHolder.product_image);


        final ViewHolder holder1 = viewHolder;
        holder1.delete_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String prod_id = holder1.cart_product_id.getText().toString();
                databaseHelper.delete_data(String.valueOf(prod_id));
                cartModels.remove(holder1.getAdapterPosition());
                notifyItemRemoved(holder1.getAdapterPosition());

            }
        });


        viewHolder.increment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String increment_value = viewHolder.quantity.getText().toString();

                int_value = Integer.parseInt(increment_value);
                int_value++;
                viewHolder.quantity.setText(String.valueOf(int_value));

                try
                {
                    String pid = cartModels.get(position).getProduct_id();
                    String vid = cartModels.get(position).getCart_variant_id();
                    String p_b_name = cartModels.get(position).getBrand_name();
                    String p_p_name = cartModels.get(position).getProduct_name();
                    String p_v_weight = cartModels.get(position).getVariant_weight();
                    String p_a_price = cartModels.get(position).getActual_price();
                    String p_o_price = cartModels.get(position).getOffered_price();
                    int p_quantity = int_value;

                    update_data(pid,vid,p_b_name,p_p_name,p_v_weight,p_a_price,p_o_price, String.valueOf(p_quantity));
                }

                catch (IndexOutOfBoundsException io)
                {
                    io.getMessage();
                }

            }
        });

        viewHolder.decrement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String decrement_value = viewHolder.quantity.getText().toString();
                int int_dec_value = Integer.valueOf(decrement_value);

                try
                {
                    String pid = cartModels.get(position).getProduct_id();
                    String vid = cartModels.get(position).getCart_variant_id();
                    String p_b_name = cartModels.get(position).getBrand_name();
                    String p_p_name = cartModels.get(position).getProduct_name();
                    String p_v_weight = cartModels.get(position).getVariant_weight();
                    String p_a_price = cartModels.get(position).getActual_price();
                    String p_o_price = cartModels.get(position).getOffered_price();

                    if(int_dec_value > 1)
                    {
                        int_dec_value--;
                        viewHolder.quantity.setText(String.valueOf(int_dec_value));

                    } else if (int_dec_value == 1)
                    {
                        viewHolder.quantity.setText(String.valueOf(int_dec_value));
                        databaseHelper.delete_data(String.valueOf(cart_model.getProduct_id()));
                        cartModels.remove(holder1.getAdapterPosition());
                        notifyItemRemoved(holder1.getAdapterPosition());
                    }

                    int p_quantity = int_dec_value;
                    update_data(pid, vid,p_b_name,p_p_name,p_v_weight,p_a_price,p_o_price, String.valueOf(p_quantity));

                }

                catch (IndexOutOfBoundsException io)
                {
                    io.getMessage();
                }



                //cart_check_details(pid,vid,p_b_name,p_p_name,p_v_weight,p_a_price,p_o_price, String.valueOf(p_quantity));
            }
        });

    }

    private void cart_check_details(String pid, String vid, String p_b_name, String p_p_name, String p_v_weight, String p_a_price, String p_o_price, String p_quantity)
    {
        databaseHelper = new DatabaseHelper(mContext);
        boolean isChecked = databaseHelper.checkDetails(pid);

        if (isChecked)
        {
            update_data(pid,vid,p_b_name,p_p_name,p_v_weight,p_a_price,p_o_price,p_quantity);
            //Toast.makeText(mContext, "updated", Toast.LENGTH_SHORT).show();
        } else
        {

            //insert_data(pid,p_b_name,p_p_name,p_v_weight,p_a_price,p_o_price,p_quantity,vid);
            // Toast.makeText(mContext, "deleted", Toast.LENGTH_SHORT).show();
        }
    }

    private void update_data(String pid, String vid,String p_b_name, String p_p_name, String p_v_weight, String p_a_price, String p_o_price, String p_quantity) {

        databaseHelper = new DatabaseHelper(mContext);
        boolean isUpdated = databaseHelper.updateData(pid,p_b_name, p_p_name, p_v_weight, p_a_price, p_o_price, p_quantity);
        if (isUpdated) {

            Log.d("UPDATE", "data updated");
        } else {

            Log.d("UPDATE", "data not updated");

        }

    }

    private void insert_data (String pid, String p_b_name, String p_p_name, String p_v_weight, String p_a_price, String p_o_price, String p_quantity,String vid)
    {

        databaseHelper = new DatabaseHelper(mContext);
        boolean isInserted = databaseHelper.insert_data("",vid,pid,p_b_name,p_p_name,p_v_weight,p_a_price,p_o_price,p_quantity);
        if (isInserted)
        {
            Log.d("INSERT", "data inserted");
        } else
        {
            Log.d("INSERT", "data not inserted");

        }
    }

    @Override
    public int getItemCount() {
        return cartModels.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView brand_name;
        TextView product_name;
        TextView variant_weight;
        TextView actual_price;
        TextView offered_price;
        TextView quantity;
        TextView cart_product_id;
        ImageView delete_button;
        ImageView increment;
        ImageView decrement;
        ImageView product_image;
        TextView cart_variant_id;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);


            brand_name = itemView.findViewById(R.id.cart_brand_name);
            product_name = itemView.findViewById(R.id.cart_product_name);
            variant_weight = itemView.findViewById(R.id.cart_variant_weight);
            actual_price = itemView.findViewById(R.id.cart_actual_price);
            offered_price = itemView.findViewById(R.id.cart_offered_price);
            quantity = itemView.findViewById(R.id.cart_display_quantity);
            cart_product_id = itemView.findViewById(R.id.cart_product_id);
            delete_button = itemView.findViewById(R.id.delete_button);
            product_image = itemView.findViewById(R.id.cart_product_image);

            increment = itemView.findViewById(R.id.cart_increment);
            decrement = itemView.findViewById(R.id.cart_decrement);

            cart_variant_id = itemView.findViewById(R.id.cart_variant_id);

            actual_price.setPaintFlags(actual_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

        }
    }
}
