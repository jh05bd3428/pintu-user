package com.knackspin.pintu.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.knackspin.pintu.Model.Shop_category;
import com.knackspin.pintu.R;

import java.util.ArrayList;

public class CategoryAdapter  extends BaseAdapter {

    private ArrayList<Shop_category> shop_cat;
    private Context mContext;
    private String sdc = "http://ryes.in/uploads/category/";
    //private String sdc = "http://knackspin.com/RYSE/public/uploads/category/";

    public CategoryAdapter(ArrayList<Shop_category> shop_cat, Context mContext) {
        this.shop_cat = shop_cat;
        this.mContext = mContext;
    }


    @Override
    public int getCount() {
        return shop_cat.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

  /*  @Override
    public int getItemViewType(int position) {
        return position;
    }
*/
    @Override
    public int getViewTypeCount() {
        return getCount();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View gridViewAndroid = null;
        if(convertView == null)
        {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            gridViewAndroid = new View(mContext);
            gridViewAndroid = inflater.inflate(R.layout.grif_view_model, null, true);
        }
        else {
            gridViewAndroid = (View) convertView;
        }
        TextView textView = gridViewAndroid.findViewById(R.id.android_gridview_text);
        ImageView imageView = gridViewAndroid.findViewById(R.id.android_gridview_image);
        textView.setText(shop_cat.get(position).getCat_name());
        Glide.with(mContext).load(sdc+shop_cat.get(position).getCat_image()).into(imageView);

        return gridViewAndroid;
    }
}
