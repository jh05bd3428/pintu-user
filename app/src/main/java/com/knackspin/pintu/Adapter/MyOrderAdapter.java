package com.knackspin.pintu.Adapter;

import android.content.Context;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.knackspin.pintu.DetailOrderStatus;
import com.knackspin.pintu.Model.MyOrderModel;
import com.knackspin.pintu.R;

import java.util.List;

public class MyOrderAdapter extends RecyclerView.Adapter<MyOrderAdapter.ViewHolder> {


    private List<MyOrderModel> myOrderModels;
    private Context context;

    public MyOrderAdapter(List<MyOrderModel> myOrderModels, Context context) {
        this.myOrderModels = myOrderModels;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext()). inflate(R.layout.my_order_model, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int position) {

        final MyOrderModel myOrderModel = myOrderModels.get(position);
        viewHolder.order_id.setText(myOrderModel.getOrder_id());
        viewHolder.order_amount.setText("\u20B9"+ myOrderModel.getOrder_amount());
        viewHolder.order_date.setText(myOrderModel.getOrder_date().substring(0,16));
        //viewHolder. order_status.setText(myOrderModel.getOrder_status().toUpperCase());

        String status = myOrderModels.get(position).getOrder_status();

        if (status.equalsIgnoreCase("placed"))
        {
            viewHolder.order_status.setText("PENDING");
            viewHolder.order_status.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
        }

        else if(status.equalsIgnoreCase("Processing"))
        {
            viewHolder.order_status.setText("CONFIRMED");
            viewHolder.order_status.setBackgroundColor(context.getResources().getColor(R.color.blue_700));
        }

        else if (status.equalsIgnoreCase("Billed"))
        {
            viewHolder.order_status.setText("CONFIRMED");
            viewHolder.order_status.setBackgroundColor(context.getResources().getColor(R.color.dot_dark_screen4));
        }

        else if (status.equalsIgnoreCase("ontheway"))
        {
            viewHolder.order_status.setText("SHIPPED");
            viewHolder.order_status.setBackgroundColor(context.getResources().getColor(R.color.dot_light_screen1));
        }
        else if (status.equalsIgnoreCase("delivered"))
        {
            viewHolder.order_status.setText("DELIVERED");
            viewHolder.order_status.setBackgroundColor(context.getResources().getColor(R.color.new_orange));
        }

        else if (status.equalsIgnoreCase("cancelled"))
        {
            viewHolder.order_status.setText("CANCELLED");
            viewHolder.order_status.setBackgroundColor(context.getResources().getColor(R.color.dark_red));
        }

        else if (status.equalsIgnoreCase("Door Closed"))
        {
            viewHolder.order_status.setText("CANCELLED");
            viewHolder.order_status.setBackgroundColor(context.getResources().getColor(R.color.dark_red));
        }



        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, DetailOrderStatus.class);
                intent.putExtra("orderId", myOrderModels.get(position).getOrder_id());
                intent.putExtra("orderAmount", myOrderModels.get(position).getOrder_amount());
                intent.putExtra("orderStatus", myOrderModels.get(position).getOrder_status());
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return myOrderModels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        TextView order_id;
        TextView order_amount;
        TextView order_date;
        TextView order_status;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            order_id = itemView.findViewById(R.id.my_order_id);
            order_amount = itemView.findViewById(R.id.my_order_amount);
            order_date = itemView.findViewById(R.id.order_date_time);
            order_status = itemView.findViewById(R.id.order_status_my);
        }
    }
}
