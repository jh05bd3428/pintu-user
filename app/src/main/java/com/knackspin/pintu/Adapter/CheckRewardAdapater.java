package com.knackspin.pintu.Adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.knackspin.pintu.Model.CheckRewardModel;
import com.knackspin.pintu.R;
import com.knackspin.pintu.utils.UrlPoint;

import java.util.ArrayList;

public class CheckRewardAdapater extends RecyclerView.Adapter<CheckRewardAdapater.ViewHolder> {

    private ArrayList<CheckRewardModel> checkRewardModels;
    private Context context;


    public CheckRewardAdapater(ArrayList<CheckRewardModel> checkRewardModels, Context context) {
        this.checkRewardModels = checkRewardModels;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder( ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.reward_model,viewGroup,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int position) {

        final CheckRewardModel checkRewardModel = checkRewardModels.get(position);


        Glide.with(context).load(UrlPoint.rewardImagePath +checkRewardModel.getReward_image_id()).into(viewHolder.product_image);

        viewHolder.product_name.setText("Product Name"+ " - "+ checkRewardModel.getReward_p_name());
        viewHolder.product_price.setText("Product Price" + " - " +checkRewardModel.getReward_p_price());
        viewHolder.product_points.setText("Reward Points"+" - " +checkRewardModel.getReward_p_point());

        viewHolder.order_reward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(context, "Reward Points are not sufficient", Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public int getItemCount() {
        return checkRewardModels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {

        ImageView product_image;
        TextView product_name;
        TextView product_price;
        TextView product_points;
        Button order_reward;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);


            product_image =itemView.findViewById(R.id.check_reward_image);
            product_name = itemView.findViewById(R.id.check_reward_text);
            product_price = itemView.findViewById(R.id.check_reward_price);
            product_points = itemView.findViewById(R.id.check_reward_points);
            order_reward = itemView.findViewById(R.id.reward_order_button);
        }
    }
}
