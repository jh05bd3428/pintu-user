package com.knackspin.pintu.Adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.knackspin.pintu.Model.OfferDialogModel;
import com.knackspin.pintu.R;
import com.knackspin.pintu.interfaces.OfferNotifFragmentData;

import java.util.List;


public class OfferDialogAdapter extends RecyclerView.Adapter<OfferDialogAdapter.ViewHolder> {



    private String dialogQwerty;
    private List<OfferDialogModel> dialogPopModel;
    private Context context;
    private OfferNotifFragmentData offerNotifFragmentData;

    public OfferDialogAdapter(String dialogQwerty, List<OfferDialogModel> dialogPopModel, Context context, OfferNotifFragmentData offerNotifFragmentData) {
        this.dialogQwerty = dialogQwerty;
        this.dialogPopModel = dialogPopModel;
        this.context = context;
        this.offerNotifFragmentData = offerNotifFragmentData;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int pos) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.offer_dialog_fragmnet,viewGroup,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, int pos) {

        final OfferDialogModel offerDialogModel = dialogPopModel.get(pos);

        viewHolder.offer_dialog_variant_id.setText(offerDialogModel.getOffer_dialog_variant_id());
        viewHolder.offer_dialog_variant_name.setText(offerDialogModel.getOffer_dialog_variant_name());
        viewHolder.offer_dialog_actual_mrp.setText("\u20B9" + offerDialogModel.getOffer_dialog_mrp_price());
        viewHolder.offer_dialog_offer_price.setText("\u20B9" + offerDialogModel.getOffer_dialog_offered_price());

        viewHolder.offer_search_full_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int pos = viewHolder.getAdapterPosition();
                offerNotifFragmentData.setData(pos, 0);
                Log.d("SearchDialogAdapter", dialogPopModel.get(pos).toString());


            }
        });
    }

    @Override
    public int getItemCount() {
        return dialogPopModel.size();
    }

    @Override
    public int getItemViewType(int position) {
        return (position);
    }


    public class ViewHolder extends RecyclerView.ViewHolder
    {
        TextView offer_dialog_variant_id;
        TextView offer_dialog_variant_name;
        TextView offer_dialog_actual_mrp;
        TextView offer_dialog_offer_price;
        TextView offer_dialog_variant_p_id;
        LinearLayout offer_search_full_layout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            offer_dialog_variant_id = itemView.findViewById(R.id.offer_dialog_variant_id);
            offer_dialog_variant_name = itemView.findViewById(R.id.offer_dialog_variant_name);
            offer_dialog_actual_mrp = itemView.findViewById(R.id.offer_dialog_actual_mrp);
            offer_dialog_offer_price = itemView.findViewById(R.id.offer_dialog_offer_price);
            offer_dialog_variant_p_id = itemView.findViewById(R.id.offer_dialog_variant_p_id);
            offer_search_full_layout = itemView.findViewById(R.id.offer_search_full_layout);
        }
    }
}
