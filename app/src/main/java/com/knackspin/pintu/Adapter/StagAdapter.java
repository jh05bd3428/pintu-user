package com.knackspin.pintu.Adapter;

import android.content.Context;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.knackspin.pintu.Model.Shop_category;
import com.knackspin.pintu.R;
import com.knackspin.pintu.SubMenu;
import com.knackspin.pintu.utils.UrlPoint;

import java.util.ArrayList;

public class StagAdapter extends RecyclerView.Adapter<StagAdapter.ViewHolder> {


    private ArrayList<Shop_category> shop_cat;
    private Context mContext;



    public StagAdapter(ArrayList<Shop_category> shop_cat, Context mContext) {
        this.shop_cat = shop_cat;
        this.mContext = mContext;
    }

    @Override
    public ViewHolder onCreateViewHolder( ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.grif_view_model,viewGroup,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int i) {

        final Shop_category shop_category = shop_cat.get(i);
        viewHolder.text.setText(shop_category.getCat_name());
        Glide.with(mContext).load(UrlPoint.categoryImage + shop_category.getCat_image()).into(viewHolder.image);


        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               int pos = viewHolder.getAdapterPosition();

               Intent intent = new Intent(mContext, SubMenu.class);
               intent.putExtra("postion", pos);
               intent.putExtra("name", shop_cat.get(i).getCat_name());
               intent.putExtra("id", shop_cat.get(i).getId());
                //intent.putExtra("flag", "2");

                mContext.startActivity(intent);

            }
        });

    }

    @Override
    public int getItemCount() {
        return shop_cat.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        ImageView image;
        TextView text;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            image = itemView.findViewById(R.id.android_gridview_image);
            text = itemView.findViewById(R.id.android_gridview_text);
        }
    }
}
