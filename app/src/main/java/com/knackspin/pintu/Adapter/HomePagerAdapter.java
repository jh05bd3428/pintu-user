package com.knackspin.pintu.Adapter;

import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.Glide;
import com.knackspin.pintu.Model.HomePagerModel;
import com.knackspin.pintu.R;
import com.knackspin.pintu.utils.UrlPoint;

import java.util.List;

public class HomePagerAdapter extends PagerAdapter {

    private List<HomePagerModel> homePagerModels;
    private Context context;
    LayoutInflater layoutInflater;

    int current_pos = 0;

    public HomePagerAdapter(List<HomePagerModel> homePagerModels, Context context) {
        this.homePagerModels = homePagerModels;
        this.context = context;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return Integer.MAX_VALUE;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == ((RelativeLayout) object);    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, final int position) {

        if (current_pos > homePagerModels.size()-1)
            current_pos = 0;
        HomePagerModel homePagerModel = homePagerModels.get(current_pos);
        current_pos++;
        View itemView = layoutInflater.inflate(R.layout.home_image_model, container, false);

        ImageView imageView = (ImageView) itemView.findViewById(R.id.iv_home_image);
        Glide.with(context).load(UrlPoint.banner_path+ homePagerModel.getImage_name()).into(imageView);

        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return POSITION_NONE;
    }
}
