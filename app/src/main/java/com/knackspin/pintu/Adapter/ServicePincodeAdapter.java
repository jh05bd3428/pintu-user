package com.knackspin.pintu.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.knackspin.pintu.HomeScreen;
import com.knackspin.pintu.Model.ServicePincodeModel;
import com.knackspin.pintu.R;
import com.knackspin.pintu.utils.UrlPoint;

import java.util.List;

public class ServicePincodeAdapter extends RecyclerView.Adapter<ServicePincodeAdapter.ViewHolder> {

    private List<ServicePincodeModel> servicePincodeModels;
    private Context context;

    public ServicePincodeAdapter(List<ServicePincodeModel> servicePincodeModels, Context context) {
        this.servicePincodeModels = servicePincodeModels;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.servisable_pincode_model, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        ServicePincodeModel servicePincodeModel = servicePincodeModels.get(position);

        Glide.with(context).load(UrlPoint.shop_image+servicePincodeModel.getService_shop_image()).into(holder.iv_service_image);
        holder.ser_shop_name.setText(servicePincodeModel.getService_shop_name());
        holder.ser_shop_address.setText(servicePincodeModel.getService_shop_address());
        holder.ser_shop_pin.setText(servicePincodeModel.getService_pincode());

        holder.rl_details_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(context, HomeScreen.class);
                intent.putExtra("store_id", servicePincodeModels.get(position).getService_shop_id());
                intent.putExtra("store_name", servicePincodeModels.get(position).getService_shop_name());
                intent.putExtra("store_delivery_charge", servicePincodeModels.get(position).getService_shop_delivery_charge());
                intent.putExtra("store_minimum_charge", servicePincodeModels.get(position).getService_shop_minimum_charge());
                context.startActivity(intent);
                ((Activity)context).finish();

            }
        });

    }

    @Override
    public int getItemCount() {
        return servicePincodeModels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView iv_service_image;
        TextView ser_shop_name, ser_shop_address,ser_shop_pin;
        RelativeLayout rl_details_layout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            iv_service_image = itemView.findViewById(R.id.iv_service_image);
            ser_shop_name = itemView.findViewById(R.id.ser_shop_name);
            ser_shop_address = itemView.findViewById(R.id.ser_shop_address);
            ser_shop_pin = itemView.findViewById(R.id.ser_shop_pin);

            rl_details_layout = itemView.findViewById(R.id.rl_details_layout);
        }
    }
}
