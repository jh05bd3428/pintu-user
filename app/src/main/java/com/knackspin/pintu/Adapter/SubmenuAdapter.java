package com.knackspin.pintu.Adapter;

import android.os.Build;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import androidx.annotation.RequiresApi;

import com.knackspin.pintu.Model.Fragmnet_model;
import com.knackspin.pintu.fragments.Product_fragment;

import java.util.List;

public class SubmenuAdapter extends FragmentStatePagerAdapter {

    private List<Fragmnet_model> subCatList;

    public SubmenuAdapter(FragmentManager fm,List<Fragmnet_model> subCatList) {
        super(fm);
        this.subCatList = subCatList;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public Fragment getItem(int position) {
        Product_fragment fragment = Product_fragment.newInstance(subCatList.get(position).getSubId());
        fragment.setRetainInstance(true);
        return fragment;
    }
    @Override
    public int getCount() {
        return subCatList.size();
    }


    @Override
    public CharSequence getPageTitle(int position) {
        return (CharSequence) subCatList.get(position).getName();
    }
}