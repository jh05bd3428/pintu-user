package com.knackspin.pintu.Adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Paint;
import android.os.Build;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;

import com.knackspin.pintu.Model.DialogModel;
import com.knackspin.pintu.R;
import com.knackspin.pintu.interfaces.DialogFragmentSetData;

import java.util.ArrayList;
import java.util.List;

public class DialogAdapter extends RecyclerView.Adapter<DialogAdapter.ViewHolder> implements View.OnClickListener {


    private static int current_position= -1;

    private List<DialogModel> dialogModels;
    private Context mContext;
    private DialogFragmentSetData dialogFragmentSetData;
    String qwerty;
    private ArrayList<String> list;

    public DialogAdapter(String qwerty, List<DialogModel> dialogModels, Activity mContext, DialogFragmentSetData dialogFragmentSetData) {
        this.qwerty = qwerty;
        this.dialogModels = dialogModels;
        this.mContext = mContext;
        this.dialogFragmentSetData = dialogFragmentSetData;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.fragment_dialog,viewGroup,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {

        final DialogModel dialogModel = dialogModels.get(position);

        viewHolder.variant_id.setText(dialogModel.getVariant_id());
        viewHolder.variant_name.setText(dialogModel.getVariant_name());
        viewHolder.actual_price.setText("Mrp"+" "+"\u20B9" + dialogModel.getMrp_price());
        viewHolder.offered_price.setText("\u20B9" + dialogModel.getOffered_price());


        viewHolder.full_layout.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {

                int pos = viewHolder.getAdapterPosition();
                dialogFragmentSetData.setData(pos, 0);
                Log.d("DialogAdapter", dialogModels.get(pos).toString());

            }

        });

    }

    @Override
    public int getItemCount() {

       return dialogModels.size();
    }

    @Override
    public int getItemViewType(int position) {
        return (position);
    }

    @Override
    public void onClick(View v) {

    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        TextView variant_id;
        TextView variant_name;
        TextView actual_price;
        TextView offered_price;
        TextView variant_p_id;
        RelativeLayout full_layout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            variant_id = itemView.findViewById(R.id.variant_id);
            variant_p_id = itemView.findViewById(R.id.variant_p_id);
            variant_name = itemView.findViewById(R.id.variant_name);
            actual_price = itemView.findViewById(R.id.actual_mrp);
            offered_price = itemView.findViewById(R.id.offer_price);

            full_layout = itemView.findViewById(R.id.dialog_frame);

            actual_price.setPaintFlags(actual_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

        }
    }

}
