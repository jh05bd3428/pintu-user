package com.knackspin.pintu.Adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.knackspin.pintu.Model.PlacedOrderModel;
import com.knackspin.pintu.R;
import com.knackspin.pintu.utils.UrlPoint;

import java.util.List;

public class PlacedOrderAdapter extends RecyclerView.Adapter<PlacedOrderAdapter.ViewHolder> {

    private List<PlacedOrderModel> placedOrderModels;
    private Context context;

    public PlacedOrderAdapter(List<PlacedOrderModel> placedOrderModels, Context context) {
        this.placedOrderModels = placedOrderModels;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder( ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.ordered_item_model,viewGroup,false);
        return new ViewHolder(view);

    }

    @Override
    public void onBindViewHolder( ViewHolder viewHolder, int i) {

        final PlacedOrderModel placedOrderModel = placedOrderModels.get(i);

        viewHolder.brand_name.setText(placedOrderModel.getPlaced_order_brand_name());
        viewHolder.product_name.setText(placedOrderModel.getPlaced_order_product_name());
        viewHolder.product_variant.setText(placedOrderModel.getPlaced_order_variant_type());
        viewHolder.product_quantity.setText(placedOrderModel.getPlaced_order_quantity());
        viewHolder.product_price.setText("\u20B9"+ placedOrderModels.get(i).getPlaced_order_product_price());

        Glide.with(context).load(UrlPoint.statusImage + placedOrderModel.getPlaced_order_product_image()).into(viewHolder.product_image);

    }

    @Override
    public int getItemCount() {
        return placedOrderModels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        TextView brand_name;
        TextView product_name;
        TextView product_variant;
        TextView product_quantity;
        TextView product_price;
        ImageView product_image;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            brand_name = itemView.findViewById(R.id.history_brand_name);
            product_name = itemView.findViewById(R.id.history_product_name);
            product_variant = itemView.findViewById(R.id.history_product_variant);
            product_quantity = itemView.findViewById(R.id.history_product_quantity);
            product_price = itemView.findViewById(R.id.histroy_product_price);
            product_image = itemView.findViewById(R.id.product_histroy_image);
        }
    }
}
