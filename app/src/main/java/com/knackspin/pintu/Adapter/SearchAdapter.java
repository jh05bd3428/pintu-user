package com.knackspin.pintu.Adapter;

import android.content.Context;
import android.graphics.Paint;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.knackspin.pintu.Database.DatabaseHelper;
import com.knackspin.pintu.Model.SearchDialogModel;
import com.knackspin.pintu.Model.SearchModel;
import com.knackspin.pintu.R;
import com.knackspin.pintu.SerachActivity;
import com.knackspin.pintu.fragments.SearchDialogFragment;
import com.knackspin.pintu.interfaces.SearchModelSetData;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;


public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.ViewHolder> {

    private List<SearchModel> modelSearch;
    private Context context;
    private List<SearchDialogModel> dialogSearchModel;
    private SerachActivity mContext;

    SerachActivity serachActivity;
    DatabaseHelper myDb;


    public static final String TAG = SearchAdapter.class.getSimpleName();


    String serach_img = "http://store.pahadiconnect.com/uploads/products/";


    int dialogspinnerPosition= 0;
    int dialogspinnerPos1 = 0;

    DecimalFormat format;

    public SearchAdapter(List<SearchModel> modelSearch, Context context, SerachActivity mContext) {
        this.modelSearch = modelSearch;
        this.context = context;
        this.mContext = mContext;

        myDb = new DatabaseHelper(context);
        serachActivity = (SerachActivity) mContext;

        format = new DecimalFormat("0.00");
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.search_model,viewGroup,false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int i) {

        final SearchModel searchModel = modelSearch.get(i);

        viewHolder.product_id.setText(searchModel.getSearch_product_id());
        viewHolder.product_name.setText(searchModel.getSearch_product_name().toUpperCase());
        viewHolder.brand_name.setText(searchModel.getSearch_brand_name().toUpperCase());
        Glide.with(context).load(serach_img+searchModel.getSearch_product_image()).into(viewHolder.product_image);

        dialogSearchModel = searchModel.getSearchdialogModel();

        if(dialogspinnerPosition == 0 )
        {
            viewHolder.variant_id.setText(dialogSearchModel.get(dialogspinnerPosition).getDialog_variant_id());
            viewHolder.variant_weight.setText(dialogSearchModel.get(dialogspinnerPosition).getDialog_variant_name());
            viewHolder.actual_price.setText("Mrp" + "-" + "\u20B9" + dialogSearchModel.get(dialogspinnerPosition).getDialog_mrp_price());
            //viewHolder.offered_price.setText("\u20B9" + dialogSearchModel.get(dialogspinnerPosition).getDialog_offered_price());
            viewHolder.offered_price.setText("\u20B9"+format.format(Double.parseDouble(dialogSearchModel.get(dialogspinnerPosition).getDialog_offered_price())));

        }

        viewHolder.dropdown_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final int pos = viewHolder.getAdapterPosition();
                ArrayList<SearchDialogModel> listOfSpinners = (ArrayList<SearchDialogModel>) modelSearch.get(i).getSearchdialogModel();

                SearchModelSetData modelSetData = new SearchModelSetData() {
                    @Override
                    public void setSearchModel(int spinnerPos, int productPos) {

                        List<SearchDialogModel> spinerModel = modelSearch.get(pos).getSearchdialogModel();
                        viewHolder.variant_id.setText(spinerModel.get(spinnerPos).getDialog_variant_id());
                        viewHolder.variant_weight.setText(spinerModel.get(spinnerPos).getDialog_variant_name());
                        viewHolder.actual_price.setText("Mrp"+ "-"+"\u20B9"+spinerModel.get(spinnerPos).getDialog_mrp_price());
                        viewHolder.offered_price.setText("\u20B9"+spinerModel.get(spinnerPos).getDialog_offered_price());

                        dialogspinnerPos1 = spinnerPos;

                    }
                };

                SearchDialogFragment searchDialogFragment = new SearchDialogFragment();
                Bundle bundle = new Bundle();
                bundle.putString("prod_id", searchModel.getSearch_product_id());
                bundle.putSerializable("list", listOfSpinners);
                bundle.putSerializable("model", modelSetData);
                searchDialogFragment.setArguments(bundle);

                searchDialogFragment.show((mContext).getSupportFragmentManager(),TAG);

            }
        });


       viewHolder.increment.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {

               String increment_value = viewHolder.displayQuantity.getText().toString();
               int int_value = Integer.parseInt(increment_value);
               int_value++;
               viewHolder.displayQuantity.setText(String.valueOf(int_value));

               try {

                   String pid = modelSearch.get(i).getSearch_product_id();
                   String p_b_name = modelSearch.get(i).getSearch_brand_name();
                   String p_p_name = modelSearch.get(i).getSearch_product_name();

                   String p_v_weight = modelSearch.get(i).getSearchdialogModel().get(dialogspinnerPos1).getDialog_variant_name();
                   String p_a_price =  modelSearch.get(i).getSearchdialogModel().get(dialogspinnerPos1).getDialog_mrp_price();
                   String vid =  modelSearch.get(i).getSearchdialogModel().get(dialogspinnerPos1).getDialog_variant_id();
                   String p_o_price =  modelSearch.get(i).getSearchdialogModel().get(dialogspinnerPos1).getDialog_offered_price();

                   int p_quantity = int_value;
                   String productUrl = serach_img+modelSearch.get(i).getSearch_product_image();

                   check_details (productUrl,pid,vid,p_b_name,p_p_name,p_v_weight,p_a_price,p_o_price, String.valueOf(p_quantity));

               }

               catch (IndexOutOfBoundsException ioe)
               {
                   ioe.getMessage();
               }


           }
       });

       viewHolder.decrement.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {

               String decrement_value = viewHolder.displayQuantity.getText().toString();
               int int_dec_value = Integer.parseInt(decrement_value);



               try
               {
                   String pid = modelSearch.get(i).getSearch_product_id();
                   String p_b_name = modelSearch.get(i).getSearch_brand_name();
                   String p_p_name = modelSearch.get(i).getSearch_product_name();

                   String p_v_weight = modelSearch.get(i).getSearchdialogModel().get(dialogspinnerPos1).getDialog_variant_name();
                   String p_a_price =  modelSearch.get(i).getSearchdialogModel().get(dialogspinnerPos1).getDialog_mrp_price();
                   String vid =  modelSearch.get(i).getSearchdialogModel().get(dialogspinnerPos1).getDialog_variant_id();
                   String p_o_price =  modelSearch.get(i).getSearchdialogModel().get(dialogspinnerPos1).getDialog_offered_price();

                   //int p_quantity = int_dec_value;
                   String productUrl = serach_img+modelSearch.get(i).getSearch_product_image();

                   if (int_dec_value > 1) {
                       int_dec_value--;
                       viewHolder.displayQuantity.setText(String.valueOf(int_dec_value));
                       check_details (productUrl,pid,vid,p_b_name,p_p_name,p_v_weight,p_a_price,p_o_price, String.valueOf(int_dec_value));
                   }else if (int_dec_value == 1){
                       myDb.delete_data(String.valueOf(pid));
                       viewHolder.displayQuantity.setText("0");
                       serachActivity.updateCartCount();
                   }
               }

               catch (IndexOutOfBoundsException ioe)
               {
                   ioe.getMessage();
               }




           }

       });

    }

    private void check_details(String product_image,String pid,String vid, String p_b_name, String p_p_name, String p_v_weight, String p_a_price, String p_o_price, String p_quantity)
    {

        myDb = new DatabaseHelper(context);
        boolean isChecked = myDb.checkDetails(pid);

        if (isChecked)
        {
            update_data(pid,p_b_name,p_p_name,p_v_weight,p_a_price,p_o_price,p_quantity);
            //Toast.makeText(context, "updated", Toast.LENGTH_SHORT).show();

        } else
        {
            insert_data(product_image, vid,pid,p_b_name,p_p_name,p_v_weight,p_a_price,p_o_price,p_quantity);
            serachActivity.updateCartCount();
            //Toast.makeText(context, "Inserted", Toast.LENGTH_SHORT).show();
        }

    }

    private void update_data(String pid, String p_b_name, String p_p_name, String p_v_weight, String p_a_price, String p_o_price, String p_quantity)
    {

        myDb = new DatabaseHelper(context);
        boolean isUpdated = myDb.updateData(pid,p_b_name,p_p_name,p_v_weight,p_a_price,p_o_price,p_quantity);
        if (isUpdated)
        {
            //Toast.makeText(context, "", Toast.LENGTH_SHORT).show();
        }else
        {
            //  Toast.makeText(context, "", Toast.LENGTH_SHORT).show();
        }
    }

    private void insert_data (String product_image,String pid,String vid, String p_b_name, String p_p_name, String p_v_weight, String p_a_price, String p_o_price, String p_quantity)
    {

        myDb = new DatabaseHelper(context);
        boolean isInserted = myDb.insert_data(product_image,vid,pid,p_b_name,p_p_name,p_v_weight,p_a_price,p_o_price,p_quantity);
        if (isInserted)
        {
            //Log.d("","");
        }

        else
        {

        }

    }


    @Override
    public int getItemCount() {
        return modelSearch.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder
    {

        TextView product_id;
        TextView brand_name;
        TextView product_name;
        ImageView product_image;
        TextView variant_name;
        TextView actual_price;
        TextView offered_price;
        ImageView increment;
        ImageView decrement;
        ImageView select_arrow;
        TextView displayQuantity;
        TextView dialog_product_type;
        TextView variant_weight;
        TextView variant_id;
        LinearLayout dropdown_layout;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);


            brand_name = itemView.findViewById(R.id.search_brand_name);
            product_name = itemView.findViewById(R.id.search_product_name);
            product_image = itemView.findViewById(R.id.search_product_image);
            variant_name = itemView.findViewById(R.id.variant_name);
            increment = itemView.findViewById(R.id.search_increment);
            decrement = itemView.findViewById(R.id.search_decrement);
            displayQuantity = itemView.findViewById(R.id.search_display_quantity);
            actual_price = itemView.findViewById(R.id.search_actual_price);
            offered_price = itemView.findViewById(R.id.search_offered_price);
            variant_weight = itemView.findViewById(R.id.search_variant_weight);
            product_id = itemView.findViewById(R.id.search_product_id);
            variant_id = itemView.findViewById(R.id.search_variant_id);
            select_arrow = itemView.findViewById(R.id.search_select_arrow);
            dropdown_layout = itemView.findViewById(R.id.dropdown_layout);

            actual_price.setPaintFlags(actual_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

        }
    }
}
