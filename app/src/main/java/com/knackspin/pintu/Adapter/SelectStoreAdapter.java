package com.knackspin.pintu.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.knackspin.pintu.HomeScreen;
import com.knackspin.pintu.Model.StoreSelectModel;
import com.knackspin.pintu.R;
import com.knackspin.pintu.utils.UrlPoint;

import java.util.List;

public class SelectStoreAdapter extends RecyclerView.Adapter<SelectStoreAdapter.ViewHolder> {

    private List<StoreSelectModel> storeSelectModels;
    private Context context;

    public SelectStoreAdapter(List<StoreSelectModel> storeSelectModels, Context context) {
        this.storeSelectModels = storeSelectModels;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.store_details_model, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {

        final StoreSelectModel storeSelectModel = storeSelectModels.get(i);

        viewHolder.store_name.setText(storeSelectModel.getStore_name());
        viewHolder.store_address.setText(storeSelectModel.getStore_address());
        viewHolder.store_id.setText(storeSelectModel.getStore_id());

        Glide.with(context).load(UrlPoint.shop_image + storeSelectModel.getStore_image()).into(viewHolder.store_image);


        viewHolder.store_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


              /*  Intent intent = new Intent(context, NewHomeScreen.class);
                intent.putExtra("store_name", storeSelectModels.get(i).getStore_name());
                context.startActivity(intent);*/

                Intent intent = new Intent(context, HomeScreen.class);
                intent.putExtra("store_id",storeSelectModels.get(i).getStore_id());
                intent.putExtra("store_name", storeSelectModels.get(i).getStore_name());
                intent.putExtra("store_delivery_charge", storeSelectModels.get(i).getStore_delievry_charge());
                intent.putExtra("store_minimum_charge", storeSelectModels.get(i).getStore_minimum_delivery());
                context.startActivity(intent);
                ((Activity)context).finish();
            }
        });

    }

    @Override
    public int getItemCount() {
        return storeSelectModels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        ImageView store_image;
        RelativeLayout store_layout;
        TextView store_name, store_address, store_description, store_id;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            store_layout = itemView.findViewById(R.id.store_layout);
            store_image = itemView.findViewById(R.id.store_image);
            store_name = itemView.findViewById(R.id.store_name);
            store_address = itemView.findViewById(R.id.store_address);
            store_description = itemView.findViewById(R.id.store_description);
            store_id = itemView.findViewById(R.id.store_id);
        }
    }

}
