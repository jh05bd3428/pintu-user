package com.knackspin.pintu.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.knackspin.pintu.Model.RandomSearchModel;
import com.knackspin.pintu.R;
import com.knackspin.pintu.SubMenu;
import com.knackspin.pintu.utils.UrlPoint;

import java.util.List;

public class RandomSearchAdapter extends RecyclerView.Adapter<RandomSearchAdapter.ViewHolder> {

    private List<RandomSearchModel> randomSearchModels;
    private Context context;

    public RandomSearchAdapter(List<RandomSearchModel> randomSearchModels, Context context) {
        this.randomSearchModels = randomSearchModels;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.random_search_model, parent, false);
        return new ViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        RandomSearchModel randomSearchModel = randomSearchModels.get(position);

        holder.tv_search_random.setText(randomSearchModel.getSer_ran_name());
        Glide.with(context).load(UrlPoint.randomImagePath + randomSearchModel.getSer_ran_image()).into(holder.search_random_image);

        holder.rl_random_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(context, SubMenu.class);
                intent.putExtra("ran_sub_cat", randomSearchModels.get(position).getSer_ran_sub_cat_id());
                intent.putExtra("id", randomSearchModels.get(position).getSer_ran_cat_id());
                intent.putExtra("flag", true);
                context.startActivity(intent);

            }
        });



    }

    @Override
    public int getItemCount() {
        return randomSearchModels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        RelativeLayout rl_random_search;
        TextView tv_search_random;
        ImageView search_random_image;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            rl_random_search = itemView.findViewById(R.id.rl_random_search);
            tv_search_random = itemView.findViewById(R.id.tv_search_random);
            search_random_image = itemView.findViewById(R.id.search_random_image);

        }
    }
}
