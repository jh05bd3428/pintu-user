package com.knackspin.pintu.Adapter;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.knackspin.pintu.Model.CouponModel;
import com.knackspin.pintu.R;

import java.util.List;

public class CouponAdapter extends RecyclerView.Adapter<CouponAdapter.ViewHolder>
{

    private List<CouponModel> couponModels;
    private Context context;

    public CouponAdapter(List<CouponModel> couponModels, Context context) {
        this.couponModels = couponModels;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.shop_coupon_model,viewGroup,false);
        return new ViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int i) {

        final CouponModel couponModel = couponModels.get(i);

        viewHolder.coupon_id.setText(couponModel.getCoupon_id());
        viewHolder.coupon_code.setText(couponModel.getCoupon_code());
        viewHolder.coupon_amount.setText("Get"+" "+couponModel.getCoupon_amount()+"%"+" "+"on your first 2 orders with us");
        viewHolder.coupon_description.setText(couponModel.getCoupon_description());

        viewHolder.coupon_code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int pos = viewHolder.getAdapterPosition();

                String coupon_code = couponModels.get(pos).getCoupon_code();
                String coupon_amount = couponModels.get(pos).getCoupon_amount();

                Intent intent = new Intent("sending_coupon_code");
                intent.putExtra("COUPON_CODE", coupon_code);
                intent.putExtra("COUPON_AMOUNT", coupon_amount);

                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);

            }
        });

    }

    @Override
    public int getItemCount() {
        return couponModels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {

        TextView coupon_id, coupon_code,coupon_amount,coupon_description;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            coupon_id = itemView.findViewById(R.id.coupon_id);
            coupon_code = itemView.findViewById(R.id.coupon_code);
            coupon_amount = itemView.findViewById(R.id.coupon_details);
            coupon_description = itemView.findViewById(R.id.coupon_descritption);



        }
    }

}
