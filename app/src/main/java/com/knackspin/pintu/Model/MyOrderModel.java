package com.knackspin.pintu.Model;

public class MyOrderModel {

    private String id;
    private String order_id;
    private String order_amount;
    private String order_date;
    private String order_status;

    public MyOrderModel(String order_id, String order_amount, String order_date, String order_status) {
        this.order_id = order_id;
        this.order_amount = order_amount;
        this.order_date = order_date;
        this.order_status = order_status;
    }

    /*public MyOrderModel( String order_id, String order_amount, String order_date) {
        this.order_id = order_id;
        this.order_amount = order_amount;
        this.order_date = order_date;
    }*/

    public String getOrder_status() {
        return order_status;
    }

    public void setOrder_status(String order_status) {
        this.order_status = order_status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getOrder_amount() {
        return order_amount;
    }

    public void setOrder_amount(String order_amount) {
        this.order_amount = order_amount;
    }

    public String getOrder_date() {
        return order_date;
    }

    public void setOrder_date(String order_date) {
        this.order_date = order_date;
    }
}
