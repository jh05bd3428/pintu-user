package com.knackspin.pintu.Model;
public class CheckRewardModel
{
    private String check_id;
    private String reward_image_id;
    private String reward_p_name;
    private String reward_p_price;
    private String reward_p_point;


    public CheckRewardModel(String reward_image_id, String reward_p_name, String reward_p_price, String reward_p_point) {
        this.reward_image_id = reward_image_id;
        this.reward_p_name = reward_p_name;
        this.reward_p_price = reward_p_price;
        this.reward_p_point = reward_p_point;
    }

    public String getReward_image_id() {
        return reward_image_id;
    }

    public void setReward_image_id(String reward_image_id) {
        this.reward_image_id = reward_image_id;
    }

    public String getReward_p_name() {
        return reward_p_name;
    }

    public void setReward_p_name(String reward_p_name) {
        this.reward_p_name = reward_p_name;
    }

    public String getReward_p_price() {
        return reward_p_price;
    }

    public void setReward_p_price(String reward_p_price) {
        this.reward_p_price = reward_p_price;
    }

    public String getReward_p_point() {
        return reward_p_point;
    }

    public void setReward_p_point(String reward_p_point) {
        this.reward_p_point = reward_p_point;
    }
}
