package com.knackspin.pintu.Model;

import java.util.List;

public class SearchModel {

    private String search_product_id;
    private String search_brand_name;
    private String search_product_name;
    private String search_product_description;
    private String search_product_image;
    private String search_actual_price;
    private String search_offered_price;
    private String search_variant_weight;
    private String search_variant_id;

    private List<SearchDialogModel> searchdialogModel;


    public SearchModel(String search_product_id, String search_brand_name, String search_product_name, String search_product_image, List<SearchDialogModel> searchdialogModel) {
        this.search_product_id = search_product_id;
        this.search_brand_name = search_brand_name;
        this.search_product_name = search_product_name;
        this.search_product_image = search_product_image;
        this.searchdialogModel = searchdialogModel;
    }


    public String getSearch_product_id() {
        return search_product_id;
    }

    public void setSearch_product_id(String search_product_id) {
        this.search_product_id = search_product_id;
    }

    public String getSearch_brand_name() {
        return search_brand_name;
    }

    public void setSearch_brand_name(String search_brand_name) {
        this.search_brand_name = search_brand_name;
    }

    public String getSearch_product_name() {
        return search_product_name;
    }

    public void setSearch_product_name(String search_product_name) {
        this.search_product_name = search_product_name;
    }

    public String getSearch_product_description() {
        return search_product_description;
    }

    public void setSearch_product_description(String search_product_description) {
        this.search_product_description = search_product_description;
    }

    public String getSearch_product_image() {
        return search_product_image;
    }

    public void setSearch_product_image(String search_product_image) {
        this.search_product_image = search_product_image;
    }

    public String getSearch_actual_price() {
        return search_actual_price;
    }

    public void setSearch_actual_price(String search_actual_price) {
        this.search_actual_price = search_actual_price;
    }

    public String getSearch_offered_price() {
        return search_offered_price;
    }

    public void setSearch_offered_price(String search_offered_price) {
        this.search_offered_price = search_offered_price;
    }

    public String getSearch_variant_weight() {
        return search_variant_weight;
    }

    public void setSearch_variant_weight(String search_variant_weight) {
        this.search_variant_weight = search_variant_weight;
    }

    public String getSearch_variant_id() {
        return search_variant_id;
    }

    public void setSearch_variant_id(String search_variant_id) {
        this.search_variant_id = search_variant_id;
    }

    public List<SearchDialogModel> getSearchdialogModel() {
        return searchdialogModel;
    }

    public void setSearchdialogModel(List<SearchDialogModel> searchdialogModel) {
        this.searchdialogModel = searchdialogModel;
    }
}
