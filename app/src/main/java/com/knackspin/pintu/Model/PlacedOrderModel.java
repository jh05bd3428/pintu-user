package com.knackspin.pintu.Model;

public class PlacedOrderModel {

    private String placed_order_brand_name;
    private String placed_order_product_name;
    private String placed_order_variant_type;
    private String placed_order_quantity;
    private String placed_order_product_price;
    private String placed_order_product_image;


    public PlacedOrderModel(String placed_order_brand_name, String placed_order_product_name, String placed_order_variant_type,
                            String placed_order_quantity, String placed_order_product_price, String placed_order_product_image) {
        this.placed_order_brand_name = placed_order_brand_name;
        this.placed_order_product_name = placed_order_product_name;
        this.placed_order_variant_type = placed_order_variant_type;
        this.placed_order_quantity = placed_order_quantity;
        this.placed_order_product_price = placed_order_product_price;
        this.placed_order_product_image = placed_order_product_image;
    }


    public String getPlaced_order_brand_name() {
        return placed_order_brand_name;
    }

    public void setPlaced_order_brand_name(String placed_order_brand_name) {
        this.placed_order_brand_name = placed_order_brand_name;
    }

    public String getPlaced_order_product_name() {
        return placed_order_product_name;
    }

    public void setPlaced_order_product_name(String placed_order_product_name) {
        this.placed_order_product_name = placed_order_product_name;
    }

    public String getPlaced_order_variant_type() {
        return placed_order_variant_type;
    }

    public void setPlaced_order_variant_type(String placed_order_variant_type) {
        this.placed_order_variant_type = placed_order_variant_type;
    }

    public String getPlaced_order_quantity() {
        return placed_order_quantity;
    }

    public void setPlaced_order_quantity(String placed_order_quantity) {
        this.placed_order_quantity = placed_order_quantity;
    }

    public String getPlaced_order_product_price() {
        return placed_order_product_price;
    }

    public void setPlaced_order_product_price(String placed_order_product_price) {
        this.placed_order_product_price = placed_order_product_price;
    }

    public String getPlaced_order_product_image() {
        return placed_order_product_image;
    }

    public void setPlaced_order_product_image(String placed_order_product_image) {
        this.placed_order_product_image = placed_order_product_image;
    }
}
