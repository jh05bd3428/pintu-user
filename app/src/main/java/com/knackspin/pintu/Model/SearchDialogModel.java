package com.knackspin.pintu.Model;

import java.io.Serializable;

public class SearchDialogModel implements Serializable {


    private String dialog_variant_id;
    private String dialog_variant_product_id;
    private String dialog_variant_name;
    private String dialog_mrp_price;
    private String dialog_offered_price;

    public SearchDialogModel(String dialog_variant_id, String dialog_variant_product_id, String dialog_variant_name, String dialog_mrp_price, String dialog_offered_price) {
        this.dialog_variant_id = dialog_variant_id;
        this.dialog_variant_product_id = dialog_variant_product_id;
        this.dialog_variant_name = dialog_variant_name;
        this.dialog_mrp_price = dialog_mrp_price;
        this.dialog_offered_price = dialog_offered_price;
    }


    public String getDialog_variant_id() {
        return dialog_variant_id;
    }

    public void setDialog_variant_id(String dialog_variant_id) {
        this.dialog_variant_id = dialog_variant_id;
    }

    public String getDialog_variant_product_id() {
        return dialog_variant_product_id;
    }

    public void setDialog_variant_product_id(String dialog_variant_product_id) {
        this.dialog_variant_product_id = dialog_variant_product_id;
    }

    public String getDialog_variant_name() {
        return dialog_variant_name;
    }

    public void setDialog_variant_name(String dialog_variant_name) {
        this.dialog_variant_name = dialog_variant_name;
    }

    public String getDialog_mrp_price() {
        return dialog_mrp_price;
    }

    public void setDialog_mrp_price(String dialog_mrp_price) {
        this.dialog_mrp_price = dialog_mrp_price;
    }

    public String getDialog_offered_price() {
        return dialog_offered_price;
    }

    public void setDialog_offered_price(String dialog_offered_price) {
        this.dialog_offered_price = dialog_offered_price;
    }
}
