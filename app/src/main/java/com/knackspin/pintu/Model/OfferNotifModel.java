package com.knackspin.pintu.Model;

import java.io.Serializable;
import java.util.List;

public class OfferNotifModel implements Serializable {

    private String offer_product_id;
    private String offer_brand_name;
    private String offer_product_name;
    private String offer_product_description;
    private String offer_product_image;
    private String offer_actual_price;
    private String offer_offered_price;
    private String offer_variant_weight;
    private String offer_variant_id;


    private List<OfferDialogModel> dialogModels;

    public OfferNotifModel(String offer_product_id, String offer_brand_name, String offer_product_name, String offer_product_image,List<OfferDialogModel> dialogModels) {
        this.offer_product_id = offer_product_id;
        this.offer_brand_name = offer_brand_name;
        this.offer_product_name = offer_product_name;
        this.offer_product_image = offer_product_image;
        this.dialogModels = dialogModels;
    }


    public String getOffer_product_id() {
        return offer_product_id;
    }

    public void setOffer_product_id(String offer_product_id) {
        this.offer_product_id = offer_product_id;
    }

    public String getOffer_brand_name() {
        return offer_brand_name;
    }

    public void setOffer_brand_name(String offer_brand_name) {
        this.offer_brand_name = offer_brand_name;
    }

    public String getOffer_product_name() {
        return offer_product_name;
    }

    public void setOffer_product_name(String offer_product_name) {
        this.offer_product_name = offer_product_name;
    }

    public String getOffer_product_description() {
        return offer_product_description;
    }

    public void setOffer_product_description(String offer_product_description) {
        this.offer_product_description = offer_product_description;
    }

    public String getOffer_product_image() {
        return offer_product_image;
    }

    public void setOffer_product_image(String offer_product_image) {
        this.offer_product_image = offer_product_image;
    }

    public String getOffer_actual_price() {
        return offer_actual_price;
    }

    public void setOffer_actual_price(String offer_actual_price) {
        this.offer_actual_price = offer_actual_price;
    }

    public String getOffer_offered_price() {
        return offer_offered_price;
    }

    public void setOffer_offered_price(String offer_offered_price) {
        this.offer_offered_price = offer_offered_price;
    }

    public String getOffer_variant_weight() {
        return offer_variant_weight;
    }

    public void setOffer_variant_weight(String offer_variant_weight) {
        this.offer_variant_weight = offer_variant_weight;
    }

    public String getOffer_variant_id() {
        return offer_variant_id;
    }

    public void setOffer_variant_id(String offer_variant_id) {
        this.offer_variant_id = offer_variant_id;
    }

    public List<OfferDialogModel> getDialogModels() {
        return dialogModels;
    }

    public void setDialogModels(List<OfferDialogModel> dialogModels) {
        this.dialogModels = dialogModels;
    }
}
