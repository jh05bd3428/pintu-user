package com.knackspin.pintu.Model;

public class CouponModel {

    private String coupon_id;
    private String coupon_code;
    private String coupon_amount;
    private String coupon_description;

    public CouponModel(String coupon_id, String coupon_code, String coupon_amount, String coupon_description) {
        this.coupon_id = coupon_id;
        this.coupon_code = coupon_code;
        this.coupon_amount = coupon_amount;
        this.coupon_description = coupon_description;
    }


    public String getCoupon_id() {
        return coupon_id;
    }

    public void setCoupon_id(String coupon_id) {
        this.coupon_id = coupon_id;
    }

    public String getCoupon_code() {
        return coupon_code;
    }

    public void setCoupon_code(String coupon_code) {
        this.coupon_code = coupon_code;
    }

    public String getCoupon_amount() {
        return coupon_amount;
    }

    public void setCoupon_amount(String coupon_amount) {
        this.coupon_amount = coupon_amount;
    }

    public String getCoupon_description() {
        return coupon_description;
    }

    public void setCoupon_description(String coupon_description) {
        this.coupon_description = coupon_description;
    }
}
