package com.knackspin.pintu.Model;

public class RandomCategory {

    String random_sc_id,random_c_id,random_cat_image,random_cat_name;

    public RandomCategory(String random_sc_id, String random_c_id, String random_cat_image, String random_cat_name) {
        this.random_sc_id = random_sc_id;
        this.random_c_id = random_c_id;
        this.random_cat_image = random_cat_image;
        this.random_cat_name = random_cat_name;
    }


    public String getRandom_sc_id() {
        return random_sc_id;
    }

    public void setRandom_sc_id(String random_sc_id) {
        this.random_sc_id = random_sc_id;
    }

    public String getRandom_c_id() {
        return random_c_id;
    }

    public void setRandom_c_id(String random_c_id) {
        this.random_c_id = random_c_id;
    }

    public String getRandom_cat_image() {
        return random_cat_image;
    }

    public void setRandom_cat_image(String random_cat_image) {
        this.random_cat_image = random_cat_image;
    }

    public String getRandom_cat_name() {
        return random_cat_name;
    }

    public void setRandom_cat_name(String random_cat_name) {
        this.random_cat_name = random_cat_name;
    }
}
