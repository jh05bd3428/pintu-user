package com.knackspin.pintu.Model;

import android.widget.ImageView;

public class SliderModel {

    private ImageView slider_image;


    public SliderModel(ImageView slider_image) {
        this.slider_image = slider_image;
    }


    public ImageView getSlider_image() {
        return slider_image;
    }

    public void setSlider_image(ImageView slider_image) {
        this.slider_image = slider_image;
    }
}
