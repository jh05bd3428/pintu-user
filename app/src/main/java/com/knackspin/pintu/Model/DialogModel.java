package com.knackspin.pintu.Model;

import java.io.Serializable;

public class DialogModel implements Serializable {

    private String variant_id;
    private String variant_product_id;
    private String variant_name;
    private String mrp_price;
    private String offered_price;




    public DialogModel(String variant_id, String variant_product_id, String variant_name, String mrp_price, String offered_price) {
        this.variant_id = variant_id;
        this.variant_product_id = variant_product_id;
        this.variant_name = variant_name;
        this.mrp_price = mrp_price;
        this.offered_price = offered_price;
    }

    public String getVariant_product_id() {
        return variant_product_id;
    }

    public void setVariant_product_id(String variant_product_id) {
        this.variant_product_id = variant_product_id;
    }

    public String getVariant_id() {
        return variant_id;
    }

    public void setVariant_id(String variant_id) {
        this.variant_id = variant_id;
    }

    public String getVariant_name() {
        return variant_name;
    }

    public void setVariant_name(String variant_name) {
        this.variant_name = variant_name;
    }

    public String getMrp_price() {
        return mrp_price;
    }

    public void setMrp_price(String mrp_price) {
        this.mrp_price = mrp_price;
    }

    public String getOffered_price() {
        return offered_price;
    }

    public void setOffered_price(String offered_price) {
        this.offered_price = offered_price;
    }
}
