package com.knackspin.pintu.Model;

public class SliderNewModel {

    private String image_id;
    private String imageUrl;

    public SliderNewModel(String image_id, String imageUrl) {
        this.image_id = image_id;
        this.imageUrl = imageUrl;
    }

    public String getImage_id() {
        return image_id;
    }

    public void setImage_id(String image_id) {
        this.image_id = image_id;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
