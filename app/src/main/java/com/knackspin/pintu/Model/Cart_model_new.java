package com.knackspin.pintu.Model;

public class Cart_model_new {

   private String product_id;
   private String product_quantity;
   private String variant_id;

    public String getVariant_id() {
        return variant_id;
    }

    public void setVariant_id(String variant_id) {
        this.variant_id = variant_id;
    }

    public String getProduct_id()
    {
          return product_id;
    }

    public void setProduct_id(String product_id)
    {
        this.product_id = product_id;
    }


    public String getProduct_quantity()
    {
        return product_quantity;
    }

    public void setProduct_quantity (String product_quantity)
        {
            this.product_quantity = product_quantity;
        }
}



