package com.knackspin.pintu.Model;

public class AddressModel {

    private String full_name;
    private String address;
    private String street;
    private String city;
    private String state;
    private String zip;
    private String phone_number;

    public AddressModel(String full_name, String address, String street, String city, String state, String zip, String phone_number) {
        this.full_name = full_name;
        this.address = address;
        this.street = street;
        this.city = city;
        this.state = state;
        this.zip = zip;
        this.phone_number = phone_number;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }
}
