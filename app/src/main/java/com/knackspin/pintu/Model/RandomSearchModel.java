package com.knackspin.pintu.Model;

public class RandomSearchModel {

    private String ser_ran_sub_cat_id;
    private String ser_ran_cat_id;
    private String ser_ran_name;
    private String ser_ran_image;

    public RandomSearchModel(String ser_ran_sub_cat_id, String ser_ran_cat_id, String ser_ran_name, String ser_ran_image) {
        this.ser_ran_sub_cat_id = ser_ran_sub_cat_id;
        this.ser_ran_cat_id = ser_ran_cat_id;
        this.ser_ran_name = ser_ran_name;
        this.ser_ran_image = ser_ran_image;
    }

    public String getSer_ran_sub_cat_id() {
        return ser_ran_sub_cat_id;
    }

    public void setSer_ran_sub_cat_id(String ser_ran_sub_cat_id) {
        this.ser_ran_sub_cat_id = ser_ran_sub_cat_id;
    }

    public String getSer_ran_cat_id() {
        return ser_ran_cat_id;
    }

    public void setSer_ran_cat_id(String ser_ran_cat_id) {
        this.ser_ran_cat_id = ser_ran_cat_id;
    }

    public String getSer_ran_name() {
        return ser_ran_name;
    }

    public void setSer_ran_name(String ser_ran_name) {
        this.ser_ran_name = ser_ran_name;
    }

    public String getSer_ran_image() {
        return ser_ran_image;
    }

    public void setSer_ran_image(String ser_ran_image) {
        this.ser_ran_image = ser_ran_image;
    }
}
