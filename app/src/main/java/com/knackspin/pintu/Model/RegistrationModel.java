package com.knackspin.pintu.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public abstract class RegistrationModel {

    @Expose
    @SerializedName("msg")
    private String msg;
    @Expose
    @SerializedName("userid")
    private int userid;
    @Expose
    @SerializedName("flag")
    private boolean flag;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public boolean getFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }
}
