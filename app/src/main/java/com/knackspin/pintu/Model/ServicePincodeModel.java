package com.knackspin.pintu.Model;

public class ServicePincodeModel {

    private String service_shop_id;
    private String service_shop_image;
    private String service_shop_name;
    private String service_shop_address;
    private String service_pincode;
    private String service_shop_delivery_charge;
    private String service_shop_minimum_charge;

    public ServicePincodeModel(String service_shop_id, String service_shop_image, String service_shop_name, String service_shop_address,
                               String service_pincode, String service_shop_delivery_charge, String service_shop_minimum_charge) {
        this.service_shop_id = service_shop_id;
        this.service_shop_image = service_shop_image;
        this.service_shop_name = service_shop_name;
        this.service_shop_address = service_shop_address;
        this.service_pincode = service_pincode;
        this.service_shop_delivery_charge = service_shop_delivery_charge;
        this.service_shop_minimum_charge = service_shop_minimum_charge;
    }

    public String getService_pincode() {
        return service_pincode;
    }

    public void setService_pincode(String service_pincode) {
        this.service_pincode = service_pincode;
    }

    public String getService_shop_name() {
        return service_shop_name;
    }

    public void setService_shop_name(String service_shop_name) {
        this.service_shop_name = service_shop_name;
    }

    public String getService_shop_address() {
        return service_shop_address;
    }

    public void setService_shop_address(String service_shop_address) {
        this.service_shop_address = service_shop_address;
    }

    public String getService_shop_image() {
        return service_shop_image;
    }

    public void setService_shop_image(String service_shop_image) {
        this.service_shop_image = service_shop_image;
    }

    public String getService_shop_id() {
        return service_shop_id;
    }

    public void setService_shop_id(String service_shop_id) {
        this.service_shop_id = service_shop_id;
    }

    public String getService_shop_delivery_charge() {
        return service_shop_delivery_charge;
    }

    public void setService_shop_delivery_charge(String service_shop_delivery_charge) {
        this.service_shop_delivery_charge = service_shop_delivery_charge;
    }

    public String getService_shop_minimum_charge() {
        return service_shop_minimum_charge;
    }

    public void setService_shop_minimum_charge(String service_shop_minimum_charge) {
        this.service_shop_minimum_charge = service_shop_minimum_charge;
    }
}
