package com.knackspin.pintu.Model;

public class Fragmnet_model {


    String name;
    String subId;

    public Fragmnet_model(String name,String subId) {
        this.name = name;
        this.subId = subId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSubId() {
        return subId;
    }

    public void setSubId(String subId) {
        this.subId = subId;
    }
}
