package com.knackspin.pintu.Model;



public class RewardModel {

    private String reward_id;
    private String reward_points;

    public RewardModel(String reward_id, String reward_points) {
        this.reward_id = reward_id;
        this.reward_points = reward_points;
    }


    public String getReward_id() {
        return reward_id;
    }

    public void setReward_id(String reward_id) {
        this.reward_id = reward_id;
    }

    public String getReward_points() {
        return reward_points;
    }

    public void setReward_points(String reward_points) {
        this.reward_points = reward_points;
    }
}
