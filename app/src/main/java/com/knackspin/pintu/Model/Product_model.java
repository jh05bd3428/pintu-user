package com.knackspin.pintu.Model;

import java.io.Serializable;
import java.util.List;

public class Product_model implements Serializable {

    private String product_id;
    private String brand_name;
    private String product_name;
    private String product_description;
    private String product_image;
    private String actual_price;
    private String offered_price;
    private String variant_weight;
    private String variant_id;



    private List<DialogModel> dialogModel;

    public Product_model(String product_id,String brand_name, String product_name, String product_image, List<DialogModel> dialogModel)
    {
        this.product_id = product_id;
        this.brand_name = brand_name;
        this.product_name = product_name;
        this.product_image = product_image;
        this.dialogModel = dialogModel;
    }

    public Product_model(String brand_name, String product_name, String product_image) {
        this.brand_name = brand_name;
        this.product_name = product_name;
        this.product_image = product_image;
    }

    public String getVariant_id() {
        return variant_id;
    }

    public void setVariant_id(String variant_id) {
        this.variant_id = variant_id;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public List<DialogModel> getDialogModel() {
        return dialogModel;
    }

    public void setDialogModel(List<DialogModel> dialogModel) {
        this.dialogModel = dialogModel;
    }

    public String getVariant_weight() {
        return variant_weight;
    }

    public void setVariant_weight(String variant_weight) {
        this.variant_weight = variant_weight;
    }

    public String getActual_price() {
        return actual_price;
    }

    public void setActual_price(String actual_price) {
        this.actual_price = actual_price;
    }

    public String getOffered_price() {
        return offered_price;
    }

    public void setOffered_price(String offered_price) {
        this.offered_price = offered_price;
    }




    public String getBrand_name() {
        return brand_name;
    }

    public void setBrand_name(String brand_name) {
        this.brand_name = brand_name;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getProduct_description() {
        return product_description;
    }

    public void setProduct_description(String product_description) {
        this.product_description = product_description;
    }

    public String getProduct_image() {
        return product_image;
    }

    public void setProduct_image(String product_image) {
        this.product_image = product_image;
    }


}


