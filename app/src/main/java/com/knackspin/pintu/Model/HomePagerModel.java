package com.knackspin.pintu.Model;

import android.os.Build;
import android.transition.Slide;

import androidx.annotation.RequiresApi;

@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
public class HomePagerModel extends Slide {

    private String image_id;
    private String image_name;

    public HomePagerModel(String image_id, String image_name) {
        this.image_id = image_id;
        this.image_name = image_name;
    }

    public String getImage_id() {
        return image_id;
    }

    public void setImage_id(String image_id) {
        this.image_id = image_id;
    }

    public String getImage_name() {
        return image_name;
    }

    public void setImage_name(String image_name) {
        this.image_name = image_name;
    }
}
