package com.knackspin.pintu.Model;

import java.io.Serializable;

public class OfferDialogModel implements Serializable {

    private String offer_dialog_variant_id;
    private String offer_dialog_variant_product_id;
    private String offer_dialog_variant_name;
    private String offer_dialog_mrp_price;
    private String offer_dialog_offered_price;


    public OfferDialogModel(String offer_dialog_variant_id, String offer_dialog_variant_product_id, String offer_dialog_variant_name,
                            String offer_dialog_mrp_price, String offer_dialog_offered_price) {
        this.offer_dialog_variant_id = offer_dialog_variant_id;
        this.offer_dialog_variant_product_id = offer_dialog_variant_product_id;
        this.offer_dialog_variant_name = offer_dialog_variant_name;
        this.offer_dialog_mrp_price = offer_dialog_mrp_price;
        this.offer_dialog_offered_price = offer_dialog_offered_price;
    }

    public String getOffer_dialog_variant_id() {
        return offer_dialog_variant_id;
    }

    public void setOffer_dialog_variant_id(String offer_dialog_variant_id) {
        this.offer_dialog_variant_id = offer_dialog_variant_id;
    }

    public String getOffer_dialog_variant_product_id() {
        return offer_dialog_variant_product_id;
    }

    public void setOffer_dialog_variant_product_id(String offer_dialog_variant_product_id) {
        this.offer_dialog_variant_product_id = offer_dialog_variant_product_id;
    }

    public String getOffer_dialog_variant_name() {
        return offer_dialog_variant_name;
    }

    public void setOffer_dialog_variant_name(String offer_dialog_variant_name) {
        this.offer_dialog_variant_name = offer_dialog_variant_name;
    }

    public String getOffer_dialog_mrp_price() {
        return offer_dialog_mrp_price;
    }

    public void setOffer_dialog_mrp_price(String offer_dialog_mrp_price) {
        this.offer_dialog_mrp_price = offer_dialog_mrp_price;
    }

    public String getOffer_dialog_offered_price() {
        return offer_dialog_offered_price;
    }

    public void setOffer_dialog_offered_price(String offer_dialog_offered_price) {
        this.offer_dialog_offered_price = offer_dialog_offered_price;
    }
}
