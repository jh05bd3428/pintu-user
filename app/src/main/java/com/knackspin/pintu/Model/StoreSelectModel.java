package com.knackspin.pintu.Model;

public class StoreSelectModel {

    private String store_id;
    private String store_image;
    private String store_name;
    private String store_address;
    private String store_delievry_charge;
    private String store_minimum_delivery;

    public StoreSelectModel(String store_id, String store_image, String store_name, String store_address, String store_delievry_charge,
                            String store_minimum_delivery) {
        this.store_id = store_id;
        this.store_image = store_image;
        this.store_name = store_name;
        this.store_address = store_address;
        this.store_delievry_charge = store_delievry_charge;
        this.store_minimum_delivery = store_minimum_delivery;
    }

    public String getStore_id() {
        return store_id;
    }

    public void setStore_id(String store_id) {
        this.store_id = store_id;
    }

    public String getStore_image() {
        return store_image;
    }

    public void setStore_image(String store_image) {
        this.store_image = store_image;
    }

    public String getStore_name() {
        return store_name;
    }

    public void setStore_name(String store_name) {
        this.store_name = store_name;
    }

    public String getStore_address() {
        return store_address;
    }

    public void setStore_address(String store_address) {
        this.store_address = store_address;
    }

    public String getStore_delievry_charge() {
        return store_delievry_charge;
    }

    public void setStore_delievry_charge(String store_delievry_charge) {
        this.store_delievry_charge = store_delievry_charge;
    }

    public String getStore_minimum_delivery() {
        return store_minimum_delivery;
    }

    public void setStore_minimum_delivery(String store_minimum_delivery) {
        this.store_minimum_delivery = store_minimum_delivery;
    }
}
