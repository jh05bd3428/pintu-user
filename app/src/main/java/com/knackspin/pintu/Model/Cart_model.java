package com.knackspin.pintu.Model;

public class Cart_model {

    private String product_id;
    private String brand_name;
    private String product_name;
    private String variant_weight;
    private String actual_price;
    private String offered_price;
    private String quantity;
    private String product_Url;
    private String cart_variant_id;


    public String getCart_variant_id() {
        return cart_variant_id;
    }

    public void setCart_variant_id(String cart_variant_id) {
        this.cart_variant_id = cart_variant_id;
    }

    public String getProduct_Url() {
        return product_Url;
    }

    public void setProduct_Url(String product_Url) {
        this.product_Url = product_Url;
    }


    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getBrand_name() {
        return brand_name;
    }

    public void setBrand_name(String brand_name) {
        this.brand_name = brand_name;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getVariant_weight() {
        return variant_weight;
    }

    public void setVariant_weight(String variant_weight) {
        this.variant_weight = variant_weight;
    }

    public String getActual_price() {
        return actual_price;
    }

    public void setActual_price(String actual_price) {
        this.actual_price = actual_price;
    }

    public String getOffered_price() {
        return offered_price;
    }

    public void setOffered_price(String offered_price) {
        this.offered_price = offered_price;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }
}
