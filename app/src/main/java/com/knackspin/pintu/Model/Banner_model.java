package com.knackspin.pintu.Model;


public class Banner_model {

    private String imageID;
    //private String imageName;
    private String image;

    public Banner_model(String imageID,  String image) {
        this.imageID = imageID;
       // this.imageName = imageName;
        this.image = image;
    }


    public String getImageID() {
        return imageID;
    }

    public void setImageID(String imageID) {
        this.imageID = imageID;
    }

   /* public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }*/

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
