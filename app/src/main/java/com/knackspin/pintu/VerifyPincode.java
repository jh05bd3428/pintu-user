package com.knackspin.pintu;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.knackspin.pintu.Adapter.SelectStoreAdapter;
import com.knackspin.pintu.Model.StoreSelectModel;
import com.knackspin.pintu.Singleton.Singleton;
import com.knackspin.pintu.utils.UrlPoint;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;

public class VerifyPincode extends AppCompatActivity implements LocationListener {


    private static final String TAG = VerifyPincode.class.getSimpleName();


    EditText pincode_text;
    Button  pincode_button;

    RecyclerView recyclerView;
    private List<StoreSelectModel> listOfStores;
    SelectStoreAdapter selectStoreAdapter;
    LinearLayoutManager linearLayoutManager;

    LinearLayout empty_store_layout;

    private boolean callApi = false;
    public static final int PASS_DATA = 103;

    List<Address> addresses;
    Geocoder geocoder;
    double n_latitude, n_longitude;
    LocationManager locationManager;

    ProgressDialog progressDialog;

    TextView tv_view_store;

    boolean pin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_pincode);


        initialization();



        addresses = new ArrayList<>();
        geocoder = new Geocoder(this, Locale.getDefault());


        listOfStores = new ArrayList<>();
        recyclerView = findViewById(R.id.store_details_rv);
        selectStoreAdapter = new SelectStoreAdapter(listOfStores, this);

        recyclerView.setHasFixedSize(true);
        linearLayoutManager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,true);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(selectStoreAdapter);

        pincode_text.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (pincode_text.getText().length() == 6 && callApi == false)
                {
                    getStoreDetails();
                    callApi = true;
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (!(pincode_text.getText().length() == 6))
                {
                    callApi = false;
                }

            }
        });

        tv_view_store.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(VerifyPincode.this, ServisablePincode.class));
                finish();
            }
        });



    }


    private void getStoreDetails() {

        CustomLayout.setUIToWait(VerifyPincode.this,true);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, UrlPoint.search_pincode, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                CustomLayout.setUIToWait(VerifyPincode.this,false);

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    if (listOfStores!= null)
                    {
                        listOfStores.clear();
                    }

                    if (jsonObject.optString("flag").equalsIgnoreCase("true"))
                    {
                        empty_store_layout.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);


                        JSONArray dataArray = jsonObject.getJSONArray("data");
                        for (int k=0; k<dataArray.length(); k++)
                        {
                            JSONObject ob1 = dataArray.getJSONObject(k);

                            StoreSelectModel ssm = new StoreSelectModel(ob1.optString("id"),ob1.optString("photo"),
                                    ob1.optString("shop_name"), ob1.optString("street_address"),
                                    ob1.optString("delivery_charge"),ob1.optString("minimum_amout"));

                            listOfStores.add(ssm);
                        }

                        recyclerView.setAdapter(selectStoreAdapter);

                    }
                    else
                        {
                            empty_store_layout.setVisibility(View.VISIBLE);
                            Toast.makeText(VerifyPincode.this, "No store found", Toast.LENGTH_SHORT).show();
                        }



                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                CustomLayout.setUIToWait(VerifyPincode.this,false);

                Toast.makeText(VerifyPincode.this, "Something went wrong", Toast.LENGTH_SHORT).show();

            }
        })

        {

            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<String, String>();
                params.put("pincode",pincode_text.getText().toString());
                return params;
            }

        };

        Singleton.getInstance(VerifyPincode.this).addTorequestqu(stringRequest);

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        );

    }



    private void initialization() {

        pincode_text = findViewById(R.id.pincode_text);
        pincode_button = findViewById(R.id.verify_pincode_button);
        empty_store_layout = findViewById(R.id.empty_store_layout);
        tv_view_store = findViewById(R.id.tv_view_store);




    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();


        if (pin)
        {
            startActivity(new Intent(VerifyPincode.this, HomeScreen.class));
            finish();
        }

        else
            {
                finish();
            }



    }

    private void checkLocationPermission() {


        if(CheckingPermissionIsEnabledOrNot())
        {
            Toast.makeText(VerifyPincode.this, "All Permissions Granted Successfully", Toast.LENGTH_LONG).show();
            getPincode();
        }

        else
        {
            grantPermission();
        }
    }

    private void grantPermission()
    {

        ActivityCompat.requestPermissions(VerifyPincode.this, new String[]
                {
                        ACCESS_FINE_LOCATION

                }, PASS_DATA);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode)
        {
            case 101:

                if (grantResults.length > 0)
                {
                    boolean LocationPermission = grantResults[0] == PackageManager.PERMISSION_GRANTED;

                    if (LocationPermission) {

                        Toast.makeText(VerifyPincode.this, "Permission Granted", Toast.LENGTH_LONG).show();
                        getPincode();

                    }
                    else {
                        Toast.makeText(VerifyPincode.this,"Permission Denied",Toast.LENGTH_LONG).show();

                    }
                }

                break;
        }
    }

    private void getPincode() {

      //  Toast.makeText(this, "Inside Pincode", Toast.LENGTH_SHORT).show();



        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    Activity#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for Activity#requestPermissions for more details.
                return;
            }
        }

        try {

            Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            onLocationChanged(location);

        }
        catch (NullPointerException ne)
        {
            ne.getMessage();
        }

    }

    public boolean CheckingPermissionIsEnabledOrNot() {

        int FirstPermissionResult = ContextCompat.checkSelfPermission(getApplicationContext(), ACCESS_FINE_LOCATION);
        return FirstPermissionResult == PackageManager.PERMISSION_GRANTED ;

    }

    @Override
    public void onLocationChanged(Location location) {


        n_longitude = location.getLongitude();
        n_latitude = location.getLatitude();


        Log.d("Location", "Longitude"+ n_longitude + " " +"Latitude" + n_latitude );

        displayPincode();


    }



    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    private void displayPincode() {

        try {
            addresses = geocoder.getFromLocation(n_latitude, n_longitude, 1);

            String address = addresses.get(0).getPostalCode();

            pincode_text.setText(address);
            Log.d("Pincode", "Pincode is "+address);

        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    @Override
    protected void onResume() {
        super.onResume();

        checkLocationPermission();

        if (getIntent().getExtras() != null)
        {
            getIntent().getExtras().getBoolean("pin");

        }

    }


}
