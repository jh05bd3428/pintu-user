package com.knackspin.pintu;

import android.content.Context;
import android.content.SharedPreferences;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.knackspin.pintu.Singleton.Singleton;
import com.knackspin.pintu.utils.ConnectionCheck;
import com.knackspin.pintu.utils.UrlPoint;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Wallet extends AppCompatActivity {


    TextView wallet_username, wallet_email, wallet_contact,wallet_balance,credit_limit;
    Button use_reward;
    public static final String DEFAULT = "NA";
    String u_id;
    String provider_id ;
    String username,email,contact,balance,credit;
    ImageView wallet_back_image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet);

        initailization();

        SharedPreferences sharedPreferences = getSharedPreferences("MyData", Context.MODE_PRIVATE);
        u_id = sharedPreferences.getString("userId", DEFAULT);
        provider_id = sharedPreferences.getString("providerID",DEFAULT);
        username = sharedPreferences.getString("login_name", DEFAULT);
        email = sharedPreferences.getString("login_email", DEFAULT);
        contact = sharedPreferences.getString("login_phone", DEFAULT);
        credit = sharedPreferences.getString("login_credit_limit",DEFAULT);

        wallet_username.setText(username);
        wallet_email.setText(email);
        wallet_contact.setText(contact);
        credit_limit.setText(credit);

        wallet_back_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });

        connectionCheck();


    }

    private void connectionCheck() {

        CustomLayout.setUIToWait(Wallet.this,true);


        if(new ConnectionCheck(this).isNetworkAvailable())
        {

            CustomLayout.setUIToWait(Wallet.this,false);
            checkUID();
            getWalletData();

        } else
        {

            CustomLayout.setUIToWait(Wallet.this,false);
            Toast.makeText(this, "Please check your Internet connection", Toast.LENGTH_SHORT).show();
        }


    }

    private void checkUID() {

        if (u_id!=null)
        {
            getWalletData();
        } else
            {
                Toast.makeText(this, "Please login to view amount", Toast.LENGTH_SHORT).show();
            }


    }

    private void getWalletData() {

        CustomLayout.setUIToWait(Wallet.this,true);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, UrlPoint.getWalletPoints, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                CustomLayout.setUIToWait(Wallet.this,false);

                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(response);

                    if(jsonObject.optString("success").equalsIgnoreCase("true"))
                    {

                        String wallet = jsonObject.getString("data");
                        wallet_balance.setText("\u20B9" + wallet);

                        SharedPreferences sharedPreferences = getSharedPreferences("MyData",Context.MODE_PRIVATE); // creating xml file to store data
                        SharedPreferences.Editor editor = sharedPreferences.edit();// edit to store data into xml file
                        editor.putString("wallet_balance", wallet);
                        editor.apply();

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                CustomLayout.setUIToWait(Wallet.this,false);

                if(error instanceof ServerError)
                {

                    Toast.makeText(Wallet.this, "Server Down! Please try again", Toast.LENGTH_SHORT).show();
                }

                else if (error instanceof NetworkError)
                {
                    Toast.makeText(Wallet.this, "Check your Internet Connection", Toast.LENGTH_SHORT).show();
                }

                else
                {
                    Toast.makeText(Wallet.this, "Something Broken", Toast.LENGTH_SHORT).show();
                }

            }
        })

        {
            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id",u_id);
                params.put("provider_id",provider_id);
                return params;
            }
        };

        Singleton.getInstance(Wallet.this).addTorequestqu(stringRequest);

    }

    private void initailization() {

        wallet_username = findViewById(R.id.wallet_username);
        wallet_email =findViewById(R.id.wallet_email);
        wallet_contact = findViewById(R.id.wallet_contact);
        wallet_balance = findViewById(R.id.wallet_balance);
        wallet_back_image = findViewById(R.id.wallet_back_image);

        credit_limit = findViewById(R.id.credit_limit);
        /*wallet_amount_view = findViewById(R.id.wallet_amount_view);
        */
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
