package com.knackspin.pintu;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.knackspin.pintu.Adapter.CartAdapter;
import com.knackspin.pintu.Database.DatabaseHelper;
import com.knackspin.pintu.Model.Cart_model;
import com.knackspin.pintu.Model.Cart_model_new;
import com.knackspin.pintu.Model.DialogModel;

import java.util.ArrayList;

public class CartDetails extends AppCompatActivity  {

    ImageView back_image,delete_button,back_arrow;
    TextView final_amt;
    TextView checkout;
    RelativeLayout empty_cart_layout;
    DatabaseHelper databaseHelper;
    TextView price_details;
    public ArrayList<Cart_model> listOfCartItems;
    private ArrayList<DialogModel> listOfdialogData;
    private ArrayList<Cart_model_new> listOfIDs;
    private RecyclerView recyclerView;
    CartAdapter cartAdapter;
    CartDetails cart_details;
    LinearLayoutManager linearLayoutManager;
    String listSize,u_id;
    int delivery_fee = 30;
    public static final String DEFAULT = "NA";

    double finalPrice;

    @Override
     protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart_details);
        initialization();
        databaseHelper = new DatabaseHelper(this);
        listOfCartItems = new ArrayList<>();
        listOfdialogData = new ArrayList<>();
        listOfIDs = new ArrayList<>();

        listOfCartItems = databaseHelper.getAllCartItems();
        recyclerView.setHasFixedSize(true);
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        cartAdapter = new CartAdapter(listOfCartItems,this);
        recyclerView.setAdapter(cartAdapter);

        SharedPreferences sharedPreferences = getSharedPreferences("MyData", Context.MODE_PRIVATE);
        u_id = sharedPreferences.getString("userId",DEFAULT);
        Log.d("UID", u_id);

        checkList();

        checkout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (u_id  !=  DEFAULT)
                {
                    sendData();
                    //Toast.makeText(CartDetails.this, "True", Toast.LENGTH_SHORT).show();
                } else
                {
                    getUserId();
                }

               // sendData();
            }
        });

        back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });

    }

    private void getUserId() {

        Intent intent = new Intent(CartDetails.this, Login.class);
        intent.putExtra("type", "2");
        startActivity(intent);
        finish();
    }

    private void sendData() {

        listOfCartItems = databaseHelper.getAllCartItems();

        if(listOfCartItems.size() == 0)
        {
            checkout.setVisibility(View.GONE);
            empty_cart_layout.setVisibility(View.VISIBLE);
        }

        else
            {
                listSize = String.valueOf(listOfCartItems.size());
                checkout.setVisibility(View.VISIBLE);
                empty_cart_layout.setVisibility(View.GONE);
                Double final_bill_new = foodGrandtotal();
                Bundle bs = new Bundle();
                bs.putString("item_amount", String.valueOf(final_bill_new));
                bs.putString("Item Count", listSize);
                bs.putString("delivery", String.valueOf(delivery_fee));
                //bs.putString("final_amount", String.valueOf(finalPrice));
                Intent intent = new Intent(CartDetails.this, DetailsActivity.class );
                intent.putExtras(bs);
                startActivity(intent);
                //finish();
            }
    }

    public void checkList() {

        if(listOfCartItems.size() == 0)
        {

            empty_cart_layout.setVisibility(View.VISIBLE);
            checkout.setVisibility(View.GONE);
            recyclerView.setVisibility(View.GONE);
        } else {

            empty_cart_layout.setVisibility(View.GONE);
            checkout.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.VISIBLE);
            getData();
        }
    }

    public double foodGrandtotal() {

        double totalprice = 0;
        double part_price;


        for(int i=0; i< listOfCartItems.size(); i++)
        {
            part_price = 0;
            final Cart_model cart_model = listOfCartItems.get(i);
            String p = cart_model.getOffered_price();
            String quantity = cart_model.getQuantity();

            part_price = Double.parseDouble(p) * Integer.parseInt(quantity);

            totalprice = totalprice+part_price;

           /* if (part_price < 300)
            {
                totalprice = totalprice+ part_price;
                finalPrice = totalprice+delivery_fee;
                delivery_fee = 30;
                //final_amt.setText((int) finalPrice);
            }else
                {
                    totalprice = totalprice+part_price;
                    finalPrice = totalprice;
                    delivery_fee = 0;
                    //final_amt.setText((int) finalPrice);
                }*/

            Log.d("Toatalprice", String.valueOf(totalprice));
        }

        return totalprice ;
    }

    private void getData() {
           listOfCartItems = databaseHelper.getAllCartItems();
           foodGrandtotal();
    }

    private void initialization() {

        back_image = findViewById(R.id.back_image);
        delete_button = findViewById(R.id.delete_button);
        recyclerView = findViewById(R.id.cart_recylerview);
        empty_cart_layout = findViewById(R.id.empty_cart_layout);
        checkout = findViewById(R.id.checkout_button);
        back_arrow = findViewById(R.id.back_arrow);

    }

    @Override
    protected void onResume() {
        super.onResume();

        if(listOfCartItems.size() == 0)
        {
            checkout.setVisibility(View.GONE);
            empty_cart_layout.setVisibility(View.VISIBLE);
        }
    }



}
