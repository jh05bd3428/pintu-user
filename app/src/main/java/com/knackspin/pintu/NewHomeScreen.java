package com.knackspin.pintu;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.NonNull;

import com.knackspin.pintu.fragments.HomeAccountFragment;
import com.knackspin.pintu.fragments.HomeFragment;
import com.knackspin.pintu.fragments.OrderHistroyFragment;

public class NewHomeScreen extends AppCompatActivity {

    BottomNavigationView bottomNavigationView;
    FragmentTransaction transaction;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_home_screen);

        bottomNavigationView = findViewById(R.id.home_navigation);

        getSupportFragmentManager().beginTransaction().replace(R.id.video_layout_container, new HomeFragment()).commit();


        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

                Fragment selectedFragment = null;

                switch (menuItem.getItemId())
                {

                    case R.id.nav_home:
                        selectedFragment = new HomeFragment();
                        break;

                    case R.id.nav_explore:
                        selectedFragment = new OrderHistroyFragment();
                        break;

                    case R.id.nav_alerts:
                        selectedFragment = new HomeAccountFragment();
                        break;

                }

                getSupportFragmentManager().beginTransaction().replace(R.id.video_layout_container, selectedFragment).commit();
                return true;
            }
        });





    }
}
