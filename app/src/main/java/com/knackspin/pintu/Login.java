package com.knackspin.pintu;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.auth.api.phone.SmsRetrieverClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.knackspin.pintu.Singleton.Singleton;
import com.knackspin.pintu.broadcast_receiver.SmsBroadcastReceiver;
import com.knackspin.pintu.utils.UrlPoint;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Login extends AppCompatActivity  {



    EditText et_login_phone,et_login_otp,et_login_fname,et_login_lname,et_login_otp2,et_login_otp3,et_login_otp4;
    Button bt_verify_number,bt_submit_otp,bt_submit_details;
    RelativeLayout rl_phone_layout,rl_details_layout,rl_otp;
    ImageView iv_close_dialog;
    TextView tv_resend_otp;

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor,editor1;


    private static final int REQ_USER_CONSENT = 200;
    SmsBroadcastReceiver smsBroadcastReceiver;

    String mix_otp, otp;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        initialization();

        rl_phone_layout.setVisibility(View.VISIBLE);
        rl_details_layout.setVisibility(View.GONE);
        rl_otp.setVisibility(View.GONE);


        bt_verify_number.setVisibility(View.VISIBLE);
        bt_submit_details.setVisibility(View.GONE);
        bt_submit_otp.setVisibility(View.GONE);


        bt_verify_number.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                checkBlankFeilds();

            }
        });

        et_login_otp.addTextChangedListener(new GenericTextWatcher(et_login_otp));
        et_login_otp2.addTextChangedListener(new GenericTextWatcher(et_login_otp2));
        et_login_otp3.addTextChangedListener(new GenericTextWatcher(et_login_otp3));
        et_login_otp4.addTextChangedListener(new GenericTextWatcher(et_login_otp4));


    }



    private void checkBlankFeilds() {

        if (et_login_phone.getText().toString().isEmpty())
        {
            Toast.makeText(this, "Enter valid phone number", Toast.LENGTH_SHORT).show();
        }
        else
        {

            FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(Login.this, new OnSuccessListener<InstanceIdResult>() {
                @Override
                public void onSuccess(InstanceIdResult instanceIdResult) {

                    verifyNumber(instanceIdResult.getToken());
                }
            });
        }


    }


    private void verifyNumber(final String sToken) {

        CustomLayout.setUIToWait(Login.this,true);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, UrlPoint.firstTimeRegister, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                CustomLayout.setUIToWait(Login.this,false);

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    if (jsonObject.optString("flag").equalsIgnoreCase("true"))
                    {

                        openOtpLayout();

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                CustomLayout.setUIToWait(Login.this,false);

                Toast.makeText(Login.this, "Something went wrong", Toast.LENGTH_SHORT).show();

            }
        })

        {
            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<String, String>();
                params.put("phone",et_login_phone.getText().toString());
                params.put("fcm",sToken);


                return params;
            }

        };

        Singleton.getInstance(Login.this).addTorequestqu(stringRequest);

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        );




    }


    private void openOtpLayout() {

        rl_phone_layout.setVisibility(View.GONE);
        rl_details_layout.setVisibility(View.GONE);

        bt_verify_number.setVisibility(View.GONE);
        bt_submit_details.setVisibility(View.GONE);

        rl_otp.setVisibility(View.VISIBLE);
        bt_submit_otp.setVisibility(View.VISIBLE);


        startSmsUserConsent();


        bt_submit_otp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                checkOtpBlankfeilds();


            }
        });

        tv_resend_otp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                checkBlankFeilds();
                //Toast.makeText(Login.this, "Resend OTP", Toast.LENGTH_SHORT).show();

            }
        });

    }



    private void checkOtpBlankfeilds() {

        if (et_login_otp.getText().toString().isEmpty())
        {
            Toast.makeText(this, "OTP is empty", Toast.LENGTH_SHORT).show();
        }

        else
        {
            checkOtp();
        }



    }

    private void checkOtp() {

        mix_otp = et_login_otp.getText().toString() + et_login_otp2.getText().toString() +
                et_login_otp3.getText().toString()+et_login_otp4.getText().toString();

        if (mix_otp.length() == 4)
        {
            verifyOtp();
        }

        else
            {
                Toast.makeText(this, "Enter Valid OTP", Toast.LENGTH_SHORT).show();
            }


    }

    private void verifyOtp() {

        CustomLayout.setUIToWait(Login.this,true);

        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(Login.this);


        StringRequest stringRequest = new StringRequest(Request.Method.POST, UrlPoint.verifyUserOtp, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                CustomLayout.setUIToWait(Login.this,false);

                //Toast.makeText(Login.this, response, Toast.LENGTH_SHORT).show();

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    if (jsonObject.optString("flag").equalsIgnoreCase("true"))
                    {

                      /*  editor.putString("f_name",jsonObject.getString("first_name"));
                        editor.putString("l_name", jsonObject.getString("last_name"));*/
                        if (jsonObject.optString("is_registered_flag").equalsIgnoreCase("false"))
                        {
                            sharedPreferences = getSharedPreferences("MyData",Context.MODE_PRIVATE); // creating xml file to store data
                            editor = sharedPreferences.edit();// edit to store data into xml file
                            editor.putString("userId", jsonObject.getString("user_id"));// data that is being put in xml file
                            editor.apply();

                            openInputNameLayout();

                        }

                        else
                            {

                                sharedPreferences = getSharedPreferences("MyData",Context.MODE_PRIVATE); // creating xml file to store data
                                editor = sharedPreferences.edit();
                                editor.putString("f_name",jsonObject.getString("first_name"));
                                editor.putString("l_name", jsonObject.getString("last_name"));
                                editor.putString("userId", jsonObject.getString("user_id"));
                                editor.apply();// edit to store data into xml file

                                if (sharedPreferences.getString("store_name","NA").equalsIgnoreCase("NA"))
                                {
                                    startActivity(new Intent(Login.this, VerifyPincode.class));
                                    finish();
                                }
                                else
                                {
                                    startActivity(new Intent(Login.this, HomeScreen.class));
                                    finish();
                                }

                            }

                    }

                    else
                    {
                        Toast.makeText(Login.this, "Inavlid OTP", Toast.LENGTH_SHORT).show();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d("Exception", "OTP Exception is" + e.getMessage());
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                CustomLayout.setUIToWait(Login.this,false);

                Toast.makeText(Login.this, "Something went wrong", Toast.LENGTH_SHORT).show();

            }
        })


        {
            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<String, String>();
                params.put("phone",et_login_phone.getText().toString());
                params.put("otp",mix_otp);


                return params;


            }

        };

        Singleton.getInstance(Login.this).addTorequestqu(stringRequest);

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        );



    }

    private void openInputNameLayout() {



        rl_phone_layout.setVisibility(View.GONE);
        rl_details_layout.setVisibility(View.VISIBLE);
        rl_otp.setVisibility(View.GONE);


        bt_verify_number.setVisibility(View.GONE);
        bt_submit_details.setVisibility(View.VISIBLE);
        bt_submit_otp.setVisibility(View.GONE);


        bt_submit_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (et_login_fname.getText().toString().isEmpty())
                {
                    Toast.makeText(Login.this, "First name is empty", Toast.LENGTH_SHORT).show();
                }

                else
                    {
                        if (et_login_lname.getText().toString().isEmpty())
                        {
                            Toast.makeText(Login.this, "Last name is empty", Toast.LENGTH_SHORT).show();
                        } else
                            {
                                submitDetails();
                            }
                    }

            }
        });




    }



    private void submitDetails() {

        CustomLayout.setUIToWait(Login.this,true);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, UrlPoint.updateProfile, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                CustomLayout.setUIToWait(Login.this,false);

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    if (jsonObject.optString("flag").equalsIgnoreCase("true"))
                    {

                        JSONObject dataObject = jsonObject.getJSONObject("data");
                        String f_name = dataObject.getString("first_name");
                        String l_name = dataObject.getString("last_name");

                        sharedPreferences = getSharedPreferences("MyData",Context.MODE_PRIVATE); // creating xml file to store data
                        editor1 = sharedPreferences.edit();// edit to store data into xml file

                        editor1.putString("f_name", f_name);
                        editor1.putString("l_name", l_name);// data that is being put in xml file
                        editor1.apply();


                        startActivity(new Intent(Login.this, VerifyPincode.class));
                        finish();
                    }

                    else
                        {
                            Toast.makeText(Login.this, "Unable to update profile", Toast.LENGTH_SHORT).show();
                        }


                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                CustomLayout.setUIToWait(Login.this,false);

                Toast.makeText(Login.this, "Something went wrong", Toast.LENGTH_SHORT).show();

            }
        })


        {
            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<String, String>();
                params.put("first_name",et_login_fname.getText().toString());
                params.put("last_name",et_login_lname.getText().toString());
                params.put("user_id",sharedPreferences.getString("userId", "NA"));


                return params;


            }

        };

        Singleton.getInstance(Login.this).addTorequestqu(stringRequest);

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        );

    }


    private void initialization() {

        et_login_phone = findViewById(R.id.et_login_phone);
        et_login_otp = findViewById(R.id.et_login_otp);
        et_login_fname = findViewById(R.id.et_login_fname);
        et_login_lname = findViewById(R.id.et_login_lname);


        bt_submit_details = findViewById(R.id.bt_submit_details);
        bt_submit_otp = findViewById(R.id.bt_submit_otp);
        bt_verify_number = findViewById(R.id.bt_verify_number);


        rl_phone_layout = findViewById(R.id.rl_phone_layout);
        rl_details_layout = findViewById(R.id.rl_details_layout);
        rl_otp = findViewById(R.id.rl_otp);

        et_login_otp2 = findViewById(R.id.et_login_otp2);
        et_login_otp3 = findViewById(R.id.et_login_otp3);
        et_login_otp4 = findViewById(R.id.et_login_otp4);

        tv_resend_otp = findViewById(R.id.tv_resend_otp);


    }


    private void startSmsUserConsent() {

        SmsRetrieverClient client = SmsRetriever.getClient(this);
        //We can add sender phone number or leave it blank
        // I'm adding null here
        client.startSmsUserConsent(null).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
               // Toast.makeText(getApplicationContext(), "On Success", Toast.LENGTH_LONG).show();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
               // Toast.makeText(getApplicationContext(), "On OnFailure", Toast.LENGTH_LONG).show();
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQ_USER_CONSENT) {
            if ((resultCode == RESULT_OK) && (data != null)) {
                //That gives all message to us.
                // We need to get the code from inside with regex
                String message = data.getStringExtra(SmsRetriever.EXTRA_SMS_MESSAGE);
                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                et_login_otp.setText(
                        String.format("%s - %s", getString(R.string.received_message), message));
                getOtpFromMessage(message);
            }
        }
    }


    private void getOtpFromMessage(String message) {

        Pattern pattern = Pattern.compile("(|^)\\d{4}");
        Matcher matcher = pattern.matcher(message);
        if (matcher.find()) {

            String number = matcher.group(0).toString();
            et_login_otp.setText(number.charAt(0)+"");
            et_login_otp2.setText(number.charAt(1)+"");
            et_login_otp3.setText(number.charAt(2)+"");
            et_login_otp4.setText(number.charAt(3)+"");

           mix_otp = number;

        }
    }

    private void registerBroadcastReceiver() {
        smsBroadcastReceiver = new SmsBroadcastReceiver();
        smsBroadcastReceiver.smsBroadcastReceiverListener =
                new SmsBroadcastReceiver.SmsBroadcastReceiverListener() {
                    @Override
                    public void onSuccess(Intent intent) {
                        startActivityForResult(intent, REQ_USER_CONSENT);
                    }
                    @Override
                    public void onFailure() {
                    }
                };
        IntentFilter intentFilter = new IntentFilter(SmsRetriever.SMS_RETRIEVED_ACTION);
        registerReceiver(smsBroadcastReceiver, intentFilter);
    }


    @Override
    protected void onStart() {
        super.onStart();
        registerBroadcastReceiver();
    }
    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(smsBroadcastReceiver);
    }



    public class GenericTextWatcher implements TextWatcher
    {
        private View view;
        private GenericTextWatcher(View view)
        {
            this.view = view;
        }

        @Override
        public void afterTextChanged(Editable editable) {
            // TODO Auto-generated method stub
            String text = editable.toString();
            switch(view.getId())
            {

                case R.id.et_login_otp:
                    if(text.length()==1)
                        et_login_otp2.requestFocus();
                    break;
                case R.id.et_login_otp2:
                    if(text.length()==1)
                        et_login_otp3.requestFocus();
                    else if(text.length()==0)
                        et_login_otp.requestFocus();
                    break;
                case R.id.et_login_otp3:
                    if(text.length()==1)
                        et_login_otp4.requestFocus();
                    else if(text.length()==0)
                        et_login_otp2.requestFocus();
                    break;
                case R.id.et_login_otp4:
                    if(text.length()==0)
                        et_login_otp3.requestFocus();
                    break;
            }
        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            // TODO Auto-generated method stub
        }

        @Override
        public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            // TODO Auto-generated method stub
        }
    }

}








