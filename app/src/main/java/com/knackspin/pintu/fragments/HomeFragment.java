package com.knackspin.pintu.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.iid.FirebaseInstanceId;
import com.knackspin.pintu.Adapter.RandomAdpater;
import com.knackspin.pintu.Adapter.StagAdapter;
import com.knackspin.pintu.CustomLayout;
import com.knackspin.pintu.Database.DatabaseHelper;
import com.knackspin.pintu.HomeScreen;
import com.knackspin.pintu.Model.Cart_model;
import com.knackspin.pintu.Model.RandomCategory;
import com.knackspin.pintu.Model.Shop_category;
import com.knackspin.pintu.R;
import com.knackspin.pintu.VerifyPincode;
import com.knackspin.pintu.utils.ConnectionCheck;
import com.knackspin.pintu.utils.UrlPoint;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import ss.com.bannerslider.banners.Banner;
import ss.com.bannerslider.views.BannerSlider;


public class HomeFragment extends Fragment {

    RelativeLayout connection_layout;
    LinearLayout no_connection_layout;

    BannerSlider bannerSlider;
    RandomAdpater randomAdpater;
    StagAdapter stagAdapter;

    ArrayList<Shop_category> listofCategory;
    ArrayList<RandomCategory> listOfRandomData;
    public static List<Banner> listOfBanner;
    public ArrayList<Cart_model> listOfCartItems;

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    HomeScreen homeScreen;
    RecyclerView recyclerView,stag_recycle_view;
    LinearLayoutManager layoutManager;
    StaggeredGridLayoutManager staggeredGridLayoutManager;

    Button reconnect_button;
    ImageView home_cart,home_notification,home_search;

    DatabaseHelper databaseHelper;
    String u_id,listsize;
    TextView cartNotificationCount,change_pincode,store_name;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.fragment_home, container, false);

        connection_layout = view.findViewById(R.id.connection_layout);
        no_connection_layout = view.findViewById(R.id.no_connection_layout);

        FirebaseInstanceId.getInstance().getToken();
        Log.d("Token", FirebaseInstanceId.getInstance().getToken());

        listofCategory = new ArrayList<>();
        listOfBanner = new ArrayList<>();
        listOfRandomData = new ArrayList<>();
        listOfCartItems = new ArrayList<>();

        recyclerView = view.findViewById(R.id.random_sub_cat);
        stag_recycle_view = view.findViewById(R.id.stag_recycle_view);



        stagAdapter = new StagAdapter(listofCategory,getContext());
        randomAdpater = new RandomAdpater(listOfRandomData,getContext());

        recyclerView.setHasFixedSize(true);
        stag_recycle_view.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(getContext(),LinearLayoutManager.HORIZONTAL,true);
        staggeredGridLayoutManager = new StaggeredGridLayoutManager(3,LinearLayoutManager.VERTICAL);

        recyclerView.setLayoutManager(layoutManager);
        stag_recycle_view.setLayoutManager(staggeredGridLayoutManager);

        cartNotificationCount = view.findViewById(R.id.tvCartNotifBadge);
        reconnect_button = view.findViewById(R.id.reconnect_button);
        home_cart = view.findViewById(R.id.home_cart);
        home_notification =view.findViewById(R.id.home_notification);
        home_search = view.findViewById(R.id.home_search);
        bannerSlider = view.findViewById(R.id.banner_slider);
        change_pincode = view.findViewById(R.id.change_pincode);
        store_name = view.findViewById(R.id.store_name);

        databaseHelper = new DatabaseHelper(getContext());

        sharedPreferences = getActivity().getSharedPreferences("MyData", Context.MODE_PRIVATE);
        u_id = sharedPreferences.getString("userId", "NA");


        if(getArguments()!=null) {
            getArguments().getString("store_id");
            editor = sharedPreferences.edit();// edit to store data into xml file
            editor.putString("store_id", getArguments().getString("store_id"));// data that is being put in xml file
            editor.apply();

            getArguments().getString("store_name");
            store_name.setText(getArguments().getString("store_name"));
            editor = sharedPreferences.edit();// edit to store data into xml file
            editor.putString("store_name", getArguments().getString("store_name"));// data that is being put in xml file
            editor.apply();

            getArguments().getString("store_delivery_charge");
            editor = sharedPreferences.edit();// edit to store data into xml file
            editor.putString("store_delivery_charge", getArguments().getString("store_delivery_charge"));// data that is being put in xml file
            editor.apply();


            getArguments().getString("store_minimum_charge");
            editor = sharedPreferences.edit();// edit to store data into xml file
            editor.putString("store_minimum_charge", getArguments().getString("store_minimum_charge"));// data that is being put in xml file
            editor.apply();


        }


        listOfCartItems = databaseHelper.getAllCartItems();
        listsize = String.valueOf(listOfCartItems.size());
        cartNotificationCount.setText(" "+ listsize);

        change_pincode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getContext(), VerifyPincode.class);
                startActivity(intent);
                getActivity().finish();

            }
        });



        return view;
    }


    @Override
    public void onResume() {
        super.onResume();

        connectionCheck();
    }

    private void connectionCheck() {

        if(new ConnectionCheck(getContext()).isNetworkAvailable())
        {

            connection_layout.setVisibility(View.VISIBLE);
            no_connection_layout.setVisibility(View.GONE);

            getHomePageData();

        } else
        {
            connection_layout.setVisibility(View.GONE);
            no_connection_layout.setVisibility(View.VISIBLE);

        }

    }

    private void getHomePageData() {
        CustomLayout.setUIToWait(getContext(),true);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, UrlPoint.homePageApi, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                CustomLayout.setUIToWait(getContext(),false);


                if (listofCategory!=null)
                {
                    listofCategory.clear();
                }

                if (listOfRandomData!=null)
                {
                    listOfRandomData.clear();
                }

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    JSONObject dataObject = jsonObject.getJSONObject("data");

                    JSONArray categoryArray = dataObject.getJSONArray("categories");
                    for (int cat =0; cat<categoryArray.length();cat++)
                    {
                        JSONObject catObject = categoryArray.getJSONObject(cat);
                        Shop_category shop_category = new Shop_category(catObject.getString("id"), catObject.getString("name"), catObject.getString("photo"));
                        listofCategory.add(shop_category);
                    }
                    stag_recycle_view.setAdapter(stagAdapter);
                    stagAdapter.notifyDataSetChanged();


                    JSONArray randonCatArray = dataObject.getJSONArray("random_category");
                    for (int ran =0 ;ran<randonCatArray.length();ran++)
                    {
                        JSONObject ranObject = randonCatArray.getJSONObject(ran);
                        RandomCategory category = new RandomCategory(ranObject.optString("sub_category_id"), ranObject.optString("categoryId"), ranObject.optString("photo"), ranObject.optString("name"));
                        listOfRandomData.add(category);

                    }
                    recyclerView.setAdapter(randomAdpater);
                    randomAdpater.notifyDataSetChanged();



                  /*  JSONArray bannerArray = dataObject.getJSONArray("banner");
                    for (int ban =0;ban<bannerArray.length();ban++)
                    {
                        JSONObject banObject = bannerArray.getJSONObject(ban);
                        listOfBanner.add(new RemoteBanner( UrlPoint.banner_path+ banObject.optString("banner_image")));
                    }

                    bannerSlider.setBanners(HomeScreen.listOfBanner);*/


                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d("Home Exception", e.getMessage());
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                CustomLayout.setUIToWait(getContext(),false);
                Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });

        RequestQueue requestQueue= Volley.newRequestQueue(getContext());
        requestQueue.add(stringRequest);


    }
}
