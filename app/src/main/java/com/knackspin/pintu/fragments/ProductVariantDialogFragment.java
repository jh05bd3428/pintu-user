package com.knackspin.pintu.fragments;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.knackspin.pintu.Adapter.DialogAdapter;
import com.knackspin.pintu.Model.DialogModel;
import com.knackspin.pintu.Model.Product_model;
import com.knackspin.pintu.R;
import com.knackspin.pintu.interfaces.DialogFragmentSetData;
import com.knackspin.pintu.interfaces.ProductModelSetData;

import java.util.ArrayList;
import java.util.List;


@SuppressLint("ValidFragment")
public class ProductVariantDialogFragment extends DialogFragment {

    TextView tv1,tv2,tv3;
    private List<DialogModel> Dummy;
    private List<Product_model> listOfVariants;
    private RecyclerView recyclerView;
    private DialogAdapter dialogAdapter;
    private ArrayList<String> arrayList;
    private ProductModelSetData productModelSetData;
    String pro_id;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if(bundle!=null){
            this.pro_id = bundle.getString("PROD_ID");
            this.Dummy = (List<DialogModel>) bundle.getSerializable("LIST");
            this.productModelSetData = (ProductModelSetData) bundle.getSerializable("MODEL");
        }

    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        return super.onCreateDialog(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dialog_recyler, container, false);
        setStyle(ProductVariantDialogFragment.STYLE_NORMAL, R.style.MyAlertDialogTheme);
        arrayList = new ArrayList<>();
        listOfVariants = new ArrayList<>();
        tv1 = view.findViewById(R.id.variant_name);
        recyclerView = view.findViewById(R.id.rvVariant);
        dialogAdapter = new DialogAdapter(pro_id,Dummy, getActivity(), new DialogFragmentSetData() {
            @Override
            public void setData(int pos, int productPos) {
                ProductVariantDialogFragment.this.dismiss();
                productModelSetData.setProductModel(pos, productPos );
            }
        });

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(),DividerItemDecoration.HORIZONTAL));
        recyclerView.setAdapter(dialogAdapter);
        return view;
    }
}

