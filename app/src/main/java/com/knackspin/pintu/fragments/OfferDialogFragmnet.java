package com.knackspin.pintu.fragments;

import android.app.Dialog;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.knackspin.pintu.Adapter.OfferDialogAdapter;
import com.knackspin.pintu.Model.OfferDialogModel;
import com.knackspin.pintu.Model.OfferNotifModel;
import com.knackspin.pintu.R;
import com.knackspin.pintu.interfaces.OfferNotifFragmentData;
import com.knackspin.pintu.interfaces.OfferNotifModelSetData;

import java.util.ArrayList;
import java.util.List;

public class OfferDialogFragmnet extends DialogFragment {

    private RecyclerView recyclerView;
    String prod_id;
    private List<OfferDialogModel> dummyDialog;
    private OfferNotifModelSetData offerNotifModelSetData;
    private OfferDialogAdapter offerDialogAdapter;
    private ArrayList<String> arrayList;
    private ArrayList<OfferNotifModel> listOfOfferVariant;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle!=null)
        {
            this.prod_id = bundle.getString("prod_id");
            this.dummyDialog = (List<OfferDialogModel>) bundle.getSerializable("list");
            this.offerNotifModelSetData =  (OfferNotifModelSetData) bundle.getSerializable("model");

        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        return super.onCreateDialog(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.offer_fragmnet, container, false);
        setStyle(ProductVariantDialogFragment.STYLE_NORMAL, R.style.MyAlertDialogTheme);
        recyclerView = view.findViewById(R.id.offer_dialog_recyler);
        arrayList = new ArrayList<>();
        listOfOfferVariant = new ArrayList<>();


        offerDialogAdapter = new OfferDialogAdapter(prod_id, dummyDialog, getActivity(), new OfferNotifFragmentData() {
            @Override
            public void setData(int pos, int productPos) {

                OfferDialogFragmnet.this.dismiss();
                offerNotifModelSetData.setOfferModel(pos, productPos);

            }
        });



        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(),DividerItemDecoration.HORIZONTAL));
        recyclerView.setAdapter(offerDialogAdapter);
        return view;


    }

}
