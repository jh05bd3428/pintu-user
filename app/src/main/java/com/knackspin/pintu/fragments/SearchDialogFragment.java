package com.knackspin.pintu.fragments;

import android.app.Dialog;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.knackspin.pintu.Adapter.SearchDialogAdapter;
import com.knackspin.pintu.Model.SearchDialogModel;
import com.knackspin.pintu.Model.SearchModel;
import com.knackspin.pintu.R;
import com.knackspin.pintu.interfaces.SearchFragmnetSetData;
import com.knackspin.pintu.interfaces.SearchModelSetData;

import java.util.ArrayList;
import java.util.List;

public class SearchDialogFragment extends DialogFragment {

    private RecyclerView recyclerView;
    String prod_id;
    private List<SearchDialogModel> dummyDialog;
    private SearchModelSetData searchModelSetData;
    private SearchDialogAdapter searchDialogAdapter;
    private ArrayList<String> arrayList;
    private ArrayList<SearchModel> listOfDialogVariant;



    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle!=null)
        {
            this.prod_id = bundle.getString("prod_id");
            this.dummyDialog = (List<SearchDialogModel>) bundle.getSerializable("list");
            this.searchModelSetData =  (SearchModelSetData) bundle.getSerializable("model");

        }
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        return super.onCreateDialog(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.serach_dialog_recyler, container, false);
        setStyle(ProductVariantDialogFragment.STYLE_NORMAL, R.style.MyAlertDialogTheme);
        recyclerView = view.findViewById(R.id.dialog_recyler);
        arrayList = new ArrayList<>();
        listOfDialogVariant = new ArrayList<>();


        searchDialogAdapter = new SearchDialogAdapter(prod_id, dummyDialog, getActivity(), new SearchFragmnetSetData() {
            @Override
            public void setData(int pos, int productPos) {

                SearchDialogFragment.this.dismiss();
                searchModelSetData.setSearchModel(pos, productPos);
            }
        });

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(),DividerItemDecoration.HORIZONTAL));
        recyclerView.setAdapter(searchDialogAdapter);
        return view;


    }
}
