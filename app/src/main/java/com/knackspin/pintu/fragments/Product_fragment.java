package com.knackspin.pintu.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.knackspin.pintu.Adapter.ProductAdapter;
import com.knackspin.pintu.Adapter.DialogAdapter;
import com.knackspin.pintu.Database.DatabaseHelper;
import com.knackspin.pintu.Model.DialogModel;
import com.knackspin.pintu.Model.Product_model;
import com.knackspin.pintu.R;
import com.knackspin.pintu.utils.UrlPoint;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@RequiresApi(api = Build.VERSION_CODES.M)
public class Product_fragment extends Fragment implements RecyclerView.OnScrollChangeListener {

    TextView displayID;
    DatabaseHelper databaseHelper;
    private RecyclerView recyclerView;
    private RelativeLayout relativeLayout;
    private List<Product_model> listofProducts;
    private List<DialogModel> listOfSpinners;
    private Serializable listOfDialog;
    private DialogModel sm;

    private ProductAdapter productAdapter;
    private DialogAdapter dialogAdapter;
    private ProgressBar progressBar;

    private int requestCount = 1;
    private RequestQueue requestQueue;
    RecyclerView rvVariant;
    String variant_id,quantity;

    int positionIndex;
    String variant_weight, variant_mrp, variant_offered_price,productId,variantId;

    private RecyclerView.LayoutManager layoutManager;

    String subID;
    String providerID ;

    ShimmerFrameLayout shimmerFrameLayout;
    SharedPreferences sharedPreferences;

    public static final String DEFAULT = "NA";


    public static Product_fragment newInstance(String sId) {
        Bundle args = new Bundle();
        args.putString("sid", sId);
        Product_fragment fragment = new Product_fragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragmnet_product_fragmnet, container, false);


        if (getArguments() != null) {
            subID = getArguments().getString("sid");
            variant_weight = getArguments().getString("variant_weight");

            Log.d("variant_weight", "Sub cat id is"+subID);
        }

        sharedPreferences = Objects.requireNonNull(this.getActivity()).getSharedPreferences("MyData", Context.MODE_PRIVATE);

        listofProducts = new ArrayList<>();
        listOfDialog = new ArrayList<>();
        relativeLayout = view.findViewById(R.id.rel_layout);
        shimmerFrameLayout = view.findViewById(R.id.shimor_effect);
        relativeLayout.setVisibility(View.GONE);
        recyclerView = view.findViewById(R.id.product_view);
        recyclerView.setHasFixedSize(true);
        productAdapter = new ProductAdapter(listofProducts, Product_fragment.this, getContext());

        //recyclerView.setAdapter(productAdapter);
        layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(productAdapter);

        requestQueue = Volley.newRequestQueue((getActivity()));
        recyclerView.setOnScrollChangeListener(this);

        progressBar = view.findViewById(R.id.progressBar);

        getData();
        return view;

    }

    @Override
    public void onResume() {
        super.onResume();
        //recyclerView.setLayoutFrozen(false);
        getData();
    }

    private void getData() {

        requestQueue.add(getDataFromServer(requestCount));
        requestCount++;

    }

    private StringRequest getDataFromServer(final int requestCount)
    {
        shimmerFrameLayout.startShimmerAnimation();
       // progressBar.setVisibility(View.VISIBLE);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, UrlPoint.getProduct + requestCount, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                shimmerFrameLayout.stopShimmerAnimation();
                shimmerFrameLayout.setVisibility(View.GONE);

                System.out.println(response);

                //CustomLayout.setUIToWait(getContext(), false);
                // Log.d("Tag1", response);
                JSONObject jsonObject = null;
                try {

                    jsonObject = new JSONObject(response);

                    if (jsonObject.optString("status").equalsIgnoreCase("found")) {
                        relativeLayout.setVisibility(View.GONE);
                        Log.d("Tag1.1", "Inside response block");
                        try {

                            JSONArray jsonArray = jsonObject.getJSONArray("list");

                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject ob = jsonArray.getJSONObject(i);

                                JSONArray productArray = ob.getJSONArray("product");

                                for (int j = 0; j < productArray.length(); j++) {

                                    JSONObject ob1 = productArray.getJSONObject(j);
                                    productId = ob1.getString("id");

                                    ArrayList<DialogModel> listOfSpinners = new ArrayList<DialogModel>();

                                    JSONArray subProduct = ob1.getJSONArray("product_variant");

                                    for (int k = 0; k < subProduct.length(); k++) {
                                        JSONObject ob2 = subProduct.getJSONObject(k);
                                        variantId = ob2.getString("variantproduct_id");
                                        listOfSpinners.add(new DialogModel(ob2.optString("id"),ob2.optString("variantproduct_id"),ob2.optString("variant_name"), ob2.optString("mrp_price"), ob2.optString("variant_price")));
                                    }

                                    Product_model pm = new Product_model(ob1.optString("id"),ob1.optString("brand_name"), ob1.optString("name"), ob1.optString("photo_1"),listOfSpinners);
                                    listofProducts.add(pm);



                                }
                            }

                            productAdapter.notifyDataSetChanged();
                            getProductDetails();


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    } else {
                        Log.d("Tag2", "No product Found");
                        relativeLayout.setVisibility(View.VISIBLE);
                        shimmerFrameLayout.setVisibility(View.GONE);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // CustomLayout.setUIToWait(getContext(), false);
                shimmerFrameLayout.stopShimmerAnimation();
                relativeLayout.setVisibility(View.GONE);

                if (error instanceof ServerError) {

                    Toast.makeText(getContext(), "Server Down! Please try again", Toast.LENGTH_LONG).show();

                } else if (error instanceof NetworkError) {
                    Toast.makeText(getContext(), "Check your Internet Connection", Toast.LENGTH_LONG).show();
                } else {
//                    Toast.makeText(getActivity(), "Something Broken", Toast.LENGTH_LONG).show();
                }
            }
        })
        {
            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<String, String>();
                params.put("sub_category_id", subID);
                params.put("provider_id", sharedPreferences.getString("store_id", DEFAULT));
                params.put("page", String.valueOf(requestCount));
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        );

        Log.d("list", new Gson().toJson(listofProducts));
        return  stringRequest;

    }

    private void getProductDetails() {

    for (int q=0; q<listofProducts.size();q++)
    {

        Product_model pm = listofProducts.get(q);

    }



    }


    private boolean isLastItemDisplaying(RecyclerView recyclerView) {
        if ((recyclerView.getAdapter()).getItemCount() != 0) {
            int lastVisibleItemPosition = ((LinearLayoutManager) Objects.requireNonNull(recyclerView.getLayoutManager())).findLastCompletelyVisibleItemPosition();
            if (lastVisibleItemPosition != RecyclerView.NO_POSITION && lastVisibleItemPosition == recyclerView.getAdapter().getItemCount() - 1)
                return true;
        }
        return false;
    }


    @Override
    public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {

        if (isLastItemDisplaying(recyclerView)) {
            //Calling the method getdata again
            getData();

        }
    }

}

