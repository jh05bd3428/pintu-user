package com.knackspin.pintu.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.knackspin.pintu.Model.Cart_model;
import com.knackspin.pintu.Model.Cart_model_new;

import java.util.ArrayList;

public class DatabaseHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "cart_item.db";

    public static final String TABLE_NAME = "cart_table";
    public static final String COLOUNM_ID = "ID";

    public static final String PRODUCT_ID = "p_product_id";
    public static final String BRAND_NAME = "p_brand_name";
    public static final String PRODUCT_NAME = "p_product_name";
    public static final String PRODUCT_VARIANT = "p_product_variant";
    public static final String PRODUCT_ACTUAL_PRICE = "p_actual_price";
    public static final String PRODUCT_SELLING_PRICE = "p_selling_price";
    public static final String PRODUCT_QUANTITY = "p_product_quantity";
    public static final String PRODUCT_IMAGE = "p_product_image";
    public static final String VARIANT_ID = "p_variant_id";


    SQLiteDatabase db;


    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, 2);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL("create table " + TABLE_NAME + "(ID INTEGER PRIMARY KEY AUTOINCREMENT,p_product_id INTEGER, p_brand_name TEXT, p_product_name TEXT, p_product_variant DOUBLE, p_actual_price INTEGER, p_selling_price DOUBLE, p_product_quantity INTEGER, p_product_image TEXT, p_variant_id INTEGER)");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME); // query to drop table
        onCreate(db);
    }


    public boolean insert_data(String PRODUCT_IMAGE, String PRODUCT_ID, String VARIANT_ID, String BRAND_NAME, String PRODUCT_NAME, String PRODUCT_VARIANT, String PRODUCT_ACTUAL_PRICE, String PRODUCT_SELLING_PRICE, String PRODUCT_QUANTITY) {

        db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put("p_product_id", PRODUCT_ID);
        contentValues.put("p_brand_name", BRAND_NAME);
        contentValues.put("p_product_name", PRODUCT_NAME);
        contentValues.put("p_product_variant", PRODUCT_VARIANT);
        contentValues.put("p_actual_price", PRODUCT_ACTUAL_PRICE);
        contentValues.put("p_selling_price", PRODUCT_SELLING_PRICE);
        contentValues.put("p_product_quantity", PRODUCT_QUANTITY);
        contentValues.put("p_product_image", PRODUCT_IMAGE);
        contentValues.put("p_variant_id", VARIANT_ID);

        db.insert(TABLE_NAME, null, contentValues);
        return true;
    }


    public ArrayList<Cart_model_new> getAllIdItems() {

        ArrayList<Cart_model_new> array_list = new ArrayList<>();
        array_list.clear();

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from " + TABLE_NAME, null);
        res.moveToFirst();

        while (!res.isAfterLast()) {
            Cart_model_new cart_model_new = new Cart_model_new();
            cart_model_new.setProduct_id(res.getString(res.getColumnIndex(PRODUCT_ID)));
            cart_model_new.setProduct_quantity(res.getString(res.getColumnIndex(PRODUCT_QUANTITY)));
            cart_model_new.setVariant_id(res.getString(res.getColumnIndex(VARIANT_ID)));

            array_list.add(cart_model_new);
            res.moveToNext();
        }

        return array_list; // return the array list
    }


    public String cartCount() {

        Cursor res = db.rawQuery("select * from " + TABLE_NAME, null);

        return String.valueOf(res.getCount());
    }

    public ArrayList<Cart_model> getAllCartItems() {

        ArrayList<Cart_model> array_list = new ArrayList<>(); //creating array list to display data in recylerview
        array_list.clear();

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from " + TABLE_NAME, null);
        res.moveToFirst();

        while (!res.isAfterLast()) {
            Cart_model cart_model = new Cart_model();
            cart_model.setProduct_id(res.getString(res.getColumnIndex(PRODUCT_ID)));
            cart_model.setBrand_name(res.getString(res.getColumnIndex(BRAND_NAME)));
            cart_model.setProduct_name(res.getString(res.getColumnIndex(PRODUCT_NAME)));
            cart_model.setVariant_weight(res.getString(res.getColumnIndex(PRODUCT_VARIANT)));
            cart_model.setActual_price(res.getString(res.getColumnIndex(PRODUCT_ACTUAL_PRICE)));
            cart_model.setOffered_price(res.getString(res.getColumnIndex(PRODUCT_SELLING_PRICE)));
            cart_model.setQuantity(res.getString(res.getColumnIndex(PRODUCT_QUANTITY)));
            cart_model.setProduct_Url(res.getString(res.getColumnIndex(PRODUCT_IMAGE)));
            cart_model.setCart_variant_id(res.getString(res.getColumnIndex(VARIANT_ID)));
            array_list.add(cart_model);
            res.moveToNext();
        }

        return array_list; // return the array list
    }

    public boolean checkDetails(String p_product_id) {
        db = this.getReadableDatabase();
        String query = "select * from cart_table where p_product_id ='" + p_product_id + "'";
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.getCount() > 0) {
            return true;
            //Toast.makeText(, "Record found", Toast.LENGTH_SHORT).show();
        } else {
            return false;

        }
    }

    public String getProdQty(String p_product_id) {
        db = this.getReadableDatabase();
        String query = "select * from cart_table where p_product_id ='" + p_product_id + "'";
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.getCount() > 0) {
            String sQty = null;
            if (cursor.moveToFirst())
                sQty = cursor.getString(cursor.getColumnIndex(PRODUCT_QUANTITY));
            return sQty;
            //Toast.makeText(, "Record found", Toast.LENGTH_SHORT).show();
        } else {
            return "0";

        }
    }



    public String getProdVariant(String p_product_id) {
        db = this.getReadableDatabase();
        String query = "select * from cart_table where p_product_id ='" + p_product_id + "'";
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.getCount() > 0) {
            String sQty = null;
            if (cursor.moveToFirst())
                sQty = cursor.getString(cursor.getColumnIndex(VARIANT_ID));
            return sQty;
            //Toast.makeText(, "Record found", Toast.LENGTH_SHORT).show();
        } else {
            return "false";

        }
    }

    public boolean updateData(String PRODUCT_ID, String BRAND_NAME, String PRODUCT_NAME, String PRODUCT_VARIANT, String PRODUCT_ACTUAL_PRICE, String PRODUCT_SELLING_PRICE, String PRODUCT_QUANTITY) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put("p_product_quantity", PRODUCT_QUANTITY);

        db.update(TABLE_NAME, contentValues, "p_product_id= ? ", new String[]{PRODUCT_ID});
        return true;

    }


    public Integer delete_data(String prod_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(TABLE_NAME, "p_product_id = ?", new String[]{String.valueOf((prod_id))});
    }

    public void Delete(Context context) {
        context.deleteDatabase(DATABASE_NAME);

    }
}
