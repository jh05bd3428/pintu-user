package com.knackspin.pintu;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.knackspin.pintu.Singleton.Singleton;
import com.knackspin.pintu.utils.UrlPoint;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class RewardsActivity extends AppCompatActivity {

    TextView reward_points;
    Button claim_reward;
    public static final String DEFAULT = "NA";
    String u_id;
    String provider_id ;
    ImageView reward_back_image;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rewards);


        reward_points = findViewById(R.id.reward_points);
        claim_reward = findViewById(R.id.claim_reward_button);
        reward_back_image = findViewById(R.id.reward_back_image);
        SharedPreferences sharedPreferences = getSharedPreferences("MyData", Context.MODE_PRIVATE);
        u_id = sharedPreferences.getString("userId", DEFAULT);
        provider_id = sharedPreferences.getString("providerID",DEFAULT);
        getReward();


        claim_reward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RewardsActivity.this, CheckReward.class);
                startActivity(intent);
                //finish();
            }
        });

        reward_back_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });
    }

    private void getReward() {

        CustomLayout.setUIToWait(RewardsActivity.this,true);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, UrlPoint.getRewardUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                CustomLayout.setUIToWait(RewardsActivity.this, false);

                JSONObject jsonObject = null;

                try {
                    jsonObject = new JSONObject(response);

                    if(jsonObject.optString("status").equalsIgnoreCase("found"))
                    {
                        String r_points = jsonObject.getString("total_reward_point");
                        reward_points.setText(r_points);
                    }

                    else
                        {
                            Toast.makeText(RewardsActivity.this, "Nothing to show", Toast.LENGTH_SHORT).show();
                        }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                CustomLayout.setUIToWait(RewardsActivity.this, false);


                // Toast.makeText(RewardsActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                if(error instanceof ServerError)
                {

                    Toast.makeText(RewardsActivity.this, "Server Down! Please try again", Toast.LENGTH_SHORT).show();
                }

                else if (error instanceof NetworkError)
                {
                    Toast.makeText(RewardsActivity.this, "Check your Internet Connection", Toast.LENGTH_SHORT).show();
                }

                else
                {
                    Toast.makeText(RewardsActivity.this, "Something Broken", Toast.LENGTH_SHORT).show();
                }

            }
        })

        {
            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id",u_id);
                params.put("provider_id",provider_id);
                return params;
            }

        };
        Singleton.getInstance(RewardsActivity.this).addTorequestqu(stringRequest);

    }


}
