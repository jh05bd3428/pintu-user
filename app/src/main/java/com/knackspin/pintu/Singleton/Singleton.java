package com.knackspin.pintu.Singleton;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

public class Singleton
{
    private RequestQueue requestQueue;
    private static Singleton mInstance;
    private Context mCtx;

    private Singleton (Context context){

        mCtx = context;
        requestQueue = getRequestQueue ();
    }

    public static synchronized Singleton getInstance (Context context)
    {
        if (mInstance == null)

        {
            mInstance = new Singleton(context);
        }
        return mInstance;
    }

    public RequestQueue getRequestQueue()
    {

        if(requestQueue == null)
        {
            requestQueue= Volley.newRequestQueue(mCtx.getApplicationContext());
        }
        return requestQueue;
    }

    public void addTorequestqu (Request request)

    {
        requestQueue.add(request);
    }


}
