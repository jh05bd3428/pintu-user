package com.knackspin.pintu;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.knackspin.pintu.Database.DatabaseHelper;
import com.knackspin.pintu.Model.Cart_model_new;
import com.knackspin.pintu.Singleton.Singleton;
import com.knackspin.pintu.utils.UrlPoint;
import com.razorpay.Checkout;
import com.razorpay.PaymentResultListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class PaymentActivity extends AppCompatActivity implements PaymentResultListener {

    String sAmount, sAddrId,provoder_id,u_id,user_email,user_phone, item_amt;
    String TAG = "TAG111";
    SharedPreferences sharedPreference;
    ProgressDialog progressBar;
    private HashMap hashMap;
    String totalCartValue, addressId;
    int nStatusCode;

    private ArrayList<Cart_model_new> listOfIds;
    DatabaseHelper databaseHelper;

    public static final String DEFAULT = "NA";
    String[] prodIdArray;
    String[] quantityArray;
    String[] variantArray;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);

        if (getIntent().getExtras() != null) {
            sAmount = "" + (Double.parseDouble(getIntent().getExtras().getString("fAmount")) * 100);
            sAddrId = getIntent().getExtras().getString("addrId");
            item_amt = getIntent().getExtras().getString("item_total");
            //callRazorPay("qbsbcavchavGhth");
        }


        databaseHelper = new DatabaseHelper(this);
        listOfIds = new ArrayList<>();
        SharedPreferences sharedPreferences = getSharedPreferences("MyData", Context.MODE_PRIVATE);
         u_id = sharedPreferences.getString("userId", DEFAULT);
         provoder_id = sharedPreferences.getString("providerID",DEFAULT);
         user_email = sharedPreferences.getString("login_email",DEFAULT );
         user_phone = sharedPreferences.getString("login_phone", DEFAULT);


        startPayment();
    }

    public void startPayment() {

        /*
          You need to pass current activity in order to let Razorpay create RazorPayActivity
         */
        final Activity activity = this;

        final Checkout co = new Checkout();

        try {
            JSONObject options = new JSONObject();
            options.put("name", "Ryes");
            options.put("description", "Shopping total");
            //You can omit the image option to fetch the image from dashboard
            options.put("image", "https://s3.amazonaws.com/rzp-mobile/images/rzp.png");
            options.put("currency", "INR");
            options.put("amount", sAmount);

            JSONObject preFill = new JSONObject();
            preFill.put("email", user_email);
            preFill.put("contact", user_phone);

            options.put("prefill", preFill);

            co.open(activity, options);
        } catch (Exception e) {
            Toast.makeText(activity, "Error in payment: " + e.getMessage(), Toast.LENGTH_SHORT)
                    .show();
            e.printStackTrace();
        }
    }

    @Override
    public void onPaymentSuccess(String s) {
        try {
            //Toast.makeText(this, "Payment Successful: " + s, Toast.LENGTH_SHORT).show();

            placeOrder();

            //getCartValue();
            //callRazorPay(s);


        } catch (Exception e) {
            Log.e("TAGERROR", "Exception in onPaymentSuccess", e);
        }
    }

    private void placeOrder() {

        CustomLayout.setUIToWait(PaymentActivity.this, true);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, UrlPoint.placeOrderApi, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                CustomLayout.setUIToWait(PaymentActivity.this, false);
                JSONObject jsonObject = null;
                try {

                    jsonObject = new JSONObject(response);

                    if (jsonObject.optString("status").equalsIgnoreCase("success"))
                    {

                        databaseHelper.Delete(PaymentActivity.this);
                        //Toast.makeText(this, "Order placed successfully ", Toast.LENGTH_SHORT).show();
                        Toast.makeText(PaymentActivity.this, "Order placed successfully", Toast.LENGTH_SHORT).show();

                        Intent intent = new Intent(PaymentActivity.this, OrderPlaced.class);
                        startActivity(intent);
                        finish();

                    } else
                        {
                            Toast.makeText(PaymentActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                        }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d("Payment Exception", e.getMessage());
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                CustomLayout.setUIToWait(PaymentActivity.this, false);
                if(error instanceof ServerError)
                {

                    Toast.makeText(PaymentActivity.this, "Server Down", Toast.LENGTH_SHORT).show();
                }

                else if (error instanceof NetworkError)
                {
                    Toast.makeText(PaymentActivity.this, "Check your Internet Connection", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    Toast.makeText(PaymentActivity.this, error.toString(), Toast.LENGTH_SHORT).show();

                }
            }
        })

        {

            @Override
            protected Map<String, String> getParams() {


                Map<String, String> params = new HashMap<String, String>();

                listOfIds =  databaseHelper.getAllIdItems();

                prodIdArray = new String[listOfIds.size()];
                for (int i = 0; i < listOfIds.size(); i++) {
                    // prodIdArray[i] = listOfIds.get(i).getProduct_id();
                    params.put("product_id["+i+"]", listOfIds.get(i).getProduct_id());
                }

                quantityArray = new String[listOfIds.size()];
                for (int j=0; j< listOfIds.size();j++ )
                {
                    // quantityArray[j] = listOfIds.get(j).getProduct_quantity();
                    params.put("quntity["+j+"]", listOfIds.get(j).getProduct_quantity());
                }

                variantArray = new String[listOfIds.size()];
                for (int k=0; k<listOfIds.size();k++)
                {
                    // variantArray[k] = listOfIds.get(k).getVariant_id();
                    params.put("variant_id["+k+"]", listOfIds.get(k).getVariant_id());
                }

                params.put("user_id", u_id);
                params.put("provider_id", provoder_id);
                params.put("order_type", "ONLINE");
                params.put("grand_total", String.valueOf(Double.parseDouble(sAmount)/100));
                params.put("sub_total",item_amt);


                return params;
            }

        };

        Singleton.getInstance(PaymentActivity.this).addTorequestqu(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        );
    }

    @Override
    public void onPaymentError(int i, String s) {
        try {
           // Toast.makeText(this, "Payment failed: " + i + " " + s, Toast.LENGTH_SHORT).show();
            Toast.makeText(this, "Payment Declined!", Toast.LENGTH_SHORT).show();
            finish();
        } catch (Exception e) {
            Log.e("TAGERROR", "Exception in onPaymentError", e);
        }
    }

    //////
  /*  private void getCartValue() {
        final ProgressDialog progressBar;
        progressBar = new ProgressDialog(this, R.style.AppCompatProgressDialogStyle);
        progressBar.setCancelable(false);
        progressBar.setMessage("Please Wait...");
        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressBar.show();

        RequestQueue queue = Volley.newRequestQueue(this);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                Utility.BASE_URL + "/cart", null, new com.android.volley.Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                //Log.d(TAG, "--addtocart_response-->" + response.toString());
                progressBar.dismiss();

                totalCartValue = response.optString("amount_payable");
                //getAddress();
            }
        }, new com.android.volley.Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "addingCartError: " + error.getMessage());
                if (error != null) {
                    NetworkResponse networkResponse = error.networkResponse;
                    int in = networkResponse.statusCode;
                    Toast.makeText(RazorPayActivity.this, "" + in, Toast.LENGTH_SHORT).show();
                }
                progressBar.dismiss();
                Toast.makeText(RazorPayActivity.this, "Something went wrong!", Toast.LENGTH_SHORT).show();
            }

        }) {

            @Override
            protected com.android.volley.Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
                int i = response.statusCode;
                Toast.makeText(RazorPayActivity.this, "" + i, Toast.LENGTH_SHORT).show();
                return super.parseNetworkResponse(response);
            }

        };

        jsonObjReq.setTag(TAG);
        // Adding request to request queue
        queue.add(jsonObjReq);

    }
*/
   /* private void getAddress() {
        final ProgressDialog progressBar;
        progressBar = new ProgressDialog(this, R.style.AppCompatProgressDialogStyle);
        progressBar.setCancelable(false);
        progressBar.setMessage("Please Wait...");
        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressBar.show();

        RequestQueue queue = Volley.newRequestQueue(this);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Utility.BASE_URL + "/cart",
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, response);
                        progressBar.dismiss();

                        try {
                            JSONArray array = new JSONArray(response);
                            for (int i = 0; i < array.length(); i++) {
                                JSONObject object = array.optJSONObject(i);
                                addressId = object.optString("id");
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "" + error.getMessage());
                progressBar.dismiss();
                if (error != null) {
                    NetworkResponse networkResponse = error.networkResponse;
                    int in = networkResponse.statusCode;
                    if (in == 402)
                        Toast.makeText(RazorPayActivity.this, "Product already present in your cart", Toast.LENGTH_SHORT).show();
                    else if (in == 400)
                        Toast.makeText(RazorPayActivity.this, "Product is currently Out of stock!", Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(RazorPayActivity.this, "Something went wrong!", Toast.LENGTH_SHORT).show();
                }
            }
        }) {

            @Override
            public byte[] getBody() throws AuthFailureError {
                return null;
            }
        };

        stringRequest.setTag(TAG);
        // Adding request to request queue
        queue.add(stringRequest);

    }*/

    ///////razorPay
    /*private void callRazorPay(String payId) {
        final ProgressDialog progressBar;
        progressBar = new ProgressDialog(RazorPayActivity.this, R.style.AppCompatProgressDialogStyle);
        progressBar.setCancelable(false);
        progressBar.setMessage("Please Wait...");
        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressBar.show();

        RequestQueue queue = Volley.newRequestQueue(RazorPayActivity.this);

        final JSONObject jsonObject = new JSONObject();
        try {

            jsonObject.put("pay_id", payId);
            jsonObject.put("address_id", sAddrId);
            jsonObject.put("amount", sAmount);
            jsonObject.put("type", "buyProducts");
            jsonObject.put("selection", new JSONArray());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        StringRequest jsonObjReq = new StringRequest(Request.Method.POST,
                Utility.BASE_URL + "/razorPay", new com.android.volley.Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "--razorPay_response-->" + response.toString());
                progressBar.dismiss();

                if (nStatusCode == 200) {
                    finish();
                    Toast.makeText(RazorPayActivity.this, "Order placed successfully", Toast.LENGTH_SHORT).show();
                }
            }
        }, new com.android.volley.Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.dismiss();
                Log.e(TAG, "razorPayError: " + error.getMessage());
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    int in = networkResponse.statusCode;
                    Log.e(TAG, "razorPayError_code: " + in);
                }
                Toast.makeText(RazorPayActivity.this, "Something went wrong !", Toast.LENGTH_SHORT).show();

            }

        }) {

            @Override
            public byte[] getBody() throws AuthFailureError {
                return jsonObject.toString().getBytes();
            }

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            protected com.android.volley.Response<String> parseNetworkResponse(NetworkResponse response) {
                if (response != null) {
                    nStatusCode = response.statusCode;
                }
                return super.parseNetworkResponse(response);
            }


        };

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                180000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        jsonObjReq.setTag(TAG);
        // Adding request to request queue
        queue.add(jsonObjReq);

    }*/
}
