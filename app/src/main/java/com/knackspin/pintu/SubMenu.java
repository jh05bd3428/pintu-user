package com.knackspin.pintu;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;

import com.google.android.material.tabs.TabLayout;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.knackspin.pintu.Adapter.SubmenuAdapter;
import com.knackspin.pintu.Database.DatabaseHelper;
import com.knackspin.pintu.Model.Cart_model;
import com.knackspin.pintu.Model.Fragmnet_model;
import com.knackspin.pintu.utils.UrlPoint;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SubMenu extends AppCompatActivity {

    public SubmenuAdapter submenuAdapter;
    public List<String> subCatMenu;
    List<Fragmnet_model> listOfSubCat;
    private ViewPager mViewPager;
    ImageView sub_cart_menu,sub_cart_notification,search_sub_menu,iv_back;
    private TabLayout tabLayout;
    TextView toolbar_text,sub_cart_count,pincode_sub,id_sub;
    int pos, random_pos;

    String id,pro_id,toolbar_name,random_cat_id,listSize;

    public static final String DEFAULT = "NA";

    public ArrayList<Cart_model> listOfCartItems;
    DatabaseHelper databaseHelper;

    SharedPreferences sharedPreferences;
    LinearLayout ll_empty_product;

    boolean flag = false;
    String value = "";


    @androidx.annotation.RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_menu);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        listOfSubCat = new ArrayList<>();
        subCatMenu = new ArrayList<>();
        listOfCartItems = new ArrayList<>();

        initialization();

        databaseHelper = new DatabaseHelper(this);

        sharedPreferences = getSharedPreferences("MyData", Context.MODE_PRIVATE);
        pro_id = sharedPreferences.getString("providerID",DEFAULT);


        if (getIntent().getExtras() != null) {

            pos = getIntent().getIntExtra("postion", 0);
            toolbar_name = getIntent().getStringExtra("name");
            id = getIntent().getStringExtra("id");
            random_cat_id = getIntent().getStringExtra("ran_sub_cat");
            flag = getIntent().getExtras().getBoolean("flag");
            toolbar_text.setText(toolbar_name);
            id_sub.setText(id);
            Log.d("TAG1", String.valueOf(id));
        }

        sub_cart_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(SubMenu.this, CartDetails.class);
                startActivity(intent);
                //finish();
            }
        });

        search_sub_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(SubMenu.this, SerachActivity.class);
                startActivity(intent);
            }
        });

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });

        listOfCartItems = databaseHelper.getAllCartItems();
        listSize = String.valueOf(listOfCartItems.size());
        sub_cart_count.setText(" "+ listSize);

        getData();

    }

    private void getData() {

        CustomLayout.setUIToWait(SubMenu.this, true);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, UrlPoint.getSubcategory, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                CustomLayout.setUIToWait(SubMenu.this, false);

                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(response);

                    if (jsonObject.optString("status").equalsIgnoreCase("found")) {
                        mViewPager.setVisibility(View.VISIBLE);
                        tabLayout.setVisibility(View.VISIBLE);
                        ll_empty_product.setVisibility(View.GONE);

                        try {
                            JSONArray jsonArray = jsonObject.getJSONArray("list");

                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject ob = jsonArray.getJSONObject(i);
                                Fragmnet_model fragmnet_model = new Fragmnet_model(ob.getString("name"), ob.getString("id"));
                                int sc_value = ob.getInt("id");
                                Log.d("TAG12", String.valueOf(sc_value));
                                listOfSubCat.add(fragmnet_model);


                              if (flag)
                              {

                                  Log.d("RANDOM", "ID is"+" "+ random_cat_id + sc_value);
                                  if (random_cat_id.equals(""+sc_value)) {
                                      random_pos = i;
                                  }
                              }

                            }
                            addTabs(mViewPager);
                            submenuAdapter.notifyDataSetChanged();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        Toast.makeText(SubMenu.this, "Product Not Found", Toast.LENGTH_SHORT).show();
                        mViewPager.setVisibility(View.GONE);
                        tabLayout.setVisibility(View.GONE);
                        ll_empty_product.setVisibility(View.VISIBLE);
                        Log.d("TAG110", "product not found");
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Log.d("Exception", e.getMessage());

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                CustomLayout.setUIToWait(SubMenu.this, false);
                Log.d("random", error.getMessage());

                //Toast.makeText(SubMenu.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                if(error instanceof ServerError)
                {

                    Toast.makeText(SubMenu.this, "Server Down! Please try again", Toast.LENGTH_SHORT).show();
                }

                else if (error instanceof NetworkError)
                {
                    Toast.makeText(SubMenu.this, "Check your Internet Connection", Toast.LENGTH_SHORT).show();
                }

                else
                {
                    Toast.makeText(SubMenu.this, "Something Broken", Toast.LENGTH_SHORT).show();
                }

            }
        }) {
            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<String, String>();
                params.put("category_id", id);
                params.put("provider_id", sharedPreferences.getString("store_id", DEFAULT));
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        );

    }

    private void initialization() {

        id_sub = findViewById(R.id.id_sub);
        pincode_sub = findViewById(R.id.pin_code_sub);
        toolbar_text = findViewById(R.id.toolbar_text);
        sub_cart_menu = findViewById(R.id.subcart_menu);
        sub_cart_notification = findViewById(R.id.submenu_notification);
        sub_cart_count = findViewById(R.id.subCartCount);
        search_sub_menu = findViewById(R.id.search_sub_menu);

        mViewPager = findViewById(R.id.viewPager);
        tabLayout = findViewById(R.id.tabID);

        ll_empty_product = findViewById(R.id.ll_empty_product);

        iv_back = findViewById(R.id.iv_back);
    }


    public void addTabs(ViewPager viewPager) {

        if (listOfSubCat != null) {

            submenuAdapter = new SubmenuAdapter(getSupportFragmentManager(), listOfSubCat);
            mViewPager.setAdapter(submenuAdapter);
            mViewPager.setCurrentItem(pos);
            tabLayout.setupWithViewPager(mViewPager);
            viewPager.setAdapter(submenuAdapter);
        }

        if (flag) {

            mViewPager.setCurrentItem(random_pos);
            tabLayout.setupWithViewPager(mViewPager);

        }
    }


    @Override
    protected void onStart() {
        super.onStart();

        listOfCartItems = databaseHelper.getAllCartItems();
        listSize = String.valueOf(listOfCartItems.size());
        sub_cart_count.setText(" "+ listSize);
        updateCartCount();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public void updateCartCount(){
        listOfCartItems = databaseHelper.getAllCartItems();
        listSize = String.valueOf(listOfCartItems.size());

        if (listSize.contains("0"))
        {
            sub_cart_count.setVisibility(View.GONE);
        }
        else
        {
            sub_cart_count.setVisibility(View.VISIBLE);
            sub_cart_count.setText(" "+ listSize);

        }


    }
}