package com.knackspin.pintu;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Handler;
import android.preference.PreferenceManager;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;

import java.util.Locale;

public class LauncherActivity extends AppCompatActivity {


    Boolean first;
    SharedPreferences preferences,sharedPreferences;
    Button new_login_button, new_reg_button,step_in_button,skip_text;
    public static final String DEFAULT = "NA";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launcher);


        initialization();



        sharedPreferences = getSharedPreferences("MyData", Context.MODE_PRIVATE);
        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        first = preferences.getBoolean("first", true);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {


                if (sharedPreferences.getString("userId","NA").equalsIgnoreCase("NA"))
                {

                    Intent intent = new Intent(LauncherActivity.this, Login.class);
                    startActivity(intent);
                    finish();


                }

                else
                    {
                        if (sharedPreferences.getString("store_name",DEFAULT).equalsIgnoreCase(DEFAULT))
                        {
                            Intent intent = new Intent(LauncherActivity.this, VerifyPincode.class);
                            startActivity(intent);
                            finish();
                        }
                        else
                        {
                            Intent intent = new Intent(LauncherActivity.this, HomeScreen.class);
                            startActivity(intent);
                            finish();
                        }


                    }



            }
        }, 2000);


    }


    private void initialization() {

        skip_text = findViewById(R.id.skip_text);
        new_login_button = findViewById(R.id.new_login_button);
        new_reg_button = findViewById(R.id.new_signup_button);
        step_in_button = findViewById(R.id.step_in_button);

    }


    private static boolean updateResources(Context context, String language) {
        Locale locale = new Locale(language);
        Locale.setDefault(locale);

        Resources resources = context.getResources();

        Configuration configuration = resources.getConfiguration();
        configuration.locale = locale;

        resources.updateConfiguration(configuration, resources.getDisplayMetrics());

        return true;
    }
}

//abundance of choice with more than 10000 proucts
//doorstep same day delivery
//flexible payment option
//Track your order
//super savings


/*
if(first)
        {
        new_login_button.setVisibility(View.VISIBLE);
        new_reg_button.setVisibility(View.VISIBLE);
        step_in_button.setVisibility(View.GONE);
        skip_text.setVisibility(View.VISIBLE);



        } else
        {

        new_login_button.setVisibility(View.GONE);
        new_reg_button.setVisibility(View.GONE);
        step_in_button.setVisibility(View.VISIBLE);
        skip_text.setVisibility(View.GONE);

        }


        new_login_button.setOnClickListener(new View.OnClickListener() {
@Override
public void onClick(View v) {

        Intent intent = new Intent(LauncherActivity.this, Login.class);
        startActivity(intent);
        finish();
        }
        });

        new_reg_button.setOnClickListener(new View.OnClickListener() {
@Override
public void onClick(View v) {

        Intent intent = new Intent(LauncherActivity.this, Regsitration.class);
        startActivity(intent);
        finish();
        }
        });

        skip_text.setOnClickListener(new View.OnClickListener() {
@Override
public void onClick(View v) {



        Intent intent = new Intent(LauncherActivity.this, VerifyPincode.class);
        startActivity(intent);
        finish();
        }
        });


        step_in_button.setOnClickListener(new View.OnClickListener() {
@Override
public void onClick(View v) {

        Intent intent = new Intent(LauncherActivity.this, HomeScreen.class);
        startActivity(intent);
        finish();
        }
        });*/
