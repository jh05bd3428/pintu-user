package com.knackspin.pintu;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.knackspin.pintu.Address.FetchAddress;
import com.knackspin.pintu.Database.DatabaseHelper;
import com.knackspin.pintu.Model.Cart_model_new;
import com.knackspin.pintu.Singleton.Singleton;
import com.knackspin.pintu.utils.UrlPoint;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;

public class DetailsActivity extends AppCompatActivity {

    Button  place_order_online;
    TextView item_count, view_provider_id, item_tot,wallet_bal,place_order_cash;
    TextView item_amount, final_pay, delivery_fee_details, change_address_button, delivery_user_id, pending_amount,discount_amount;

    String itemAmount,delivery_fee,walletBalance;
    String before_coupon_discount="0";
    String wallet,provider_id;


    RadioButton cod,online;
    String pay_type = "";

    Double p_final_pay, p_new, p_item_amount;

    private ArrayList<Cart_model_new> listOfIds;
    DatabaseHelper databaseHelper;

    String i_count,credit_limit;
    TextView details_username, details_address, details_street;

    public static final String DEFAULT = "";
    public static final String WALLET_DEFAULT = "0";
    String[] prodIdArray;
    String[] quantityArray;
    String[] variantArray;
    String u_id;

    ImageView detail_arrow_button;
    DecimalFormat decimalFormat;

    RelativeLayout coupon_relative_layout,rl_no_address_layout;


    public static final int PASS_DATA = 101;
    public static final int COUPON_DATA = 102;

    SharedPreferences sharedPreferences;

    LinearLayout address_layout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        initialization();

        databaseHelper = new DatabaseHelper(this);

        listOfIds = new ArrayList<>();
        decimalFormat = new DecimalFormat("0.00");

        sharedPreferences = getSharedPreferences("MyData", Context.MODE_PRIVATE);

        u_id = sharedPreferences.getString("userId", DEFAULT);
        walletBalance = sharedPreferences.getString("wallet_balance", WALLET_DEFAULT);

        view_provider_id.setText(provider_id);
        pending_amount.setText(walletBalance);


        if (getIntent().getExtras() != null) {
            itemAmount = getIntent().getExtras().getString("item_amount");
            item_tot.setText(itemAmount);

            i_count = getIntent().getExtras().getString("Item Count");

             delivery_fee = sharedPreferences.getString("store_delivery_charge","0");
        }


        double d_item_amount = Double.parseDouble(itemAmount);
        double d_delivery_fee = Double.parseDouble(delivery_fee);
        double d_wallet_balance = Double.parseDouble(walletBalance);
        double d_before_discount_amount = Double.parseDouble(before_coupon_discount);

        if (d_item_amount < Double.parseDouble(sharedPreferences.getString("store_minimum_charge","0")))
        {

            //final pay calculation
            p_final_pay = (d_item_amount - Double.parseDouble(before_coupon_discount) + d_delivery_fee) -d_wallet_balance;
            final_pay.setText("\u20B9"+String.valueOf(p_final_pay));
            final_pay.setText("\u20B9"+decimalFormat.format(p_final_pay));

            //item total calculation
            item_amount.setText("\u20B9"+itemAmount);
            item_amount.setText("\u20B9"+decimalFormat.format(d_item_amount));

           //set coupon discount details
            discount_amount.setText("-"+" "+"\u20B9"+before_coupon_discount);
            discount_amount.setText("-"+" "+"\u20B9"+decimalFormat.format(d_before_discount_amount));

           //set delivery fee details
            delivery_fee_details.setText("\u20B9"+delivery_fee);
            delivery_fee_details.setText("\u20B9"+decimalFormat.format(d_delivery_fee));

            //set pending amount details
            pending_amount.setText("\u20B9"+wallet);
            pending_amount.setText("\u20B9"+decimalFormat.format(d_wallet_balance));





        } else
        {

            p_final_pay = (d_item_amount - Double.parseDouble(before_coupon_discount) + 0) -d_wallet_balance;
            final_pay.setText("\u20B9"+String.valueOf(p_final_pay));
            final_pay.setText("\u20B9"+decimalFormat.format(p_final_pay));

            //item total calculation
            item_amount.setText("\u20B9"+itemAmount);
            item_amount.setText("\u20B9"+decimalFormat.format(d_item_amount));

            //set coupon discount details
            discount_amount.setText("-"+" "+"\u20B9"+before_coupon_discount);
            discount_amount.setText("-"+" "+"\u20B9"+decimalFormat.format(d_before_discount_amount));

            //set delivery fee details
            delivery_fee_details.setText("0.00");
           // delivery_fee_details.setText(decimalFormat.format(delivery_fee));

            //set pending amount details
            pending_amount.setText("\u20B9"+wallet);
            pending_amount.setText("\u20B9"+decimalFormat.format(d_wallet_balance));

        }



        place_order_cash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


              if(details_address.getText().toString().isEmpty())
              {
                  Toast.makeText(DetailsActivity.this, "Enter Delivery address", Toast.LENGTH_SHORT).show();
              } else
                  {


                      sendOrderDetails();

                  }

            }
        });

        change_address_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                changeAddress();

            }
        });

        detail_arrow_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });

        delivery_user_id.setText(u_id);


        place_order_online.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                sendOnlineData();
            }
        });

        coupon_relative_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent intent = new Intent(DetailsActivity.this, ShowCoupon.class);
                startActivityForResult(intent,COUPON_DATA);
            }
        });

        rl_no_address_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                changeAddress();


            }
        });
    }




    private void sendOnlineData() {

        Intent intent = new Intent(DetailsActivity.this, PaymentActivity.class);
        intent.putExtra("fAmount", final_pay.getText().toString().replace("\u20B9", ""));
        intent.putExtra("item_total",item_amount.getText().toString());
        startActivity(intent);
    }


    private void changeAddress() {



        if(CheckingPermissionIsEnabledOrNot())
        {
            Toast.makeText(DetailsActivity.this, "All Permissions Granted Successfully", Toast.LENGTH_LONG).show();
            Intent intent = new Intent(this, FetchAddress.class);
            startActivityForResult(intent,PASS_DATA);
        }

        else
        {
            grantPermission();
        }

    }

    private void getArray() {

       /* listOfIds =  databaseHelper.getAllIdItems();

        prodIdArray = new String[listOfIds.size()];
        for (int i = 0; i < listOfIds.size(); i++) {
            prodIdArray[i] = listOfIds.get(i).getProduct_id();
        }

        quantityArray = new String[listOfIds.size()];
        for (int j=0; j< listOfIds.size();j++ )
        {
            quantityArray[j] = listOfIds.get(j).getProduct_quantity();
        }

        variantArray = new String[listOfIds.size()];
        for (int k=0; k<listOfIds.size();k++)
        {
             variantArray[k] = listOfIds.get(k).getVariant_id();
        }
*/
    }

    private void sendOrderDetails() {

        CustomLayout.setUIToWait(DetailsActivity.this, true);
        final String f_amount = final_pay.getText().toString();
        final String d_uid = delivery_user_id.getText().toString();


        StringRequest stringRequest = new StringRequest(Request.Method.POST, UrlPoint.placeOrderApi, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                CustomLayout.setUIToWait(DetailsActivity.this, false);

                JSONObject jsonObject = null;

                try {
                    jsonObject = new JSONObject(response);

                    if (jsonObject.optString("status").equalsIgnoreCase("success"))
                    {
                        databaseHelper.Delete(DetailsActivity.this);

                        Intent intent = new Intent(DetailsActivity.this, OrderPlaced.class);
                        startActivity(intent);
                        finish();
                    } else
                        {
                            Toast.makeText(DetailsActivity.this,response, Toast.LENGTH_SHORT).show();
                        }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                CustomLayout.setUIToWait(DetailsActivity.this, false);

                if(error instanceof ServerError)
                {

                    Toast.makeText(DetailsActivity.this, "Server Down", Toast.LENGTH_SHORT).show();
                }

                else if (error instanceof NetworkError)
                {
                    Toast.makeText(DetailsActivity.this, "Check your Internet Connection", Toast.LENGTH_SHORT).show();
                }
                 else
                     {
                         Toast.makeText(DetailsActivity.this, error.toString(), Toast.LENGTH_SHORT).show();

                     }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<String, String>();

                listOfIds =  databaseHelper.getAllIdItems();

                prodIdArray = new String[listOfIds.size()];
                for (int i = 0; i < listOfIds.size(); i++) {
                   // prodIdArray[i] = listOfIds.get(i).getProduct_id();
                    params.put("product_id["+i+"]", listOfIds.get(i).getProduct_id());

                }

                quantityArray = new String[listOfIds.size()];
                for (int j=0; j< listOfIds.size();j++ )
                {
                   // quantityArray[j] = listOfIds.get(j).getProduct_quantity();
                    params.put("quntity["+j+"]", listOfIds.get(j).getProduct_quantity());
                }

                variantArray = new String[listOfIds.size()];
                for (int k=0; k<listOfIds.size();k++)
                {
                   // variantArray[k] = listOfIds.get(k).getVariant_id();
                    params.put("variant_id["+k+"]", listOfIds.get(k).getVariant_id());
                }

                params.put("user_id", d_uid);
                params.put("provider_id", sharedPreferences.getString("store_id", DEFAULT));
                params.put("order_type", "COD");
                params.put("grand_total", final_pay.getText().toString());
                params.put("sub_total",item_amount.getText().toString() );
                params.put("shipping_charge", delivery_fee_details.getText().toString());

                return params;
            }

        };

        Singleton.getInstance(DetailsActivity.this).addTorequestqu(stringRequest);

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        );
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PASS_DATA) {
            if(resultCode == RESULT_OK) {

                String user_name = data.getStringExtra("cx_name");
                String user_address = data.getStringExtra("cx_address");
                String user_street = data.getStringExtra("cx_street");
                String user_city = data.getStringExtra("cx_city");
                String user_state = data.getStringExtra("cx_state");
                String user_pincode = data.getStringExtra("cx_zip");
                String user_phone = data.getStringExtra("cx_phone");


                details_address.setText(user_address +""+ user_street);
                details_username.setText(user_name);

            }
        } else if (requestCode == COUPON_DATA)
        {
            if (resultCode == RESULT_OK)
            {
                // after_discount_amount = data.getStringExtra("discount_amount");
                String coupon_code = data.getStringExtra("coupon_code");
                String coupon_discount_amount = data.getStringExtra("discount_amount");



                double d_item_amount = Double.parseDouble(itemAmount);
                double d_delivery_fee = Double.parseDouble(delivery_fee);
                double d_wallet_balance = Double.parseDouble(wallet);
                double d_discount_amount = Double.parseDouble(coupon_discount_amount);

                String cal_discount_amount = String.valueOf((d_item_amount * d_discount_amount) / 100);
                double d_cal_discount_amount = Double.parseDouble(cal_discount_amount);
                Log.d("Discount", cal_discount_amount);


                if (d_item_amount < Double.parseDouble(sharedPreferences.getString("store_minimum_charge","0")))
                {
                    p_final_pay = (d_item_amount - Double.parseDouble(cal_discount_amount) + d_delivery_fee) - d_wallet_balance;
                    final_pay.setText("\u20B9"+String.valueOf(p_final_pay));
                    final_pay.setText("\u20B9"+decimalFormat.format(p_final_pay));

                    //item total calculation
                    item_amount.setText("\u20B9"+itemAmount);
                    item_amount.setText("\u20B9"+decimalFormat.format(d_item_amount));

                    discount_amount.setText("-"+" "+"\u20B9"+cal_discount_amount);
                    discount_amount.setText("-"+" "+"\u20B9"+decimalFormat.format(d_cal_discount_amount));

                    //set delivery fee details
                    delivery_fee_details.setText("\u20B9"+delivery_fee);
                    delivery_fee_details.setText("+"+" "+"\u20B9"+decimalFormat.format(d_delivery_fee));

                    //set pending amount details
                    pending_amount.setText("\u20B9"+wallet);
                    pending_amount.setText("\u20B9"+decimalFormat.format(d_wallet_balance));


                } else
                    {
                        p_final_pay = (d_item_amount - Double.parseDouble(cal_discount_amount) + 0) - d_wallet_balance;
                        final_pay.setText("\u20B9"+String.valueOf(p_final_pay));
                        final_pay.setText("\u20B9"+decimalFormat.format(p_final_pay));

                        //item total calculation
                        item_amount.setText("\u20B9"+itemAmount);
                        item_amount.setText("\u20B9"+decimalFormat.format(d_item_amount));

                        discount_amount.setText("-"+" "+"\u20B9"+cal_discount_amount);
                        discount_amount.setText("-"+" "+"\u20B9"+decimalFormat.format(d_cal_discount_amount));

                        //set delivery fee details
                        delivery_fee_details.setText("0.00");
                        //delivery_fee_details.setText("\u20B9"+decimalFormat.format(d_delivery_fee));

                        //set pending amount details
                        pending_amount.setText("\u20B9"+wallet);
                        pending_amount.setText("\u20B9"+decimalFormat.format(d_wallet_balance));
                    }


            }
        }

    }

    private void getSavedAddress() {

        CustomLayout.setUIToWait(DetailsActivity.this, true);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, UrlPoint.getUserAddress, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                CustomLayout.setUIToWait(DetailsActivity.this, false);

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    if (jsonObject.optString("status"). equalsIgnoreCase("success"))
                    {

                        //Toast.makeText(DetailsActivity.this, "Address present", Toast.LENGTH_SHORT).show();

                        JSONArray jsonArray = jsonObject.optJSONArray("data");

                        for (int add =0;add<jsonArray.length();add++)
                        {
                            JSONObject ob = jsonArray.getJSONObject(add);
                            String user_name = ob.getString("name")+" "+ob.optString("last_name");
                            String user_address = ob.getString("address");
                            String user_street = ob.getString("street");
                            String user_city = ob.getString("city");
                            String user_state = ob.getString("state");
                            String user_zip = ob.getString("zip");
                            String user_phone = ob.getString("phone");

                            details_username.setText(user_name);
                            details_address.setText(user_address);
                            details_street.setText(user_street);

                            if (details_address.getText().toString().isEmpty())
                            {
                                address_layout.setVisibility(View.GONE);
                                rl_no_address_layout.setVisibility(View.VISIBLE);
                                //change_address_button.setText("ADD ADDRESS");
                            } else
                                {
                                    address_layout.setVisibility(View.VISIBLE);
                                    rl_no_address_layout.setVisibility(View.GONE);
                                    change_address_button.setText("UPDATE ADDRESS");
                                }

                        }

                    }
                    else if (jsonObject.optString("status").equalsIgnoreCase("user id not found"))
                    {

                        address_layout.setVisibility(View.GONE);
                        rl_no_address_layout.setVisibility(View.VISIBLE);



                    }



                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                CustomLayout.setUIToWait(DetailsActivity.this, false);

                Toast.makeText(DetailsActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        })

        {
            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id",u_id);
                return params;
            }
        };

        Singleton.getInstance(DetailsActivity.this).addTorequestqu(stringRequest);

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        );

    }


    private void initialization() {

        //place_order = findViewById(R.id.place_order_button);

        place_order_cash = findViewById(R.id.place_order_cash);
        place_order_online = findViewById(R.id.place_order_online);

        change_address_button = findViewById(R.id.change_address_button);
        item_amount = findViewById(R.id.item_amount);
        final_pay = findViewById(R.id.final_pay);
        delivery_user_id = findViewById(R.id.delivery_user_id);

        details_username = findViewById(R.id.details_username);
        details_address = findViewById(R.id.details_address);
        details_street = findViewById(R.id.details_street);

        pending_amount = findViewById(R.id.pending_amount);
        discount_amount = findViewById(R.id.discount_amount);

       /* cod = findViewById(R.id.cod_pay);
        online = findViewById(R.id.online_pay);*/

        delivery_fee_details = findViewById(R.id.delivery_fee_details);
        view_provider_id = findViewById(R.id.view_provider_id);
        wallet_bal = findViewById(R.id.wallet_bal);

        item_tot = findViewById(R.id.item_tot);

        detail_arrow_button = findViewById(R.id.detail_back_arrow);

        coupon_relative_layout = findViewById(R.id.coupon_relative_layout);

        address_layout = findViewById(R.id.address_layout);
        rl_no_address_layout = findViewById(R.id.rl_no_address_layout);
    }


    private void grantPermission()
    {

        ActivityCompat.requestPermissions(DetailsActivity.this, new String[]
                {
                        ACCESS_FINE_LOCATION

                }, 101);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode)
        {
            case 101:

                if (grantResults.length > 0)
                {
                    boolean LocationPermission = grantResults[0] == PackageManager.PERMISSION_GRANTED;

                    if (LocationPermission) {

                        Toast.makeText(DetailsActivity.this, "Permission Granted", Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(this, FetchAddress.class);
                        startActivityForResult(intent,PASS_DATA);

                    }
                    else {
                        Toast.makeText(DetailsActivity.this,"Permission Denied",Toast.LENGTH_LONG).show();

                    }
                }

                break;
        }
    }

    public boolean CheckingPermissionIsEnabledOrNot() {

        int FirstPermissionResult = ContextCompat.checkSelfPermission(getApplicationContext(), ACCESS_FINE_LOCATION);

        return FirstPermissionResult == PackageManager.PERMISSION_GRANTED ;

    }


    @Override
    protected void onResume() {
        super.onResume();

        getSavedAddress();


    }
}
