package com.knackspin.pintu;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.knackspin.pintu.Singleton.Singleton;
import com.knackspin.pintu.utils.UrlPoint;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ForgotPassword extends AppCompatActivity {

    EditText phone_number,otp,password;
    Button verify_phone_button, verify_otp_button, change_password;
    String get_otp,shared_phone;

    boolean verifyPhone = false;
    boolean verifyOtp = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        initialization();


        otp.setEnabled(false);
        password.setEnabled(false);
        change_password.setEnabled(false);


        phone_number.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (phone_number.getText().length() == 10 && verifyPhone == false)
                {
                    checkPhoneNumber();
                    verifyPhone = true;
                }

            }

            @Override
            public void afterTextChanged(Editable s) {


                if (!(phone_number.getText().length() == 10))
                {
                    verifyPhone = false;
                }


            }
        });


    }





    private void checkPhoneNumber() {

        CustomLayout.setUIToWait(ForgotPassword.this,true);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, UrlPoint.checkMobileNumber, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                CustomLayout.setUIToWait(ForgotPassword.this,false);

                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(response);

                    if (jsonObject.optString("status").equalsIgnoreCase("success"))
                    {

                        Toast.makeText(ForgotPassword.this, "Please enter the OTP received", Toast.LENGTH_SHORT).show();


                        get_otp = jsonObject.getString("otp");
                            checkOtp();

                    } else
                        {
                            Toast.makeText(ForgotPassword.this, "Phone number not registered", Toast.LENGTH_SHORT).show();
                        }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d("phoneException", e.getMessage());
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                CustomLayout.setUIToWait(ForgotPassword.this,false);
                Toast.makeText(ForgotPassword.this, error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        })

        {
            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<String, String>();
                params.put("phone",phone_number.getText().toString());
                return params;
            }

        };

        Singleton.getInstance(ForgotPassword.this).addTorequestqu(stringRequest);

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        );
    }


    private void checkOtp() {

        otp.setEnabled(true);

        otp.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (otp.getText().length() == 4 && verifyOtp == false)
                {
                    verifyOtp();
                    verifyOtp = true;
                }


            }

            @Override
            public void afterTextChanged(Editable s) {

                if (!(otp.getText().length() == 4))
                {
                    verifyOtp = false;
                }


            }
        });

    }

    private void verifyOtp() {

        if (get_otp.equalsIgnoreCase(otp.getText().toString()))
        {
            Toast.makeText(this, "OTP Verified! Update password now." , Toast.LENGTH_SHORT).show();
            updatePassword();
        }
        else
        {
            Toast.makeText(this, "Enter correct otp", Toast.LENGTH_SHORT).show();

        }


    }

    private void updatePassword() {

        password.setEnabled(true);
        change_password.setEnabled(true);
        change_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (password.getText().toString().isEmpty())
                {
                    Toast.makeText(ForgotPassword.this, "Enter Valid Password", Toast.LENGTH_SHORT).show();
                } else
                {
                    changePassword();
                }
            }
        });



    }

    private void changePassword() {

        CustomLayout.setUIToWait(ForgotPassword.this,true);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, UrlPoint.changPassword, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                CustomLayout.setUIToWait(ForgotPassword.this,false);

               JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(response);

                    if (jsonObject.getString("status"). equalsIgnoreCase("success"))
                    {

                        SharedPreferences sharedPreferences = getSharedPreferences("MyData", Context.MODE_PRIVATE);
                        shared_phone = sharedPreferences.getString("userPhone", "");


                        Toast.makeText(ForgotPassword.this, "Password changed succesfully", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(ForgotPassword.this, Login.class);
                        startActivity(intent);
                        finish();
                    } else
                        {
                            Toast.makeText(ForgotPassword.this, "Password change failed", Toast.LENGTH_SHORT).show();
                        }


                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d("ChangeError", e.getMessage());
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                CustomLayout.setUIToWait(ForgotPassword.this,false);
                Toast.makeText(ForgotPassword.this, error.getMessage(), Toast.LENGTH_SHORT).show();


            }
        })
        {
            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<String, String>();
                params.put("phone",phone_number.getText().toString());
                params.put("password", password.getText().toString());
                return params;
            }


        };

        Singleton.getInstance(ForgotPassword.this).addTorequestqu(stringRequest);

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        );
    }

    private void initialization() {

        phone_number = findViewById(R.id.et_forgot_phone);
        otp = findViewById(R.id.et_forgot_otp);
        password = findViewById(R.id.et_forgot_password);
        change_password = findViewById(R.id.bt_reset_password);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
    }
}
