package com.knackspin.pintu;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.knackspin.pintu.Adapter.OfferNotifAdapter;
import com.knackspin.pintu.Database.DatabaseHelper;
import com.knackspin.pintu.Model.Cart_model;
import com.knackspin.pintu.Model.OfferDialogModel;
import com.knackspin.pintu.Model.OfferNotifModel;
import com.knackspin.pintu.Singleton.Singleton;
import com.knackspin.pintu.utils.UrlPoint;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NotifOffers extends AppCompatActivity {


    private RecyclerView recyclerView;
    private OfferNotifAdapter offerNotifAdapter;
    private List<OfferNotifModel> listofOffers;
    private List<OfferDialogModel> listOfDilogModels;
    private RecyclerView.LayoutManager layoutManager;
    public ArrayList<Cart_model> listOfCartItems;

    DatabaseHelper databaseHelper;

    String u_id,p_id;
    String listSize;
    TextView tvCartOfferCount;
    ImageView offer_cart_button,detail_back_arrow;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notif_offers);

        listofOffers = new ArrayList<>();
        recyclerView = findViewById(R.id.offer_recylerview);
        offerNotifAdapter = new OfferNotifAdapter(listofOffers, this, NotifOffers.this);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(offerNotifAdapter);
        databaseHelper = new DatabaseHelper(this);
        tvCartOfferCount = findViewById(R.id.tvCartOfferBadge);
        offer_cart_button = findViewById(R.id.offer_cart_button);
        detail_back_arrow = findViewById(R.id.detail_back_arrow);

        SharedPreferences sharedPreferences = getSharedPreferences("MyData", Context.MODE_PRIVATE);
        u_id = sharedPreferences.getString("userId", "NA");
        p_id = sharedPreferences.getString("providerID", "NA");


        getOfferProducts();

        offer_cart_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(NotifOffers.this, CartDetails.class);
                startActivity(intent);

            }
        });

        detail_back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(NotifOffers.this, HomeScreen.class);
                startActivity(intent);

            }
        });

    }

    private void getOfferProducts() {

        CustomLayout.setUIToWait(NotifOffers.this,true);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, UrlPoint.showOfferProduct, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                CustomLayout.setUIToWait(NotifOffers.this,false);


                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.optString("flag").equalsIgnoreCase("true"))
                    {

                        JSONObject dataObject = jsonObject.getJSONObject("data");
                        JSONArray jsonArray = dataObject.getJSONArray("product");

                        for (int product =0; product<jsonArray.length();product++)
                        {
                            JSONArray ja =  jsonArray.getJSONArray(product);

                            for (int product_new=0; product_new<ja.length(); product_new++)
                            {

                                JSONObject jsonObject1 = ja.getJSONObject(product_new);
                                ArrayList<OfferDialogModel> listOfDialogOffers = new ArrayList<OfferDialogModel>();

                                JSONArray subProduct = jsonObject1.getJSONArray("variants");

                                for (int s_prod=0; s_prod<subProduct.length();s_prod++)
                                {
                                    JSONObject jsonObject2 = subProduct.getJSONObject(s_prod);

                                    listOfDialogOffers.add(new OfferDialogModel(jsonObject2.optString("id"),
                                            jsonObject2.optString("variantproduct_id"),
                                            jsonObject2.optString("variant_name"),
                                            jsonObject2.optString("mrp_price"),
                                            jsonObject2.optString("variant_price")));

                                }

                                OfferNotifModel onm = new OfferNotifModel(jsonObject1.optString("id"),
                                        jsonObject1.optString("brand_name"),
                                        jsonObject1.optString("name"),
                                        jsonObject1.optString("photo_1"), listOfDialogOffers);

                                listofOffers.add(onm);
                            }

                        }

                         offerNotifAdapter.notifyDataSetChanged();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d("OfferException", e.getMessage());
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                CustomLayout.setUIToWait(NotifOffers.this,false);

                Toast.makeText(NotifOffers.this, error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        })

        {
            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", u_id);
                params.put("provider_id", p_id);
                return params;
            }

        };

        Singleton.getInstance(NotifOffers.this).addTorequestqu(stringRequest);

    }


    @Override
    protected void onStart() {
        super.onStart();

        listOfCartItems = databaseHelper.getAllCartItems();
        listSize = String.valueOf(listOfCartItems.size());
        tvCartOfferCount.setText(" "+ listSize);
        updateCartCount();

    }



    public void updateCartCount(){
        listOfCartItems = databaseHelper.getAllCartItems();
        tvCartOfferCount.setText(" "+ listOfCartItems.size());
    }
}
