package com.knackspin.pintu;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;

import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;

import com.google.android.material.navigation.NavigationView;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import android.view.MenuItem;
import android.view.ViewGroup;
import android.view.animation.LayoutAnimationController;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.play.core.appupdate.AppUpdateInfo;
import com.google.android.play.core.appupdate.AppUpdateManager;
import com.google.android.play.core.appupdate.AppUpdateManagerFactory;
import com.google.android.play.core.install.InstallState;
import com.google.android.play.core.install.InstallStateUpdatedListener;
import com.google.android.play.core.install.model.AppUpdateType;
import com.google.android.play.core.install.model.InstallStatus;
import com.google.android.play.core.install.model.UpdateAvailability;
import com.google.android.play.core.tasks.Task;
import com.knackspin.pintu.Adapter.HomePagerAdapter;
import com.knackspin.pintu.Adapter.RandomAdpater;
import com.knackspin.pintu.Adapter.SliderImageAdapter;
import com.knackspin.pintu.Adapter.StagAdapter;
import com.knackspin.pintu.Database.DatabaseHelper;
import com.knackspin.pintu.Model.Cart_model;
import com.knackspin.pintu.Model.HomePagerModel;
import com.knackspin.pintu.Model.RandomCategory;
import com.knackspin.pintu.Model.Shop_category;
import com.knackspin.pintu.Model.SliderModel;
import com.knackspin.pintu.Model.SliderNewModel;
import com.knackspin.pintu.utils.ConnectionCheck;
import com.knackspin.pintu.utils.UrlPoint;
import com.smarteist.autoimageslider.IndicatorAnimations;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;
import com.smarteist.autoimageslider.SliderViewAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;


import ss.com.bannerslider.banners.Banner;
import ss.com.bannerslider.views.BannerSlider;

public class HomeScreen extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener{

    ViewPager view_pager;
    BannerSlider bannerSlider;
    RandomAdpater randomAdpater;
    StagAdapter stagAdapter;
    HomePagerAdapter homePagerAdapter;
    SliderImageAdapter sliderViewAdapter;

    SliderView sliderView;

    ArrayList<Shop_category> listofCategory;
    ArrayList<RandomCategory> listOfRandomData;
    public static List<Banner> listOfBanner;
    ArrayList <HomePagerModel> listOfImages;
    public ArrayList<Cart_model> listOfCartItems;
    List<SliderNewModel> sliderItemList;

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;


    HomeScreen homeScreen;
    RecyclerView recyclerView,stag_recycle_view;
    LinearLayoutManager layoutManager;
    StaggeredGridLayoutManager staggeredGridLayoutManager;

    TextView cartNotificationCount,selected_pincode,store_name,button_change,button_dismiss,button_yes,button_no;
    Button reconnect_button;

    GridView gridView;
    TextView providerId;
    String getProviderID,listsize, getSelectedPincode, u_id,provider_id;

    ImageView home_cart,home_notification,home_search,change_pincode;

    DatabaseHelper databaseHelper;

    RelativeLayout connection_layout;
    LinearLayout no_connection_layout;
    DrawerLayout drawer;

    public static final String DEFAULT = "NA";

    AppUpdateManager appUpdateManager;
    Task<AppUpdateInfo> appUpdateInfoTask;
    int MY_REQUEST_CODE = 11;

    int currentPage = 0;
    Timer timer;
    final long DELAY_MS = 2000;
    final long PERIOD_MS = 5000;

    LayoutAnimationController layoutAnimation;
    private LinearLayout dotsLayout;
    private int current_position = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        /*FirebaseInstanceId.getInstance().getToken();
        Log.d("Token", FirebaseInstanceId.getInstance().getToken());*/

        appUpdateManager = AppUpdateManagerFactory.create(this);
        appUpdateInfoTask = appUpdateManager.getAppUpdateInfo();

        checkForUpdate();



        listofCategory = new ArrayList<>();
        listOfBanner = new ArrayList<>();
        listOfRandomData = new ArrayList<>();
        listOfCartItems = new ArrayList<>();
        listOfImages = new ArrayList<>();
        sliderItemList = new ArrayList<>();

        initialization();

        stagAdapter = new StagAdapter(listofCategory,this);
        randomAdpater = new RandomAdpater(listOfRandomData,this);
        homePagerAdapter = new HomePagerAdapter(listOfImages, this);
        sliderViewAdapter = new SliderImageAdapter(this);

        sliderView.setSliderAdapter(sliderViewAdapter);

        sliderView.setIndicatorAnimation(IndicatorAnimations.WORM); //set indicator animation by using SliderLayout.IndicatorAnimations. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
        sliderView.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
        sliderView.setAutoCycleDirection(SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH);
        sliderView.setIndicatorSelectedColor(Color.GREEN);
        sliderView.setIndicatorUnselectedColor(Color.GRAY);
        sliderView.setScrollTimeInSec(4); //set scroll delay in seconds :
        sliderView.startAutoCycle();

        recyclerView.setHasFixedSize(true);
        stag_recycle_view.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,true);
        staggeredGridLayoutManager = new StaggeredGridLayoutManager(3,LinearLayoutManager.VERTICAL);

        recyclerView.setLayoutManager(layoutManager);
        stag_recycle_view.setLayoutManager(staggeredGridLayoutManager);

        databaseHelper = new DatabaseHelper(this);

        sharedPreferences = getSharedPreferences("MyData", Context.MODE_PRIVATE);
        u_id = sharedPreferences.getString("userId", DEFAULT);



        if(getIntent().getExtras()!=null)
        {
            getIntent().getStringExtra("store_id");
            editor = sharedPreferences.edit();// edit to store data into xml file
            editor.putString("store_id", getIntent().getStringExtra("store_id"));// data that is being put in xml file
            editor.apply();

            getIntent().getStringExtra("store_name");
            editor = sharedPreferences.edit();// edit to store data into xml file
            editor.putString("store_name", getIntent().getStringExtra("store_name"));// data that is being put in xml file
            editor.apply();

            getIntent().getStringExtra("store_delivery_charge");
            editor = sharedPreferences.edit();// edit to store data into xml file
            editor.putString("store_delivery_charge", getIntent().getStringExtra("store_delivery_charge"));// data that is being put in xml file
            editor.apply();


            getIntent().getStringExtra("store_minimum_charge");
            editor = sharedPreferences.edit();// edit to store data into xml file
            editor.putString("store_minimum_charge", getIntent().getStringExtra("store_minimum_charge"));// data that is being put in xml file
            editor.apply();

        }

        listOfCartItems = databaseHelper.getAllCartItems();
        listsize = String.valueOf(listOfCartItems.size());

        if (listsize.contains("0"))
        {
            cartNotificationCount.setVisibility(View.GONE);
        }
        else
        {
            cartNotificationCount.setVisibility(View.VISIBLE);
            cartNotificationCount.setText(" "+ listsize);

        }

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        toggle.getDrawerArrowDrawable().setColor(Color.WHITE);
        drawer.addDrawerListener(toggle);
        toggle.syncState();


        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View view = navigationView.getHeaderView(0);
        TextView nav_user = view.findViewById(R.id.home_user_name);
        TextView nav_id = view.findViewById(R.id.user_id);
        TextView nav_code = view.findViewById(R.id.textProviderId);
        TextView selected_pincode = view.findViewById(R.id.selected_pincode);


        if (u_id != DEFAULT)
        {
            //navigationView.getMenu().findItem(R.id.nav_logout).setVisible(true);
            navigationView.getMenu().setGroupVisible(R.id.g10,false); //login hidden
            navigationView.getMenu().setGroupVisible(R.id.g9,true); //logout visible
            //nav_user.setText(sharedPreferences.getString("f_name",DEFAULT));

        } else
        {
            navigationView.getMenu().setGroupVisible(R.id.g10,true);
            navigationView.getMenu().setGroupVisible(R.id.g9,false);
            //nav_user.setText("Please LogIn");

        }


        nav_user.setText(sharedPreferences.getString("f_name",DEFAULT));
        nav_id.setText(u_id);
        nav_code.setText(sharedPreferences.getString("store_id", DEFAULT));
        selected_pincode.setText(sharedPreferences.getString("selected_pincode", DEFAULT));


        if (sharedPreferences.getString("store_name", DEFAULT).equalsIgnoreCase("DEFAULT"))
        {
            store_name.setText("Please select your store");
        }

        else
            {
                store_name.setText(sharedPreferences.getString("store_name", DEFAULT).toUpperCase());

            }




        change_pincode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                popUpConfirmation();

            }
        });

        home_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(HomeScreen.this, CartDetails.class);
                startActivity(intent);
            }
        });

        home_notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(HomeScreen.this, Notification.class);
                startActivity(intent);
            }
        });

        home_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(HomeScreen.this, SerachActivity.class);
                startActivity(intent);
            }
        });

        reconnect_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                checkConnection();
            }
        });

        view_pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                if (current_position > listOfImages.size() -1)
                    current_position = 0;
                    createDotsShow(current_position++);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });



        checkConnection();

    }

    private void checkConnection() {


        if(new ConnectionCheck(this).isNetworkAvailable())
        {
            connection_layout.setVisibility(View.VISIBLE);
            no_connection_layout.setVisibility(View.GONE);
            getHomePageData();
            home_cart.setClickable(true);
            home_notification.setClickable(true);
            home_search.setClickable(true);
            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);


        } else
        {
            Toast.makeText(homeScreen, "Please check your Internet connection", Toast.LENGTH_SHORT).show();
            connection_layout.setVisibility(View.GONE);
            no_connection_layout.setVisibility(View.VISIBLE);
            home_cart.setClickable(false);
            home_notification.setClickable(false);
            home_search.setClickable(false);
            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

        }
    }

    private void getHomePageData() {

        CustomLayout.setUIToWait(HomeScreen.this,true);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, UrlPoint.homePageApi, new Response.Listener<String>() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onResponse(String response) {
                CustomLayout.setUIToWait(HomeScreen.this,false);




                if (listofCategory!=null)
                {
                    listofCategory.clear();
                }

                if (listOfRandomData!=null)
                {
                    listOfRandomData.clear();
                }


                try {
                    JSONObject jsonObject = new JSONObject(response);

                    JSONObject dataObject = jsonObject.getJSONObject("data");

                    JSONArray categoryArray = dataObject.getJSONArray("categories");
                    for (int cat =0; cat<categoryArray.length();cat++)
                    {
                        JSONObject catObject = categoryArray.getJSONObject(cat);
                        Shop_category shop_category = new Shop_category(catObject.getString("id"), catObject.getString("name"), catObject.getString("photo"));
                        listofCategory.add(shop_category);
                    }
                    stag_recycle_view.setAdapter(stagAdapter);
                    stagAdapter.notifyDataSetChanged();

                    JSONArray randonCatArray = dataObject.getJSONArray("random_category");
                    for (int ran =0 ;ran<randonCatArray.length();ran++)
                    {
                        JSONObject ranObject = randonCatArray.getJSONObject(ran);
                        RandomCategory category = new RandomCategory(ranObject.optString("sub_category_id"), ranObject.optString("categoryId"), ranObject.optString("photo"), ranObject.optString("name"));
                        listOfRandomData.add(category);

                    }
                    recyclerView.setAdapter(randomAdpater);
                    randomAdpater.notifyDataSetChanged();


                    JSONArray bannerArray = dataObject.getJSONArray("banner");
                    for (int ban =0;ban<bannerArray.length();ban++)
                    {
                        JSONObject banObject = bannerArray.getJSONObject(ban);
                        SliderNewModel sliderNewModel = new SliderNewModel(banObject.optString("id"),UrlPoint.banner_path+banObject.optString("banner_image"));
                       // lisofSlideImages.add(new SlideModel(UrlPoint.banner_path+banObject.optString("banner_image")));
                        //HomePagerModel hpg = new HomePagerModel(banObject.optString("id"),banObject.optString("banner_image"));
                        //listOfBanner.add(new RemoteBanner( UrlPoint.banner_path+ banObject.optString("banner_image")));
                       // listOfImages.add(hpg);

                        sliderItemList.add(sliderNewModel);

                    }

                    sliderViewAdapter.renewItems(sliderItemList);

                    //image_slider.setImageList(lisofSlideImages);
                   // view_pager.setAdapter(homePagerAdapter);
                    //homePagerAdapter.notifyDataSetChanged();
                    //slideShoew();

                     //bannerSlider.setBanners(HomeScreen.listOfBanner);


                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d("Home Exception", e.getMessage());
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                CustomLayout.setUIToWait(HomeScreen.this,false);
                Toast.makeText(HomeScreen.this, "Something went wrong", Toast.LENGTH_SHORT).show();

            }
        });

        RequestQueue requestQueue= Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        );

    }

    private void slideShoew() {


        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == Integer.MAX_VALUE)
                    currentPage = 0;
                view_pager.setCurrentItem(currentPage++, true);
            }
        };

        timer = new Timer(); // This will create a new Thread
        timer.schedule(new TimerTask() { // task to be scheduled
            @Override
            public void run() {
                handler.post(Update);
            }
        }, DELAY_MS, PERIOD_MS);

        createDotsShow(current_position++);


    }

    private void createDotsShow(int current_position)
    {
         if (dotsLayout.getChildCount() > 0)
          dotsLayout.removeAllViews();

         ImageView dots[] = new ImageView[listOfImages.size()];
         for (int i=0; i<listOfImages.size(); i++)
         {
              dots[i] = new ImageView(this);

              if (i == current_position)
              {
                dots[i].setImageDrawable(ContextCompat.getDrawable(this, R.drawable.active_dots));
              }
              else
              {
                  dots[i].setImageDrawable(ContextCompat.getDrawable(this, R.drawable.in_active_dots));
              }

              LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                      ViewGroup.LayoutParams.WRAP_CONTENT);

              layoutParams.setMargins(4,0,4,0);
              dotsLayout.addView(dots[i], layoutParams);
         }

    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
            finish();

        } else {
            super.onBackPressed();
            finish();
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.notification_icon) {

            Intent intent = new Intent(HomeScreen.this, Notification.class);
            startActivity(intent);
        }

        if (id == R.id.cart) {

            Intent intent = new Intent(HomeScreen.this, CartDetails.class);
            startActivity(intent);
        }

        if(id == R.id.search)
        {
            Intent intent = new Intent(HomeScreen.this, SerachActivity.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            // Handle the camera action
        } else if (id == R.id.nav_orders) {

            Intent intent = new Intent(HomeScreen.this, MyOrders.class);
            startActivity(intent);

        } else if (id == R.id.nav_about_us) {

            Uri uri = Uri.parse("http://store.pahadiconnect.com/about_us"); // missing 'http://' will cause crashed
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            startActivity(intent);

        } else if (id == R.id.nav_contact_us) {

            Intent intent = new Intent(HomeScreen.this, ContactUs.class);
            startActivity(intent);

        } else if(id == R.id.nav_terms) {

            Uri uri = Uri.parse("http://store.pahadiconnect.com/terms_condition"); // missing 'http://' will cause crashed
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            startActivity(intent);

        } else if(id == R.id.nav_logout) {

            confirmLagout();

        } else if (id == R.id.nav_login) {

            Intent intent = new Intent(HomeScreen.this, Login.class);
            startActivity(intent);
            finish();

        } else if (id==R.id.nave_refer)
        {
            Intent intent = new Intent(HomeScreen.this, ReferActivity.class);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return false;
    }



    private void initialization() {


        store_name = findViewById(R.id.store_name);
        bannerSlider = findViewById(R.id.banner_slider);
        stag_recycle_view = findViewById(R.id.stag_recycle_view);
        change_pincode = findViewById(R.id.change_pincode);

        providerId = findViewById(R.id.pincode);
        homeScreen = this;

        recyclerView = findViewById(R.id.random_sub_cat);

        home_cart = findViewById(R.id.home_cart);
        home_notification = findViewById(R.id.home_notification);
        home_search = findViewById(R.id.home_search);

        cartNotificationCount = findViewById(R.id.tvCartNotifBadge);
        reconnect_button = findViewById(R.id.reconnect_button);

        connection_layout = findViewById(R.id.connection_layout);
        no_connection_layout = findViewById(R.id.no_connection_layout);

        view_pager = findViewById(R.id.view_pager);
        dotsLayout = findViewById(R.id.dot_contatiner);
        sliderView = findViewById(R.id.imageSlider);


    }


    private void confirmLagout() {

        LayoutInflater layoutInflater =LayoutInflater.from(this);
        @SuppressLint("InflateParams") final View showCancelDialog = layoutInflater.inflate(R.layout.confirm_logout,null);

        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(showCancelDialog);


        final AlertDialog alertDialog = builder.create();
        alertDialog.show();

        button_yes = showCancelDialog.findViewById(R.id.button_yes);
        button_no = showCancelDialog.findViewById(R.id.button_no);

        button_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                alertDialog.dismiss();
            }
        });

        button_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                performLogout();

            }
        });

    }

    private void performLogout() {

        SharedPreferences sharedPreferences = getSharedPreferences("MyData", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor1 = preferences.edit().clear();

        editor.remove("userId");
        editor.remove("email");
        editor.remove("p_code");

        editor.remove("login_name");
        editor.remove("login_email");
        editor.remove("login_phone");
        editor.remove("shopName");


        editor.remove("customer_name");
        editor.remove("login_address");
        editor.remove("login_street");
        editor.remove("login_state");
        editor.remove("login_city");
        editor.remove("login_zip");
        editor.remove("login_phone");
        editor.remove("wallet_balance");
        editor.remove("store_name");
        editor.remove("f_name");
        editor.remove("l_name");


        editor1.putBoolean("first", true);

        editor.apply();
        editor1.apply();

        databaseHelper.Delete(this);

        Intent intent = new Intent(HomeScreen.this, Login.class);
        intent.putExtra("type","1");
        startActivity(intent);
        finish();

    }


    private void popUpConfirmation() {


        LayoutInflater layoutInflater =LayoutInflater.from(this);
        @SuppressLint("InflateParams") final View showCancelDialog = layoutInflater.inflate(R.layout.change_pincode,null);

        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(showCancelDialog);


        final AlertDialog alertDialog = builder.create();
        alertDialog.show();

        button_change = showCancelDialog.findViewById(R.id.button_change);
        button_dismiss = showCancelDialog.findViewById(R.id.button_dismiss);

        button_dismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                alertDialog.dismiss();
            }
        });

        button_change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                performOperation();

            }
        });
    }

    private void performOperation() {

        databaseHelper.Delete(HomeScreen.this);
        Intent intent = new Intent(HomeScreen.this, VerifyPincode.class);
        intent.putExtra("pin", true);
        startActivity(intent);
        finish();
    }


    @Override
    protected void onResume() {
        super.onResume();

        listOfCartItems = databaseHelper.getAllCartItems();
        listsize = String.valueOf(listOfCartItems.size());

        if (listsize.contains("0"))
        {
            cartNotificationCount.setVisibility(View.GONE);
        }
        else
        {
            cartNotificationCount.setVisibility(View.VISIBLE);
            cartNotificationCount.setText(" "+ listsize);

        }

    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(LocalHelp.onAttach(newBase));
    }

    private void checkForUpdate() {

        appUpdateInfoTask.addOnSuccessListener(appUpdateInfo -> {
            if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE
                    // For a flexible update, use AppUpdateType.FLEXIBLE
                    && appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.FLEXIBLE)) {
                // Request the update.

                try {
                    appUpdateManager.startUpdateFlowForResult(
                            // Pass the intent that is returned by 'getAppUpdateInfo()'.
                            appUpdateInfo,
                            // Or 'AppUpdateType.FLEXIBLE' for flexible updates.
                            AppUpdateType.FLEXIBLE,
                            // The current activity making the update request.
                            this,
                            // Include a request code to later monitor this update request.
                            MY_REQUEST_CODE);
                } catch (IntentSender.SendIntentException e) {
                    e.printStackTrace();
                }
            }else if (appUpdateInfo.installStatus() == InstallStatus.DOWNLOADED){
                popupSnackbarForCompleteUpdate();
            } else {
                Log.e("TAG111", "checkForAppUpdateAvailability: something else");
            }
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == MY_REQUEST_CODE) {
            if (resultCode != RESULT_OK) {
                Log.e("TAG111", "Update flow failed! Result code: " + resultCode);
                // If the update is cancelled or fails,
                // you can request to start the update again.

            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (appUpdateManager != null) {
            appUpdateManager.unregisterListener(installStateUpdatedListener);
        }
    }

    InstallStateUpdatedListener installStateUpdatedListener = new InstallStateUpdatedListener() {
        @Override
        public void onStateUpdate(InstallState state) {
            if (state.installStatus() == InstallStatus.DOWNLOADED){
                popupSnackbarForCompleteUpdate();
            } else if (state.installStatus() == InstallStatus.INSTALLED){
                if (appUpdateManager != null){
                    appUpdateManager.unregisterListener(installStateUpdatedListener);
                }

            } else {
                Log.i("TAG111", "InstallStateUpdatedListener: state: " + state.installStatus());
            }
        }
    };


    private void popupSnackbarForCompleteUpdate() {
        Snackbar snackbar =
                Snackbar.make(
                        findViewById(R.id.home_layout),
                        "New app is ready!",
                        Snackbar.LENGTH_INDEFINITE);

        snackbar.setAction("Install", view -> {
            if (appUpdateManager != null){
                appUpdateManager.completeUpdate();
            }
        });


        snackbar.setActionTextColor(getResources().getColor(R.color.blue_700));
        snackbar.show();
    }



}



