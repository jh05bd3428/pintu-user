package com.knackspin.pintu;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.knackspin.pintu.Adapter.ServicePincodeAdapter;
import com.knackspin.pintu.Model.ServicePincodeModel;
import com.knackspin.pintu.Singleton.Singleton;
import com.knackspin.pintu.utils.UrlPoint;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ServisablePincode extends AppCompatActivity {

    RecyclerView serviseable_rv;
    ServicePincodeAdapter servicePincodeAdapter;
    private List<ServicePincodeModel> listOfServicePincode;
    LinearLayoutManager linearLayoutManager;

    ImageView iv_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_pincode);

        listOfServicePincode = new ArrayList<>();
        serviseable_rv = findViewById(R.id.serviseable_rv);
        iv_back = findViewById(R.id.iv_back);
        servicePincodeAdapter = new ServicePincodeAdapter(listOfServicePincode, this);

        serviseable_rv.setHasFixedSize(true);
        linearLayoutManager = new LinearLayoutManager(this);
        serviseable_rv.setLayoutManager(linearLayoutManager);
        serviseable_rv.setAdapter(servicePincodeAdapter);

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();
            }
        });


        getServiceDetails();

    }

    private void getServiceDetails()
    {
        CustomLayout.setUIToWait(ServisablePincode.this, true);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, UrlPoint.getServisablePincode, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                CustomLayout.setUIToWait(ServisablePincode.this, false);

                if (!listOfServicePincode.isEmpty())
                {
                    listOfServicePincode.clear();
                }

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    if (jsonObject.optString("flag").equalsIgnoreCase("true"))
                    {
                        JSONArray dataArray = jsonObject.getJSONArray("data");
                        for (int q=0; q<dataArray.length(); q++)
                        {
                            JSONObject ob = dataArray.getJSONObject(q);
                            ServicePincodeModel spm = new ServicePincodeModel(ob.optString("id"),ob.optString("photo"),
                                    ob.optString("shop_name"), ob.optString("street_address")
                                    ,ob.optString("pin_code"),ob.optString("delivery_charge"),
                                    ob.optString("minimum_amout"));

                            listOfServicePincode.add(spm);
                        }

                        serviseable_rv.setAdapter(servicePincodeAdapter);
                        servicePincodeAdapter.notifyDataSetChanged();
                    }

                    else
                        {
                            Toast.makeText(ServisablePincode.this, "Nothing found", Toast.LENGTH_SHORT).show();
                        }


                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                CustomLayout.setUIToWait(ServisablePincode.this, false);

                Toast.makeText(ServisablePincode.this, "Something went wrong", Toast.LENGTH_SHORT).show();

            }
        });


        Singleton.getInstance(ServisablePincode.this).addTorequestqu(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        );

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
    }
}