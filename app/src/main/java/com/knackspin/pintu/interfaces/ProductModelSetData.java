package com.knackspin.pintu.interfaces;

import java.io.Serializable;

public interface ProductModelSetData extends Serializable {

    void setProductModel(int pos, int productPos);
}
