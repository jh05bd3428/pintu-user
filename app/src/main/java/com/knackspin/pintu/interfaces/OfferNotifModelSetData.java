package com.knackspin.pintu.interfaces;

import java.io.Serializable;

public interface OfferNotifModelSetData  extends Serializable {

    public void setOfferModel(int pos, int productPos);

}
