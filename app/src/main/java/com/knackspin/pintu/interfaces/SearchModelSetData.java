package com.knackspin.pintu.interfaces;

import java.io.Serializable;

public interface SearchModelSetData extends Serializable {

    public void setSearchModel(int pos, int productPos);
}
