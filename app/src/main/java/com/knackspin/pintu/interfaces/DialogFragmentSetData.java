package com.knackspin.pintu.interfaces;

public interface DialogFragmentSetData {
     void setData(int pos, int productPos);
}
