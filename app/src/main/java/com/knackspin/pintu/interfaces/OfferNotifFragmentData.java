package com.knackspin.pintu.interfaces;

public interface OfferNotifFragmentData {

    void setData(int pos, int productPos);

}
