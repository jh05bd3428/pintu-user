package com.knackspin.pintu;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.speech.RecognizerIntent;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.knackspin.pintu.Adapter.RandomSearchAdapter;
import com.knackspin.pintu.Adapter.SearchAdapter;
import com.knackspin.pintu.Database.DatabaseHelper;
import com.knackspin.pintu.Model.Cart_model;
import com.knackspin.pintu.Model.RandomSearchModel;
import com.knackspin.pintu.Model.SearchDialogModel;
import com.knackspin.pintu.Model.SearchModel;
import com.knackspin.pintu.Singleton.Singleton;
import com.knackspin.pintu.utils.UrlPoint;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class SerachActivity extends AppCompatActivity {


    private List<SearchModel> listOfSearchItem;
    RecyclerView recyclerView,suggestion_rv;
    SearchAdapter searchAdapter;
    RandomSearchAdapter randomSearchAdapter;

    private  List<SearchDialogModel> listofSearchDialog;
    private RecyclerView.LayoutManager layoutManager, randomManager;
    public ArrayList<Cart_model> listOfCartItems;
    public ArrayList<RandomSearchModel> listOfRandomSearch;


    DatabaseHelper databaseHelper;
    String listSize;
    TextView tvCartSearchBadge;

    String providerID;
    public static final String DEFAULT = "NA";

    EditText etProductSearch,etSearchProduct;
    Button search,search_voice;
    ImageView serach_cart_button,search_back,iv_random_search;

    public  static final int SPEECH_VOICE = 199;

    SharedPreferences sharedPreferences;

    private boolean callApi = false;
    LinearLayout ll_random_search,ll_search_layout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_serach);

        initialization();

        sharedPreferences = getSharedPreferences("MyData", Context.MODE_PRIVATE);
        providerID = sharedPreferences.getString("store_id",DEFAULT);


        listOfSearchItem = new ArrayList<>();
        listOfCartItems = new ArrayList<>();
        listofSearchDialog = new ArrayList<>();
        listOfRandomSearch  = new ArrayList<>();

        recyclerView = findViewById(R.id.serach_recylerview);
        suggestion_rv = findViewById(R.id.suggestion_rv);

        searchAdapter = new SearchAdapter(listOfSearchItem,this,SerachActivity.this);
        randomSearchAdapter = new RandomSearchAdapter(listOfRandomSearch, this);

        recyclerView.setHasFixedSize(true);
        suggestion_rv.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(this);
        randomManager = new LinearLayoutManager(this);

        recyclerView.setLayoutManager(layoutManager);
        suggestion_rv.setLayoutManager(randomManager);

        recyclerView.setAdapter(searchAdapter);
        suggestion_rv.setAdapter(randomSearchAdapter);




        iv_random_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                speak();
            }
        });


        serach_cart_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(SerachActivity.this, CartDetails.class);
                startActivity(intent);
            }
        });

        search_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });

        etSearchProduct.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {


            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {



            }

            @Override
            public void afterTextChanged(Editable editable) {

                if(etSearchProduct.getText().toString().length() > 0)
                {
                    ll_search_layout.setVisibility(View.VISIBLE);
                    ll_random_search.setVisibility(View.GONE);
                    getSearchData();

                }

                else
                    {
                        ll_search_layout.setVisibility(View.GONE);
                        ll_random_search.setVisibility(View.VISIBLE);
                        getRandomData();

                        Toast.makeText(SerachActivity.this, "Nothing to show", Toast.LENGTH_SHORT).show();
                    }



            }
        });

        getRandomData();


    }




    private void initialization() {

        etProductSearch = findViewById(R.id.etProductSearch);
        search = findViewById(R.id.search_click);
        serach_cart_button = findViewById(R.id.search_cart_button);

        tvCartSearchBadge = findViewById(R.id.tvCartSearchBadge);
        search_voice = findViewById(R.id.search_voice);
        search_back = findViewById(R.id.search_back);
        etSearchProduct = findViewById(R.id.etSearchProduct);

        databaseHelper = new DatabaseHelper(this);
        suggestion_rv = findViewById(R.id.suggestion_rv);
        iv_random_search = findViewById(R.id.iv_random_search);

        ll_random_search = findViewById(R.id.ll_random_search);
        ll_search_layout = findViewById(R.id.ll_search_layout);

    }
    private void speak() {

        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT,"Search Product");


        try {

            startActivityForResult(intent, SPEECH_VOICE);

        } catch (Exception e )
        {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode)
        {
            case SPEECH_VOICE:
                {
                    if (resultCode == RESULT_OK && null!=data)
                    {
                        ArrayList<String> searchlist = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                        etSearchProduct.setText(searchlist.get(0));
                        search.performClick();
                    }

                    break;
                }
        }
    }

    private void getSearchData() {


        CustomLayout.setUIToWait(SerachActivity.this,true);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, UrlPoint.getSearchProduct, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                CustomLayout.setUIToWait(SerachActivity.this,false);

                JSONObject jsonObject = null;


                if (!listOfSearchItem.isEmpty())
                {
                    listOfSearchItem.removeAll(listOfSearchItem);

                }

                try {
                    jsonObject = new JSONObject(response);

                    if(jsonObject.optString("status").equalsIgnoreCase("found"))
                    {

                        JSONArray jsonArray = jsonObject.getJSONArray("details");
                        for (int a=0; a<jsonArray.length();a++)
                        {
                            if (jsonArray.length() > 0)
                            {
                                JSONObject object = jsonArray.getJSONObject(a);
                                JSONArray productArray = object.getJSONArray("product");

                                for(int b=0; b< productArray.length();b++)
                                {
                                    JSONObject object1 = productArray.getJSONObject(b);
                                    ArrayList<SearchDialogModel> listofdialog = new ArrayList<>();

                                    JSONArray subproduct = object1.getJSONArray("product_variant");

                                    for (int c=0; c<subproduct.length();c++)
                                    {
                                        JSONObject object2 = subproduct.getJSONObject(c);
                                        listofdialog.add(new SearchDialogModel(object2.optString("id"),object2.optString("variantproduct_id"),object2.optString("variant_name"), object2.optString("mrp_price"), object2.optString("variant_price")));
                                    }

                                    SearchModel sm = new SearchModel(object1.optString("id"),object1.optString("brand_name"), object1.optString("name"), object1.optString("photo_1"),listofdialog);
                                    listOfSearchItem.add(sm);
                                }
                            }
                            else
                            {
                                Toast.makeText(SerachActivity.this, "Products Not Available", Toast.LENGTH_SHORT).show();
                            }

                        }

                        recyclerView.setAdapter(searchAdapter);
                        searchAdapter.notifyDataSetChanged();

                    }
                    else
                    {
                        Toast.makeText(SerachActivity.this, "Products Not Available", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                CustomLayout.setUIToWait(SerachActivity.this,false);
                Toast.makeText(SerachActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        })

        {
            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<String, String>();
                params.put("product_name", etSearchProduct.getText().toString());
                params.put("provider_id",sharedPreferences.getString("store_id",DEFAULT));
                return params;
            }

        };

        Singleton.getInstance(SerachActivity.this).addTorequestqu(stringRequest);

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        );

    }

    private void getRandomData() {
        CustomLayout.setUIToWait(SerachActivity.this,true);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, UrlPoint.homePageApi, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                CustomLayout.setUIToWait(SerachActivity.this,false);

                if (!listOfRandomSearch.isEmpty())
                {
                    listOfRandomSearch.clear();
                }

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    JSONObject dataObject = jsonObject.getJSONObject("data");
                    JSONArray randonCatArray = dataObject.getJSONArray("random_category");
                    for (int ran =0 ;ran<randonCatArray.length();ran++)
                    {
                        JSONObject ranObject = randonCatArray.getJSONObject(ran);
                        RandomSearchModel category = new RandomSearchModel(ranObject.optString("sub_category_id"), ranObject.optString("categoryId"), ranObject.optString("name"), ranObject.optString("photo"));
                        listOfRandomSearch.add(category);

                    }
                    suggestion_rv.setAdapter(randomSearchAdapter);
                    randomSearchAdapter.notifyDataSetChanged();

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                CustomLayout.setUIToWait(SerachActivity.this,true);

                Toast.makeText(SerachActivity.this, "Something went wrong!", Toast.LENGTH_SHORT).show();

            }
        });

        Singleton.getInstance(SerachActivity.this).addTorequestqu(stringRequest);

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        );


    }


    @Override
    protected void onStart() {
        super.onStart();

        listOfCartItems = databaseHelper.getAllCartItems();
        listSize = String.valueOf(listOfCartItems.size());
        tvCartSearchBadge.setText(" "+ listSize);
        updateCartCount();

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public void updateCartCount(){

        listOfCartItems = databaseHelper.getAllCartItems();
        listSize = String.valueOf(listOfCartItems.size());

        if (listSize.contains("0"))
        {
            tvCartSearchBadge.setVisibility(View.GONE);
        }
        else
        {
            tvCartSearchBadge.setVisibility(View.VISIBLE);
            tvCartSearchBadge.setText(" "+ listSize);

        }

    }

}


// ryes-249612