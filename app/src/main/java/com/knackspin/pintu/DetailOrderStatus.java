package com.knackspin.pintu;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Handler;
import android.os.Looper;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.knackspin.pintu.Adapter.PlacedOrderAdapter;
import com.knackspin.pintu.Model.PlacedOrderModel;
import com.knackspin.pintu.Singleton.Singleton;
import com.knackspin.pintu.utils.ConnectionCheck;
import com.knackspin.pintu.utils.ProgressBarAnimation;
import com.knackspin.pintu.utils.UrlPoint;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class DetailOrderStatus extends AppCompatActivity {

       TextView order_id,order_status;
       int nStatus = 0;
       String u_id;
       Timer timer;
       float rate;
       public static final String DEFAULT = "NA";
       ProgressBar progressBar;
       ImageView status1,status2,status3,status4,status5,histroy_detail_back,cancel_button;
       ImageView ivSt1,ivSt2,ivSt3,ivSt4,ivSt5;
       TextView  tvSt1,tvSt2,tvSt3,tvSt4,tvSt5;
       TextView od_customer_name,od_customer_address,od_customer_city,od_customer_pincode,od_customer_number,tv_shop_name;

       TextView his_final_amount, his_delivery_amount, his_grand_total, user_rating,rating_number,dismiss_button,dialog_cancel_button;
       Button submit_rating_button;

       RatingBar ratingBar;
       EditText rating_text,cancellation_text;



       private RecyclerView recyclerView;
       private ArrayList<PlacedOrderModel> listOfPlacedOrder;
       private PlacedOrderAdapter placedOrderAdapter;
       LinearLayoutManager linearLayoutManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_order_status);

        initialization();

        listOfPlacedOrder = new ArrayList<>();
        recyclerView.setHasFixedSize(true);
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        placedOrderAdapter = new PlacedOrderAdapter(listOfPlacedOrder, this);
        recyclerView.setAdapter(placedOrderAdapter);


        timer = new Timer();
        SharedPreferences sharedPreferences = getSharedPreferences("MyData", Context.MODE_PRIVATE);
        u_id = sharedPreferences.getString("userId", DEFAULT);


        if(getIntent()!=null)
        {

            String id =  getIntent().getStringExtra("orderId");
            order_id.setText(id);
            String o_status = getIntent().getStringExtra("orderStatus");

            if (o_status.equalsIgnoreCase("placed"))
            {
                order_status.setText("PENDING");
            }

            else if (o_status.equalsIgnoreCase("Processing"))
            {
                order_status.setText("CONFIRMED");
            }

            else if (o_status.equalsIgnoreCase("Billed"))
            {
                order_status.setText("CONFIRMED");
            }
            else if (o_status.equalsIgnoreCase("ontheway"))
            {
                order_status.setText("SHIPPED");

            }

            else if (o_status.equalsIgnoreCase("delivered"))
            {
                order_status.setText("DELIVERED");

            }

            else if (o_status.equalsIgnoreCase("cancelled"))
            {
                order_status.setText("CANCELLED");

            }

            else if (o_status.equalsIgnoreCase("Door Closed"))
            {
                order_status.setText("CANCELLED");

            }


        }

        histroy_detail_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });

        checkConnection();


        cancel_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                confirmCancellation();
            }
        });
    }


    private void checkConnection() {

            if (new ConnectionCheck(this).isNetworkAvailable())
            {
                      getStatus();
            } else
                {
                    Toast.makeText(this, "Check Internet Connection", Toast.LENGTH_SHORT).show();
                }
    }


    private void getStatus() {

        CustomLayout.setUIToWait(DetailOrderStatus.this, true);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, UrlPoint.statusUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                CustomLayout.setUIToWait(DetailOrderStatus.this, false);

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    if (jsonObject.optString("status").equalsIgnoreCase("found"))
                    {

                        JSONArray detailsArray = jsonObject.getJSONArray("order_details");
                        for (int l= 0; l < detailsArray.length(); l++)
                        {
                            JSONObject ob = detailsArray.getJSONObject(l);
                            his_final_amount.setText("\u20B9"+" "+ob.optString("sub_total"));
                            his_grand_total.setText("\u20B9"+" "+ob.optString("grand_total"));
                            his_delivery_amount.setText("\u20B9"+" "+ob.optString("shipping_cost"));

                            od_customer_name.setText(ob.optString("first_name")+ " "+ ob.optString("last_name"));
                            od_customer_address.setText(ob.optString("street_address"));
                            od_customer_city.setText(ob.optString("city"));
                            od_customer_pincode.setText(ob.optString("pin_code"));
                            od_customer_number.setText(ob.optString("phone"));
                            tv_shop_name.setText("Ordered from"+" "+ob.optString("shop_name").toUpperCase());

                            String status = ob.optString("status");


                            if(status.equalsIgnoreCase("placed"))
                            {
                                cancel_button.setVisibility(View.VISIBLE);
                                nStatus = 1;
                            }

                            else if(status.equalsIgnoreCase("Processing"))
                            {
                                nStatus = 2;
                                cancel_button.setVisibility(View.VISIBLE);

                            }

                            else if (status.equalsIgnoreCase("Billed"))
                            {
                                nStatus = 3;
                                cancel_button.setVisibility(View.VISIBLE);

                            }

                            else  if (status.equalsIgnoreCase("ontheway"))
                            {
                                nStatus = 4;
                                cancel_button.setVisibility(View.GONE);

                            }

                            else if (status.equalsIgnoreCase("delivered"))
                            {
                                nStatus = 5;
                                cancel_button.setVisibility(View.GONE);

                                if (user_rating.getText().toString().isEmpty())
                                {
                                    showRatingPopup();

                                } else
                                {
                                    Log.d("Feedback", "Feedback Given");
                                }

                            }

                            else if(status.equalsIgnoreCase("Denied"))
                            {
                                nStatus = 6;
                                cancel_button.setVisibility(View.GONE);
                            }

                            else if(status.equalsIgnoreCase("Door Closed"))
                            {
                                nStatus = 7;
                                cancel_button.setVisibility(View.GONE);
                            }

                            else if(status.equalsIgnoreCase("cancelled"))
                            {
                                nStatus = 8;
                                cancel_button.setVisibility(View.GONE);
                            }

                            else
                            {
                                nStatus = 0;
                            }

                            JSONArray productArray = ob.getJSONArray("products");
                            for (int w= 0; w<productArray.length(); w++)
                            {
                                JSONObject ob1 = productArray.getJSONObject(w);

                                PlacedOrderModel pcm = new PlacedOrderModel(ob1.optString("brand_name"),
                                        ob1.optString("product_name"), ob1.optString("veriant_name"),
                                        ob1.optString("quantity"), ob1.optString("product_price"),
                                        ob1.optString("photo_1"));

                                listOfPlacedOrder.add(pcm);

                            }

                            recyclerView.setAdapter(placedOrderAdapter);
                        }

                    } else
                        {
                            Toast.makeText(DetailOrderStatus.this, "No product found", Toast.LENGTH_SHORT).show();
                        }

                    setStatus(nStatus);

                } catch (JSONException e) {
                    e.printStackTrace();
                }



            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                CustomLayout.setUIToWait(DetailOrderStatus.this, true);


                Toast.makeText(DetailOrderStatus.this, "Something went wrong", Toast.LENGTH_SHORT).show();

            }
        })
        {
            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<String, String>();
                params.put("order_id",order_id.getText().toString());
                return params;
            }
        };

        Singleton.getInstance(DetailOrderStatus.this).addTorequestqu(stringRequest);

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        );

    }


    private void setStatus(int nStatus) {

        switch (nStatus)
        {
            case 1:
                ProgressBarAnimation animation = new ProgressBarAnimation(progressBar,0,20);
                animation.setDuration(2000);
                progressBar.startAnimation(animation);
                status1.setImageResource(R.drawable.ic_dot_green);
                status2.setImageResource(R.drawable.ic_dot_gray);
                status3.setImageResource(R.drawable.ic_dot_gray);
                status4.setImageResource(R.drawable.ic_dot_gray);
                status5.setImageResource(R.drawable.ic_dot_gray);

             break;

            case 2:
                ProgressBarAnimation animation3 = new ProgressBarAnimation(progressBar, 0 ,40);
                animation3.setDuration(2000);
                progressBar.startAnimation(animation3);
                updateProgress();
                break;

            case 3:

                ProgressBarAnimation animation4 = new ProgressBarAnimation(progressBar, 0 ,60);
                animation4.setDuration(2000);
                progressBar.startAnimation(animation4);
                updateProgress();
                break;

            case 4:

                ProgressBarAnimation animation5 = new ProgressBarAnimation(progressBar, 0 ,80);
                animation5.setDuration(2000);
                progressBar.startAnimation(animation5);
                updateProgress();
                break;

            case 5:

                ProgressBarAnimation animation6 = new ProgressBarAnimation(progressBar, 0 ,100);
                animation6.setDuration(2000);
                progressBar.startAnimation(animation6);
                updateProgress();
                break;

            case 6:

                ProgressBarAnimation animation7 = new ProgressBarAnimation(progressBar, 0 ,100);
                animation7.setDuration(2000);
                progressBar.startAnimation(animation7);

                ivSt2.setImageDrawable(null);
                status2.setImageDrawable(null);
                tvSt2.setText("");

                ivSt3.setImageDrawable(null);
                status3.setImageDrawable(null);
                tvSt3.setText("");

                ivSt4.setImageDrawable(null);
                status4.setImageDrawable(null);
                tvSt4.setText("");

                ivSt5.setImageResource(R.drawable.ic_rejected);
                tvSt5.setText("Rejected");
                status5.setImageResource(R.drawable.red_dot);

                break;


            case 7:

                ProgressBarAnimation animation8 = new ProgressBarAnimation(progressBar, 0 ,100);
                animation8.setDuration(2000);
                progressBar.startAnimation(animation8);

                ivSt2.setImageDrawable(null);
                status2.setImageDrawable(null);
                tvSt2.setText("");

                ivSt3.setImageDrawable(null);
                status3.setImageDrawable(null);
                tvSt3.setText("");

                ivSt4.setImageDrawable(null);
                status4.setImageDrawable(null);
                tvSt4.setText("");

                ivSt5.setImageResource(R.drawable.ic_rejected);
                tvSt5.setText("Cancelled");
                status5.setImageResource(R.drawable.red_dot);
                cancel_button.setVisibility(View.GONE);
                order_status.setBackgroundColor(Color.RED);


                break;

            case 8:

                ProgressBarAnimation animation9 = new ProgressBarAnimation(progressBar, 0 ,100);
                animation9.setDuration(2000);
                progressBar.startAnimation(animation9);

                ivSt2.setImageDrawable(null);
                status2.setImageDrawable(null);
                tvSt2.setText("");

                ivSt3.setImageDrawable(null);
                status3.setImageDrawable(null);
                tvSt3.setText("");

                ivSt4.setImageDrawable(null);
                status4.setImageDrawable(null);
                tvSt4.setText("");

                ivSt5.setImageResource(R.drawable.ic_rejected);
                tvSt5.setText("Cancelled");
                status5.setImageResource(R.drawable.red_dot);
                cancel_button.setVisibility(View.GONE);
                order_status.setBackgroundColor(Color.RED);


                break;
        }

    }

    public void updateProgress() {

        timer.schedule(new TimerTask() {
            @Override
            public void run() {

                int nProgress = progressBar.getProgress();
                switch (nProgress)
                {
                    case 10:
                        updateUI(1);
                        break;

                    case 40:
                        updateUI(2);
                        break;

                    case 60:
                        updateUI(3);
                        break;

                    case 80:
                        updateUI(4);
                        break;

                    case 100:
                        updateUI(5);
                        break;
                }

            }
        },2,5);

    }

    private void updateUI(final int i) {

        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {

                if (i == 1) {

                   // Toast.makeText(DetailOrderStatus.this, i, Toast.LENGTH_SHORT).show();
                    status1.setImageResource(R.drawable.ic_dot_green);
                    status2.setImageResource(R.drawable.ic_dot_green);
                    status3.setImageResource(R.drawable.ic_dot_gray);
                    status4.setImageResource(R.drawable.ic_dot_gray);
                    status5.setImageResource(R.drawable.ic_dot_gray);
                    order_status.setText("Confirmed");
                  //  order_status.setBackgroundColor(getResources().getColor(R.color.black));
                    order_status.setBackgroundColor(getResources().getColor(R.color.blue_700));

                }

                else if (i == 3) {

                    status1.setImageResource(R.drawable.ic_dot_green);
                    status2.setImageResource(R.drawable.ic_dot_green);
                    status3.setImageResource(R.drawable.ic_dot_green);
                    status4.setImageResource(R.drawable.ic_dot_gray);
                    status5.setImageResource(R.drawable.ic_dot_gray);
                    order_status.setText("Billed");
                    order_status.setBackgroundColor(getResources().getColor(R.color.dot_dark_screen4));


                }

                else if (i == 4) {

                    status1.setImageResource(R.drawable.ic_dot_green);
                    status2.setImageResource(R.drawable.ic_dot_green);
                    status3.setImageResource(R.drawable.ic_dot_green);
                    status4.setImageResource(R.drawable.ic_dot_green);
                    status5.setImageResource(R.drawable.ic_dot_gray);
                    order_status.setText("Shipped");
                    order_status.setBackgroundColor(getResources().getColor(R.color.dot_light_screen1));


                }

                else if  (i == 5) {

                    status1.setImageResource(R.drawable.ic_dot_green);
                    status2.setImageResource(R.drawable.ic_dot_green);
                    status3.setImageResource(R.drawable.ic_dot_green);
                    status4.setImageResource(R.drawable.ic_dot_green);
                    status5.setImageResource(R.drawable.ic_dot_green);
                    order_status.setText("Delivered");
                    order_status.setBackgroundColor(getResources().getColor(R.color.new_orange));


                }
            }
        });
    }

    private void showRatingPopup() {

        LayoutInflater layoutInflater =LayoutInflater.from(this);
        @SuppressLint("InflateParams")
        View showDialog = layoutInflater.inflate(R.layout.rating_bar,null);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(showDialog);

        final AlertDialog alertDialog = builder.create();
        alertDialog.show();

        ratingBar = showDialog.findViewById(R.id.rating);
        rating_text = showDialog.findViewById(R.id.rating_text);
        submit_rating_button = showDialog.findViewById(R.id.submit_rating_button);
        rating_number = showDialog.findViewById(R.id.rating_number);

        submit_rating_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                rate  = ratingBar.getRating();
                Log.d("Rating", String.valueOf(rate));

                alertDialog.dismiss();

                sendFeedback();

            }
        });

    }

    private void sendFeedback() {

                CustomLayout.setUIToWait(DetailOrderStatus.this,true);
                StringRequest stringRequest = new StringRequest(Request.Method.POST, UrlPoint.feedback, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        CustomLayout.setUIToWait(DetailOrderStatus.this,false);

                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            if (jsonObject.optString("status").equalsIgnoreCase("success"))
                            {
                                Toast.makeText(DetailOrderStatus.this, "Feedback Submitted", Toast.LENGTH_SHORT).show();
                            }else
                                {
                                    Toast.makeText(DetailOrderStatus.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                                }


                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.i("FeedbackException", e.getMessage());
                        }


                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        CustomLayout.setUIToWait(DetailOrderStatus.this,false);

                        Toast.makeText(DetailOrderStatus.this, "Something went wrong", Toast.LENGTH_SHORT).show();

                    }
                })

                {
                    @Override
                    protected Map<String, String> getParams() {

                        Map<String, String> params = new HashMap<String, String>();
                        params.put("order_id",order_id.getText().toString());
                        params.put("rating", String.valueOf(rate));
                        params.put("message",rating_text.getText().toString());

                        return params;
                    }
                };

             Singleton.getInstance(DetailOrderStatus.this).addTorequestqu(stringRequest);

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        );

    }

    private void confirmCancellation() {

        LayoutInflater layoutInflater =LayoutInflater.from(this);
        @SuppressLint("InflateParams") final View showCancelDialog = layoutInflater.inflate(R.layout.custom_order_cancellation,null);

        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(showCancelDialog);

        final AlertDialog alertDialog = builder.create();
        alertDialog.show();

        cancellation_text = showCancelDialog.findViewById(R.id.cancellation_text);
        dismiss_button = showCancelDialog.findViewById(R.id.dismiss_button);
        dialog_cancel_button = showCancelDialog.findViewById(R.id.order_cancel_button);

        dismiss_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                alertDialog.dismiss();
            }
        });

        dialog_cancel_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                alertDialog.dismiss();
                orderCancel();
            }
        });
    }

    private void orderCancel() {

        CustomLayout.setUIToWait(DetailOrderStatus.this, true);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, UrlPoint.cancelOrder, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                CustomLayout.setUIToWait(DetailOrderStatus.this, false);

                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(response);

                    if (jsonObject.optString("status").equalsIgnoreCase("cancel"))
                    {
                        Toast.makeText(DetailOrderStatus.this, "Order has been cancelled", Toast.LENGTH_SHORT).show();
                        cancel_button.setVisibility(View.GONE);
                        order_status.setText("Cancelled");

                    } else
                    {
                        Toast.makeText(DetailOrderStatus.this, "Unable to cancel order", Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d("CancelException", e.getMessage());
                }

            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                CustomLayout.setUIToWait(DetailOrderStatus.this, false);

                Toast.makeText(DetailOrderStatus.this, "Something went wrong", Toast.LENGTH_SHORT).show();

            }
        })

        {
            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<String, String>();
                params.put("order_id",order_id.getText().toString());
                params.put("remark", cancellation_text.getText().toString());
                return params;
            }

        };

        Singleton.getInstance(DetailOrderStatus.this).addTorequestqu(stringRequest);

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        );

    }

    private void initialization() {

        order_id = findViewById(R.id.orderField);
        order_status = findViewById(R.id.orderStatus);
        progressBar = findViewById(R.id.progressStatus);

        status1 = findViewById(R.id.ivProgress1);
        status2 = findViewById(R.id.ivProgress2);
        status3 = findViewById(R.id.ivProgress3);
        status4 = findViewById(R.id.ivProgress4);
        status5 = findViewById(R.id.ivProgress5);

        ivSt1 = findViewById(R.id.ivPlaced);
        ivSt2 = findViewById(R.id.ivConfirmed);
        ivSt3 = findViewById(R.id.ivBilled);
        ivSt4 = findViewById(R.id.ivPicked);
        ivSt5 = findViewById(R.id.ivDelivered);

        tvSt1 = findViewById(R.id.textPlaced);
        tvSt2 = findViewById(R.id.textConfirmed);
        tvSt3 = findViewById(R.id.textBilled);
        tvSt4 = findViewById(R.id.textPicked);
        tvSt5 = findViewById(R.id.textDelivered);

        histroy_detail_back = findViewById(R.id.histroy_detail_back);

        his_final_amount = findViewById(R.id.his_final_amount);
        his_delivery_amount = findViewById(R.id.his_delivery_amount);
        his_grand_total = findViewById(R.id.his_grand_total);

        recyclerView = findViewById(R.id.orderred_item_recycle);
        cancel_button = findViewById(R.id.cancel_button);

        user_rating = findViewById(R.id.user_rating);

        od_customer_name = findViewById(R.id.od_customer_name);
        od_customer_address = findViewById(R.id.od_customer_address);
        od_customer_city = findViewById(R.id.od_customer_city);
        od_customer_pincode = findViewById(R.id.od_customer_pincode);
        od_customer_number = findViewById(R.id.od_customer_number);
        tv_shop_name = findViewById(R.id.tv_shop_name);


        //od_customer_address,od_customer_city,od_customer_pincode,od_customer_number

    }
}
/*
1. placed
2.Processing
3.Billed
4.On The Way
5.delivered
 5.1 Denied
 5.2 Door Closed
        */
