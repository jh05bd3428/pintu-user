package com.knackspin.pintu;

import android.content.Intent;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import cdflynn.android.library.checkview.CheckView;

public class OrderPlaced extends AppCompatActivity {

    final static int SPLASH_TIME_OUT = 2000;


    CheckView checkView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_placed);

        initialization();
         checkView.check();


         new Handler().postDelayed(new Runnable() {
             @Override
             public void run() {

                 Intent intent = new Intent(OrderPlaced.this, HomeScreen.class );
                 startActivity(intent);
                 finish();

             }
         }, SPLASH_TIME_OUT);
    }




    private void initialization() {

        checkView = findViewById(R.id.check);
    }
}
