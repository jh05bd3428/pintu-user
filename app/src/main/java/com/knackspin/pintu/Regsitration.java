package com.knackspin.pintu;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.knackspin.pintu.Singleton.Singleton;
import com.knackspin.pintu.utils.UrlPoint;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Regsitration extends AppCompatActivity {


    String verify_otp;
    String demo = "1111";
    AlertDialog alertDialog;
    String android_id;

    EditText reg_phone_field,reg_fname_field,reg_lname_field,reg_password_field,et_otp,reg_otp;
    Button bt_submit,bt_otp,reg_verify_otp_button,reg_button;
    TextView tv_login;
    ImageView iv_back;

    public static final String DEFAULT = "NA";
    LinearLayout login_layout;
    RelativeLayout reg_main_layout;

    private Context context = this;
    private boolean callApi = false;
    private boolean verifyApi = false;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_regsitration);

         initialization();

        reg_otp.setEnabled(false);
        reg_fname_field.setEnabled(false);
        reg_lname_field.setEnabled(false);
        reg_password_field.setEnabled(false);
        bt_submit.setEnabled(false);


        reg_phone_field.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (reg_phone_field.getText().length() == 10 && callApi == false)
                {
                    checkPhoneNumber();
                    callApi = true;
                }


            }

            @Override
            public void afterTextChanged(Editable s) {

                if (!(reg_phone_field.getText().length() == 10))
                {
                    callApi = false;
                }


            }
        });

        tv_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Regsitration.this, Login.class);
                startActivity(intent);
                finish();
            }
        });

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });

    }

    private void checkPhoneNumber() {

        CustomLayout.setUIToWait(Regsitration.this,true);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, UrlPoint.register_otp, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                CustomLayout.setUIToWait(Regsitration.this,false);
                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(response);

                    if (jsonObject.getString("status").equalsIgnoreCase("success"))
                    {

                        verify_otp = jsonObject.getString("otp");


                        Toast.makeText(context, "Enter the otp received", Toast.LENGTH_SHORT).show();
                        checkOtp();

                    } else
                    {
                        Toast.makeText(context, "User already registered", Toast.LENGTH_SHORT).show();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d("PhoneVerifyException", e.getMessage( ));
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                CustomLayout.setUIToWait(Regsitration.this, false);
                if (error instanceof ServerError) {

                    Snackbar snackbar = Snackbar.make(reg_main_layout, "Server Down! please try again", Toast.LENGTH_SHORT);
                    snackbar.show();
                    ;
                } else if (error instanceof NetworkError) {
                    Snackbar snackbar = Snackbar.make(reg_main_layout, "Internet Issue! please try again", Toast.LENGTH_SHORT);
                    snackbar.show();
                    ;
                } else {
                    Snackbar snackbar = Snackbar.make(reg_main_layout, "Something went wrong! please try again", Toast.LENGTH_SHORT);
                    snackbar.show();
                    ;
                }
            }
        })

        {
            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<String, String>();
                params.put("phone",reg_phone_field.getText().toString());
                return params;
            }


        };

        Singleton.getInstance(Regsitration.this).addTorequestqu(stringRequest);

    }

    private void checkOtp() {

        reg_otp.setEnabled(true);

        reg_otp.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (reg_otp.getText().length() == 4 && verifyApi == false)
                {
                    verifyOtp();
                    verifyApi = true;
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

                if (!(reg_otp.getText().length() == 4))
                {
                    verifyApi = false;
                }

            }
        });



    }

    private void verifyOtp() {

        if (verify_otp.equalsIgnoreCase(reg_otp.getText().toString())) {

            Toast.makeText(context, "Number valid", Toast.LENGTH_SHORT).show();
            reg_fname_field.setEnabled(true);
            reg_lname_field.setEnabled(true);
            reg_password_field.setEnabled(true);

            registerUser();

        }
        else
        {
            Toast.makeText(context, "Invalid OTP", Toast.LENGTH_SHORT).show();

        }

    }

    private void registerUser() {
        
        bt_submit.setEnabled(true);

        bt_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                
                
                checkBlankFeilds();
                
            }
        });
        


    }

    private void checkBlankFeilds() {
        
        if (reg_fname_field.getText().toString().isEmpty())
        {
            Toast.makeText(context, "Enter first name", Toast.LENGTH_SHORT).show();
        } 
        
        else
            {
                if (reg_lname_field.getText().toString().isEmpty()) 
                {
                    Toast.makeText(context, "Enter last name", Toast.LENGTH_SHORT).show();

                } else 
                    {
                        if (reg_password_field.getText().toString().isEmpty())
                        {
                            Toast.makeText(context, "Enter password", Toast.LENGTH_SHORT).show();

                        } else
                        {
                           performRegistration();
                        }
                    }
            }
        
        
        
    }

    private void performRegistration() {

        CustomLayout.setUIToWait(Regsitration.this,true);
        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(Regsitration.this);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, UrlPoint.Registration_Url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                CustomLayout.setUIToWait(Regsitration.this,false);

                JSONObject  jsonObject;

                try {
                    jsonObject = new JSONObject(response);

                    if (jsonObject.optString("flag").equalsIgnoreCase("true"))
                    {
                        String userId = jsonObject.getString("userid");
                        Toast.makeText(Regsitration.this, "Registration Succesful", Toast.LENGTH_SHORT).show();

                        SharedPreferences sharedPreferences = getSharedPreferences("MyData",Context.MODE_PRIVATE); // creating xml file to store data
                        SharedPreferences.Editor editor = sharedPreferences.edit();// edit to store data into xml file
                        editor.putString("userId", userId);// data that is being put in xml file
                        editor.apply();

                        SharedPreferences.Editor editor1 = preferences.edit();
                        editor1.putBoolean("first", false);
                        editor1.apply();

                        Intent intent = new Intent(Regsitration.this, Login.class);
                        startActivity(intent);
                        finish();

                    }else
                    {
                        Toast.makeText(Regsitration.this, "Already Registered", Toast.LENGTH_SHORT).show();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d("RegException", e.getMessage());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                CustomLayout.setUIToWait(Regsitration.this,false);

                //  Toast.makeText(Regsitration.this, error.getMessage(), Toast.LENGTH_SHORT).show();

                if(error instanceof ServerError)
                {

                    Snackbar snackbar = Snackbar.make(reg_main_layout, "Server Down! please try again", Toast.LENGTH_SHORT);
                    snackbar.show();;                }

                else if (error instanceof NetworkError)
                {
                    Snackbar snackbar = Snackbar.make(reg_main_layout, "Internet Issue! please try again", Toast.LENGTH_SHORT);
                    snackbar.show();;                }

                else
                {
                    Snackbar snackbar = Snackbar.make(reg_main_layout, "Something went wrong! please try again", Toast.LENGTH_SHORT);
                    snackbar.show();;                }

            }
        })

        {

            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<String, String>();
                params.put("fName",reg_fname_field.getText().toString());
                params.put("lName",reg_lname_field.getText().toString());
                params.put("email","");
                params.put("pincode","");
                params.put("phone",reg_phone_field.getText().toString());
                params.put("password",reg_password_field.getText().toString());

                return params;
            }

        };

        Singleton.getInstance(Regsitration.this).addTorequestqu(stringRequest);


    }


    private void initialization() {

        reg_phone_field = findViewById(R.id.reg_phone_field);
        reg_fname_field = findViewById(R.id.reg_fname_field);
        reg_lname_field = findViewById(R.id.reg_lname_field);
        reg_password_field = findViewById(R.id.reg_password_field);
        //et_otp = findViewById(R.id.et_otp);

        bt_submit = findViewById(R.id.bt_submit);
        //bt_otp = findViewById(R.id.bt_otp);
        reg_otp = findViewById(R.id.reg_otp);
        tv_login = findViewById(R.id.tv_login);
        iv_back = findViewById(R.id.iv_back);
        reg_main_layout = findViewById(R.id.reg_main_layout);

    }

}

